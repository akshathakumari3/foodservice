<?php
/**
* Language file for menu strings
*
*/
return [


    'dashboard'              => 'Dashboard',
    'my_worksheet'           => 'My Worksheet',
    'register_customer'      => 'Register Customer',
    'register_property'      => 'Register Property',
    'print_notary_documents' => 'Print Notary Documents',
    'new_application'        => 'New Application',
    'customer'               => 'Customer',
    'property'               => 'Property',
    'transactions'           => 'Transactions',

   
];
