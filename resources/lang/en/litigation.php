<?php

return [

    'winner'                  => 'Winner',
    'plaintiff'               => 'Plaintiff',
    'defendant'               => 'Defendant',
    'create_litigation'       => 'Create New Litigation',
    'litigation_start_date'   => 'Litigation Start Date',
    'litigation_type'         => 'Litigation Type',
    'court_case_description'  => 'Court Case Description',
    'comments'                => 'Comments',
    'plaintiff_details'       => 'Plaintiff Details',
    'plaintiff_name'          => 'Plaintiff Name',
    'defendant_details'       => 'Defendant Details',
    'defendant_name'          => 'Defendant Name',
    'litigation'              => 'Litigation',
    'litigation_registration' => 'Litigation Registration',
    'parties'                 => 'Parties',
    'litigation_date'         => 'Litigation Date',
    'court_case_number'       => 'Court Case Number',
    'select_defendant'        => 'Select Defendant',
    'select_plaintiff'        => 'Select Plaintiff',
    'litigation_details'      => 'Litigation Details',
    'litigation_parties'      => 'Litigation Parties',
    'add_plaintiff'           => 'Add Plaintiff',
    'add_defendant'           => 'Add Defendant',
    'add_property'            => 'Add Property'


];
