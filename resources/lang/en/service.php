 <?php
/**
* Language file for customer 
*
*/
return [

		'provider_name'                 => 'Service Provider Name',
		'Category'                		=> 'Category',
		'contact_person_name'           => 'Contact Person Name',
		'contact_number'                 => 'Contact Number'  
];