<?php
/**
* Language file for payment strings
*
*/
return [


     'payment_details'     => 'Payment Details',
     'application_details' => 'Application Details',
     'sales_invoice'       => 'Sales Invoice',
     'pay'                 => 'Pay',
     'invoice_details'     => 'Invoice Details',
     'no'                  => 'No.',
     'fee_item'            => 'Fee Item',
     'fee'                 => 'Fee',
     'net_subtotal'        => 'Net Subtotal',
     'net_total'           => 'Net Total',
     'tax'                 => 'Tax',
     'total'               => 'TOTAL',
     'payment'             => 'Payment',
     'invoice_amount'      => 'Invoice Amount',
     'payment_amount'      => 'Payment Amount',
     'payment_date'        => 'Payment Date',
     'payment_method'      => 'Payment Method',
     'payment_ref_no'      => 'Payment Ref No',
     'payee_customer'      => 'Payee Customer',
     'customer_mobile'     => 'Customer Mobile',
     'gr_number'           => 'GR Number',
     'gr_receipt'          => 'GR Receipt',
     'property'            => 'Property',
     'register_customer'   => 'Register Customer',
     'register_property'   => 'Register Property',
     'new_application'     => 'New Application',
     'customer'            => 'Customer',
     'property_litigation' => 'Property Litigation',
     'my_worksheet'        => 'My Worksheet',
     'worksheet_list'      => 'Worksheet List',
     'transactions_list'   => 'Transactions List',
     'submit_lien'         => 'Submit Lien',
     'status'              => 'Status',
     'ref_no'              => 'Ref No',
     'service'             => 'Service',
     'application_date'    => 'Application Date',
     'buyer_name'          => 'Buyer Name',
     'seller_name'         => 'Seller Name',
     'parcel_id'           => 'Parcel Id',
     'settlement_amount'   => 'Settlement Amount',
     'payment_status'      => 'Payment Status',
     'action'              => 'Action',
     'type'                => 'Payment Type',
     'update_invoice'      => 'Update Invoice'
];
