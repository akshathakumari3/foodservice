 <?php
/**
* Language file for customer 
*
*/
return [

		'name'					 => 'Name',
		'client'					 => 'Client',
		'client_name'                => 'Client Name',
		'type'                		 => 'Type',
		'staryear'                	 => 'Started Year',
		'client_contact_number'      => 'Contact Number',
		'person_contact_number'      => 'Contact Person',
		'Email'      				 => 'Email',
		'client_list'				 => 'Client List',
		'first_name'				 => 'Client  Name',
		'no_of_employees'			 => 'No. Of Employee',
		'started_year'				 => 'Started Year',
		'turnover'					 => 'Turnover',
		'primary_contact_person'	 => 'Primary Contact Person',
		'primary_contact_number'	 => 'Primary Contact Number',
		'contact_number_service_provider'=> 'Contact Service Provider',
		'secondary_contact_person'	=> 'Secondary Contact Person',
		'postal_code'				=> 'Postal Code',
		'logo'						=> 'Logo',
		'secondary_service_provider'=> 'Secondary Service Provider',

];