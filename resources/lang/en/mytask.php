<?php
/**
* Language file for mytask strings
*
*/
return [


     'my_worksheet'       => 'My Worksheet',
     'my_tasks'           => 'My Tasks',
     'task_id'            => 'Task Id',
     'reference_no'       => 'Reference No',
     'service'            => 'Service',
     'parcel_id'          => 'Parcel ID',
     'buyer'              => 'Buyer',
     'settlement_date'    => 'Settlement Date',
     'payment_details'    => 'Payment Details',
     'application_status' => 'Application Status',
     'action'             => 'Action',
     'to_claim'           => 'To Claim',
     'building'           => 'Building',


   
];
