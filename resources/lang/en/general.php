<?php
/**
* Language file for general strings
*
*/
return [

    's_no'                       => 'S.No',
    'no'                         => 'No',
    'noresults'                  => 'No Results',
    'yes'                        => 'Yes',
    'site_name'                  => 'SiteName',
    'home'                       => 'Home',
    'dashboard'                  => 'Dashboard',
    'new_application'            => 'New Application',
    'register_customer'          => 'Register Customer',
    'register_property'          => 'Register Property',
    'save'                       => 'Save',
    'cancel'                     => 'Cancel',
    'back'                       => 'Back',
    'edit'                       => 'Edit',
    'close'                      => 'Close',
    'customers_list'             => 'Customers List',
    'select_value'               => 'Select Value',
    'please_select'              => 'Please Select',
    'add'                        => 'Add',
    'submit'                     => 'Submit',
    'select'                     => 'Select',
    'reset'                      => 'Reset',
    'search'                     => 'Search',
    'print'                      => 'Print',
    'documents'                  => 'Documents',
    'action'                     => 'Action',
    'select_sub_district_street' => 'Please Select Sub District And Street Name.',
    'select_sub_district'        => 'Please Select Sub District.',
    'select_street_or_grid'      => 'Fadlan Xulo Wadada Ama Grid.',
    'customer_property_details'  => 'Customer and Property Details',
    'sales_invoice'              => 'Sales Invoice',
    'pay'                        => 'Pay',
    'digital_signature'          => 'Signature',
    'clear'                      => 'Clear',
    'witness1_signature'         => 'Witness 1 Signature',
    'witness2_signature'         => 'Witness 2 Signature',
    'customer_signature'         => 'Customer Signature',
    'notary_signature'           => 'Notary Signature',
    'receiver_signature'         => 'Receiver Signature',
    'select_sub_district_grid'   => 'Please Select Sub District And Grid.',
    'buyer_signature'            => 'Buyer Signature',
    'seller_signature'           => 'Seller Signature',
    'trustee_signature'          => 'Trustee Signature',
    'select_district'            => 'Please Select District',
  

];
