 <?php
/**
* Language file for  comments
*
*/

return [

  'date'        => 'Date',
  'comments'    => 'Comments',
  'updated_by'  => 'Updated By',
  'action'      => 'Action',
  'add_comment' => 'Add Comment',
  'comment'     => 'Comment',

   
];
