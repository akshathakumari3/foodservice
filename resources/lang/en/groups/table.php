<?php
/**
* Language file for group table headings
*
*/

return array(

    'id'         => 'Id',
    'name'       => 'Name',
    'users'      => 'No. of Users',
    'created_at' => 'Created at',
    'actions'	 => 'Actions',
    'somali_name'=> 'Somali Name',
    'sort_order' => 'Sort Order'

);
