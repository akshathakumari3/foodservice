<?php
/**
* Language file for general strings
*
*/
return [

    'tax_invoice'                => 'Tax Data Assign',
    'print_tax_invoice'          => 'Print Tax Invoice',
    'download'                   => 'Download Tax Invoice', 
    'tax_invoice_success'        => 'Tax Invoice Created Successfully.',
    'tax_collection'             => 'Tax Collection',
    'tax_notice_no'              => 'Scan Tax Notice No',
    'tax_payment'                => 'Tax Payment',
    'tax_ref_no'                 => 'Tax Invoice Ref Number',
    'tax_invoice_update'         => 'Tax Invoice Updated Successfully.',
    'tax_invoice_error'          => 'Tax Invoice Update Issue.',
    'tax_invoice_nothing'        => 'Nothing to Genearate Tax Invoices',
    'assign_officer'             => 'Assign Officer',
    'tax_surveyor_assigned'      => 'Tax Surveyor Assigned',
    'assignee'                   => 'Assignee',
    'select_assignee'            => 'Please Select Assignee',
    'tax_invoice_creation'       => 'Tax Invoice Creation',
    'parcel_data_sync'           => 'Parcel Data Sync',
    'parcel_data_sync_success'   => 'Parcel Data Sync Successfully.',
    'parcel_data_sync_nothing'   => 'Nothing to Parcel Data Sync',
    'grid'                       => 'Grid',
    'officer_name'               => 'Officer Name',
    'tax_survey'                 => 'Tax Survey',
    'tax_survey_view'            => 'Tax Survey View',
    'receiver_name'              => 'Receiver Name',
    'no_of_adults'               => 'No of Adults',
    'no_of_childrens'            => 'No of Childrens',
    'no_of_kitchen'              => 'No of Kitchen',
    'no_of_floor'                => 'No of Floor',
    'no_of_bedrooms'             => 'No of Bedrooms',
    'rent_amount'                => 'Rent Amount',
    'rent'                       => 'Rent',
    'no_of_beds'                 => 'No of Beds',
    'building_age'               => 'Building Age',
    'property_height'            => 'Property Height',
    'no_of_residential_rooms'    => 'No of Residential Rooms',
    'no_of_commercial_rooms'     => 'No of Commercial Rooms',
    'contact_name'               => 'Contact Name'


];
