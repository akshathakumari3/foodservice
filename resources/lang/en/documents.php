 <?php
/**
* Language file for  documents
*
*/

return [

  'documents'       => 'Documents',
  'date'            => 'Date',
  'document_type'   => 'Document Type',
  'updated_by'      => 'Updated By',
  'action'          => 'Action',
  'add_document'    => 'Add Document',
  'document_name'   => 'Document Name',
  'upload_document' => 'Upload Document',
 
];
