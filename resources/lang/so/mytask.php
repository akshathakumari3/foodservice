<?php
/**
* Language file for mytask strings
*
*/
return [

    
     'my_worksheet'       => 'Xaashida Shaqadayda',
     'my_tasks'           => 'Hawlahayga',
     'task_id'            => 'Hawsha Id',
     'reference_no'       => 'Tixraac Maya',
     'service'            => 'Adeeg',
     'parcel_id'          => 'ID Aqoonsi',
     'buyer'              => 'Iibsade',
     'settlement_date'    => 'Taariikhda dejinta',
     'payment_details'    => 'Faahfaahinta Bixinta',
     'application_status' => 'Xaaladda Codsiga',
     'action'             => 'Ficil',
     'to_claim'           => 'Ku andacoodid',
     'building'           => 'Dhismaha',
   
];
