 <?php
/**
* Language file for  documents 
*
*/

return [

  'documents'       => 'Waraaqaha Lifaaqa',
  'date'            => 'Taariikhda',
  'document_name'   => 'Magaca Waraaqda',
  'document_type'   => 'Nooca Waraaqda',
  'updated_by'      => 'Waxa Diyaariyey',
  'action'          => 'Talaabada La Qaaday',
  'add_document'    => 'Kudar Dukumenti',
  'upload_document' => 'Diiwaan Galinta',
   
];
