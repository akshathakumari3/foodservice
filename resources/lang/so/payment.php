<?php
/**
* Language file for payment strings
*
*/
return [


     'payment_details'     => 'Faahfaahinta Bixinta',
     'application_details' => 'Faahfaahinta Codsiga',
     'sales_invoice'       => 'Xaashiyaha iibka',
     'pay'                 => 'Bixi',
     'invoice_details'     => 'Faahfaahinta qaansheegta',
     'no'                  => 'Maya.',
     'fee_item'            => 'Shayga Lacagta',
     'fee'                 => 'Lacag',
     'net_subtotal'        => 'Isku-darka Net Net',
     'net_total'           => 'Wadarta guud',
     'tax'                 => 'Canshuur',
     'total'               => 'Wadarta',
     'payment'             => 'Bixinta',
     'invoice_amount'      => 'Tirada qaansheegadka',
     'payment_amount'      => 'Tirada Bixinta',
     'payment_date'        => 'Taariikhda Bixinta',
     'payment_method'      => 'Qaabka mushaar-bixinta',
     'payment_ref_no'      => 'Bixinta Ref No',
     'payee_customer'      => 'Macaamiisha Payee',
     'customer_mobile'     => 'Adeegga Macaamiisha',
     'gr_number'           => 'Lambarka GR',
     'gr_receipt'          => 'GR Rasiidha',
     'property'            => 'Hanti',
     'register_customer'   => 'Diiwaangeli Macaamiisha',
     'register_property'   => 'Diiwaangelinta Hanti',
     'new_application'     => 'Codsi Cusub',
     'customer'            => 'Macaamiil',
     'property_litigation' => 'Dacwada Hantiyeed',
     'my_worksheet'        => 'Xaashida Shaqadayda',
     'worksheet_list'      => 'Liiska Xaashiyaha',
     'transactions_list'   => 'Liiska Xawaaladaha',
     'submit_lien'         => 'Gudbi Lien',
     'status'              => 'Xaaladda',
     'ref_no'              => 'Ref Maya',
     'service'             => 'Adeeg',
     'application_date'    => 'Taariikhda Codsiga',
     'buyer_name'          => 'Magaca iibsadaha',
     'seller_name'         => 'Magaca Iibiyaha',
     'parcel_id'           => 'buugga Id',
     'settlement_amount'   => 'Tirada Xalka',
     'payment_status'      => 'Xaaladda Bixinta',
     'action'              => 'Ficil',
     'type'                => 'Nooca Lacagta',
     'update_invoice'      => 'Cusboonaysiinta qaansheegta'

];
