<?php
/**
* Language file for general strings
*
*/
return [
 
    'tax_invoice'              => 'Diiwaangelinta Macluumaadka Canshuuraha',
    'print_tax_invoice'        => 'Daabac Xaanshida Cashuurta',
    'download'                 => 'Soo degso qaansheegta canshuurta', 
    'tax_invoice_success'      => 'Bixinta cashuurta si guul leh ayaa loo abuuray.',
    'tax_collection'           => 'Uruurinta Canshuuraha',
    'tax_notice_no'            => 'Tirtir Lambarka Ogeysiiska Canshuuraha',
    'tax_payment'              => 'Bixinta Canshuuraha',
    'tax_ref_no'               => 'Lambarka Tixraaca Cashuurta',
    'tax_invoice_update'       => 'Bixinta cashuurta si guul leh ayey u cusbooneysiisay.',
    'tax_invoice_error'        => 'Bixinta Cusboonaysiinta Bixinta Cashuurta.',
    'tax_invoice_nothing'      => 'Waxba Kama Helo Bixinta Cashuuraha',
    'assign_officer'           => 'Sarkaalka Meelaynta',
    'assignee'                 => 'Assignee',
    'tax_surveyor_assigned'    => 'Canshuuraha Kormeeraha Loogu talagalay',
    'select_assignee'          => 'Fadlan Dooro Assignee',
    'tax_invoice_creation'     => 'Cashuur Bixinta Cashuurta',
    'parcel_data_sync'         => 'Synceliska Wadajirka ee Parcel',
    'parcel_data_sync_success' => 'Iskuduwaha Xogta Parcel si guul ah.',
    'parcel_data_sync_nothing' => 'Waxba iskumidma Parcel Data Sync',
    'grid'                     => 'Grid',
    'officer_name'             => 'Magaca Sarkaalka',
    'tax_survey'               => 'Sahaminta Canshuuraha',
    'tax_survey_view'          => 'Muuqaalka Sahanka Sahanka',
    'receiver_name'            => 'Magaca Soo Dhaweynta',
    'no_of_adults'             => 'Tirada Dadka Waaweyn',
    'no_of_childrens'          => 'Tirada Caruurta',
    'no_of_kitchen'            => 'Tirada jikada',
    'no_of_floor'              => 'Tirada Dabaqa',
    'no_of_bedrooms'           => 'Qolalka hurdada',
    'rent_amount'              => 'Tirada Kirada',
    'rent'                     => 'Kirada',
    'no_of_beds'               => 'Tirada Sariiraha',
    'building_age'             => "Da'da Dhismaha",
    'property_height'          => 'Dhererka Dhismaha',
    'no_of_residential_rooms'  => 'Tirada Qolalka Degaanka',
    'no_of_commercial_rooms'   => 'Tirada Qolalka Ganacsiga',
    'contact_name'             => 'Magaca Xiriirka'

];
