 <?php
/**
* Language file for comments 
*
*/

return [

  'date'        => 'Taariikhda',
  'comments'    => 'Faalada',
  'updated_by'  => 'Waxa Diyaariyey',
  'action'      => 'Talaabada La Qaaday',
  'add_comment' => 'Kudar Faallo',
  'comment'     => 'Faallo',
];
