<?php
/**
* Language file for general strings
*
*/
return [

    's_no'                       => 'Nambarada taxan',
    'no'                         => 'Maya',
    'yes'                        => 'Haa',
    'dashboard'                  => 'Boorsada',
    'new_application'            => 'Codsi Cusub',
    'register_customer'          => 'Diiwaangeli Macaamiisha',
    'register_property'          => 'Diiwaangelinta Hanti',
    'save'                       => 'Badbaadi',
    'cancel'                     => 'Tirtir',
    'back'                       => 'Soo noqo',
    'edit'                       => 'Tafatir',
    'close'                      => 'Xidh',
    'customers_list'             => 'Liiska Macaamiisha',
    'select_value'               => 'Xul Qiimaha',
    'please_select'              => 'Fadlan dooro',
    'add'                        => 'ku dar',
    'submit'                     => 'Gudbi',
    'select'                     => 'Xullo',
    'reset'                      => 'Dib u dejinta',
    'search'                     => 'Raadin',
    'print'                      => 'Daabac',
    'documents'                  => 'Dukumentiyada',
    'action'                     => 'Ficil',
    'select_sub_district_street' => 'Fadlan Xulo Qaybta Degmo iyo Magaca Wadada.',
    'select_sub_district'        => 'Fadlan Xulo Degmo-hoosaadka.',
    'select_street_or_grid'      => 'Fadlan Xulo Qaybta Degmo Ama Grid.',
    'customer_property_details'  => 'Faahfaahinta Macaamiisha iyo Hantida',
    'sales_invoice'              => 'Xaashiyaha iibka',
    'pay'                        => 'Bixi',
    'digital_signature'          => 'Saxeex',
    'clear'                      => 'Nadiifi',
    'witness1_signature'         => 'Markhaati 1 Saxeex',
    'witness2_signature'         => 'Markhaati 2 Saxeex',
    'customer_signature'         => 'Saxiixa Macaamiisha',
    'notary_signature'           => 'Saxeex Nootaayo',
    'receiver_signature'         => 'Saxiixa Qaataha ah',
    'select_sub_district_grid'   => 'Fadlan Xulo Degmo-hoosaad iyo Grid.',
    'buyer_signature'            => 'Saxiixa Iibsiga',
    'seller_signature'           => 'Saxiixa Iibiyaha',
    'trustee_signature'          => 'Saxiixa Masuulka',



];
