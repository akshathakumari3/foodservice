<?php

return [

    'winner'                  => 'Guul',
    'plaintiff'               => 'Dacwoode',
    'defendant'               => 'Eedaysanaha',
    'create_litigation'       => 'Abuur Maxkamad Cusub',
    'litigation_start_date'   => 'Taariikhda Bilaabashada',
    'litigation_type'         => 'Nooca dacwada',
    'court_case_description'  => 'Sharraxaadda Kiiska Maxkamadda',
    'comments'                => 'Faallooyinka',
    'plaintiff_details'       => 'Faahfaahinta Dacwoodaha',
    'plaintiff_name'          => 'Magaca Dacwooda',
    'defendant_details'       => 'Faahfaahinta Eedaysanaha',
    'defendant_name'          => 'Magaca eedaysanaha',
    'litigation'              => 'Dacwada',
    'litigation_registration' => 'Diiwaan Gelinta Dacwada',
    'parties'                 => 'Dhinacyada',
    'litigation_date'         => 'Taariikhda Dacwada',
    'court_case_number'       => 'Lambarka Kiiska Maxkamad',
    'select_defendant'        => 'Xulo Eedeeyaha',
    'select_plaintiff'        => 'Xulo Dacwoe',
    'litigation_details'      => 'Faahfaahinta dacwadda',
    'litigation_parties'      => 'Qaybaha dacwadda',
    'add_plaintiff'           => 'Kudar Dacwo',
    'add_defendant'           => 'Kudar Eedaysanaha',
    'add_property'            => 'Ku dar Hanti'


];
