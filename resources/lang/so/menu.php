<?php
/**
* Language file for general strings
*
*/
return [


    'dashboard'              => 'Boorsada',
    'my_worksheet'           => 'Xaashida Shaqadayda',
    'register_customer'      => 'Diiwaangeli Macaamiisha',
    'register_property'      => 'Diiwaangelinta Hanti',
    'print_notary_documents' => 'Daabac Dukumiinti Nootaayo ah',
    'new_application'        => 'Codsi Cusub',
    'customer'               => 'Macaamiil',
    'property'               => 'Hanti',
    'transactions'           => 'Xawaaladda',

   
];
