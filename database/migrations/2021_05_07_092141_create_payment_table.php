<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblpayment', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('order_id')->unsigned()->nullable();
			$table->decimal('amount')->nullable();
			$table->integer('paid_by')->unsigned()->nullable();
			$table->date('payment_date')->nullable();
			$table->foreign('order_id')->references('id')->on('tblorder');
			$table->foreign('paid_by')->references('id')->on('tbluser');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpayment');
    }
}
