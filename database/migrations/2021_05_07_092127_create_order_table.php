<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblorder', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('customer_id')->unsigned()->nullable();
			$table->date('order_date')->nullable();
			$table->decimal('total_amount')->nullable();
			$table->boolean('status')->default(1);
			$table->integer('created_by')->unsigned()->nullable();
			$table->foreign('created_by')->references('id')->on('tbluser');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblorder');
    }
}
