<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblorderdetails', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('order_id')->unsigned()->nullable();
			$table->integer('menu_id')->unsigned()->nullable();
			$table->decimal('amount')->nullable();
			$table->decimal('total_amount')->nullable();
			$table->foreign('order_id')->references('id')->on('tblorder');
			$table->foreign('menu_id')->references('id')->on('tblmenu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblorderdetails');
    }
}
