<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblmenu', function (Blueprint $table) {
            $table->increments('id');
			$table->string('menu_name')->nullable();
			$table->decimal('price')->nullable();
			$table->integer('cateogry_id')->unsigned()->nullable();
			$table->text('image')->nullable();
			$table->string('ingredients')->nullable();
		    $table->boolean('status')->default(1);
		    $table->foreign('cateogry_id')->references('id')->on('tblcateogry');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblmenu');
    }
}
