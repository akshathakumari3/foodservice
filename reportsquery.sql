Report SQL 

1.Dashboard Report


  select transactions.id AS transaction_id, property.district AS district, users.first_name AS user, services.name AS transaction_type,
    CASE
    WHEN (workflow_log.state = "0") THEN "Assigned"
    WHEN (workflow_log.state = "1") THEN "In Progress"
    WHEN (workflow_log.action = "Approve") THEN "Approved"
    WHEN (workflow_log.action = "Rejecte") THEN "Rejected"
    END AS status, TIMESTAMPDIFF(SECOND, `workflow_log`.`assigned_time`, `workflow_log`.`actioned_time`) AS lapse from `workflow_log` 
    inner join `transactions` on `transactions`.`id` = `workflow_log`.`transaction_id` inner join `property` on `property`.`id` = `transactions`.`property_id`
    inner join `services` on `services`.`id` = `transactions`.`service_id` left join `users` on `users`.`id` = `workflow_log`.`user_id` where `role_id` = 3