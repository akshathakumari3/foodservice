<?php

namespace App\ReportServiceProvider;
use Config;
use DB;
use DateTime;
use DateInterval;
use DatePeriod;
use Sentinel;

class ReportServiceProvider {
    protected $date;

    public function __construct()
    {
        $this->date = Config::get('general');
        $this->insurance = Config::get('insurance');
        
    }
    public function customDateTimeFormat($date)
	{
        if($date != 'null' && strlen($date) != 1) {
            $newDate = date($this->date['date-time-format'], strtotime($date));
            return $newDate;
        }
        return '-';
    }
	public function customDateFormat($date)
	{
        if($date != 'null' && strlen($date) != 1) {
            $newDate = date($this->date['date-format'], strtotime($date));
            return $newDate;
        }
    }
    public function customTimeFormat($date)
	{
        if($date != 'null') {
            $newDate = date($this->date['time-format'], strtotime($date));
            return $newDate;
        }
        return '-';
    }
    public function customNumberFormat ($value) {
        if(strlen($value) == 1 && strpos($value,'-') == 0) {
            return number_format((int)0,2);
        }
        return number_format((int)$value, 2);
    }
    public function emptyValue($value) {

        if(strlen($value) == 1 && strpos($value,'-') == 0) {
            return " ";
        }
        return $value;
    }

    public function getSericeType() {
        return $this->insurance['service_type'];
    }
    public function getPaymentType() {
        return $this->insurance['payment_type'];
    }
    public function getClaimStatus() {
        return $this->insurance['claim_status'];
    }
    public function getClaimType() {
        return $this->insurance['claim_type'];
    }
    public function getState() {
        return $this->insurance['states'];
    }
    public function getCountry() {
        return $this->insurance['countries'];
    }

    public function getServiceProviderID() {
        $id = null;
        if(Sentinel::inRole('insurance-officer')) {
            $id = "insurance-officer";
        } else if (Sentinel::getUser()->group == 'Service Provider') {
            $id = isset(Sentinel::getUser()->group_id) ? Sentinel::getUser()->group_id: "" ;
        }
        return $id;
    }

    public function getClientId () {
        if (Sentinel::inRole('client')) {
            return isset(Sentinel::getUser()->group_id) ? Sentinel::getUser()->group_id: "" ;
        }
        return null;
    }
}