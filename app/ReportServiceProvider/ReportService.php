<?php

namespace App\ReportServiceProvider;

class ReportService {

	protected static function resolveFacede($name) 
	{
		return app()[$name];
	}

	public static function __callStatic($method, $arguments)
	{
		return (self::resolveFacede('ReportService'))
				->$method(...$arguments);
	}
	
}