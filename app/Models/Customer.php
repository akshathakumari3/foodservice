<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class Customer extends Model
{

    
    public $table = 'tblcustomer';
    protected $fillable = ['first_name','last_name','middle_name','email','phone_number','landline'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name	' => '',
        'last_name' => '',
        'middle_name' => '',
        'email' => '',
        //'location_reference' => 'required',
        //'phone_carrier' => 'required'
    ];
    
   


}
