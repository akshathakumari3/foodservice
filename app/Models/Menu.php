<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class Menu extends Model
{

    
    public $table = 'tblmenu';
    protected $fillable = ['menu_name','price','cateogry_id','image','ingredients','status'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'menu_name	' => '',
        'price' => '',
        'cateogry_id' => '',
        'image' => '',
        //'location_reference' => 'required',
        //'phone_carrier' => 'required'
    ];
    
   
	public function category()
    {
        return $this->belongsTo('App\Models\Cateogry', 'cateogry_id', 'id');
    }

}
