<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class OrderDetails extends Model
{

    
    public $table = 'tblorderdetails';
    protected $fillable = ['order_id','menu_id','amount','total_amount'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id	' => '',
        'menu_id' => '',
        'amount' => '',
        //'location_reference' => 'required',
        //'phone_carrier' => 'required'
    ];
    
   
	public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

}
