<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class Order extends Model
{

    
    public $table = 'tblorder';
    protected $fillable = ['customer_id','order_date','total_amount'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id	' => '',
        'order_date' => '',
        'total_amount' => '',
        //'location_reference' => 'required',
        //'phone_carrier' => 'required'
    ];
    
   
	

}
