<?php 
namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Sentinel;
use Analytics;
use View;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;
use Charts;
use App\Datatable;
use App\User;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\Period;
use Illuminate\Support\Carbon;
use File;
use App\Models\Transaction;

class JoshController extends Controller {

    /**
     * Message bag.
     *
     * @var Illuminate\Support\MessageBag
     */
    protected $messageBag = null;

    /**
     * Initializer.
     *
     */
    public function __construct()
    {
        $this->messageBag = new MessageBag;

    }


    public function showView($name=null)
    {
        if(View::exists(env('DOMAINCODE').'.admin/'.$name))
        {
            if(Sentinel::check())
                return view(env('DOMAINCODE').'.admin.'.$name);
            else
                return redirect('admin/signin')->with('error', 'You must be logged in!');
        }
        else
        {
            abort('404');
        }
    }

    public function activityLogData()
    {
        $logs = Activity::get(['causer_id', 'log_name', 'description','created_at']);
        return DataTables::of($logs)
            ->make(true);
    }

    public function showHome() {   
        if($user = Sentinel::check()) { 
            
            if(Sentinel::inRole('insurance-officer') ){
                return Redirect::route('user.dashboard2')->with('success', trans('auth/message.signin.success'));
            }else if(Sentinel::inRole('client')){
				
                return Redirect::route('app.client.dashboard')->with('success', trans('auth/message.signin.success'));
            }else if(Sentinel::inRole('receptionist') || Sentinel::inRole('specialist')|| Sentinel::inRole('receiptionist') || Sentinel::inRole('admin') || Sentinel::inRole('service-provider')|| Sentinel::inRole('payment')){
                return Redirect::route('user.dashboard')->with('success', trans('auth/message.signin.success'));
            }          
        }else{
            return redirect('admin/signin')->with('error', 'You must be logged in!');
        }
    }

}