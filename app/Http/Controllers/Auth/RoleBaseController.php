<?php

namespace App\Http\Controllers\Auth;

use \App\Models\Transaction;
use Sentinel;
use Illuminate\Support\Facades\Log;
use App\Helpers\WorkflowHelper;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Models\service;
use App\Helpers\ErpHelper;
use App\Models\District;
use App\Models\Invocie\Invocie;
use App\Models\Customer;
use App\Models\Insurance\Policy;
use App\Models\Insurance\ClaimApplication;
use App\Models\ServiceProvider\ServiceProvider;
use App\Models\Client\Client;
use Flash;
use Redirect;
use App\Models\InvoiceHelper;
use Config;
use App\Models\Insurance\Specialists;
class RoleBaseController extends \App\Http\Controllers\JoshController
{
   
    public function __construct() {
        $this->erpHelper = new ErpHelper();
    }    
    
    
    public function dashboard() {
		
        return view(env('DOMAINCODE').".portal.partials.dashboard-common");
    }
    public function getDashboardDetails(Request $request) {
        $start_date             = $request->start;
         $end_date               = $request->end." "."23:00:00";
        $user = Sentinel::getUser();   
		$group_id      = Sentinel::getUser()->group_id;
        $role = strtolower($user->getRoles()->pluck('name')->first());
        $view = 'portal.' . $role . '.dashboard';
               
        $tasksAssignedToMe = "";
        $taskId = "";
        $unclaimedTasks = ""; 
        $building          = Config::get('gis.building');  
        $date=date('Y-m-d');
        $dayVistCount=ClaimApplication::where('day_visit_or_hospitalization','Day Visit')->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count(); 
        $dayVistCurrentCount=ClaimApplication::where('day_visit_or_hospitalization','Day Visit')->where('date_of_hospitalization','=',$date)->count();  
        $hospitalizationCount=ClaimApplication::where('day_visit_or_hospitalization','Hospitalization')->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $hospitalizationCurrentCount=ClaimApplication::where('day_visit_or_hospitalization','Hospitalization')->where('date_of_hospitalization','=',$date)->count();
        $claimSettled=ClaimApplication::where('status','Settled')->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $claimSettledCurrent=ClaimApplication::where('status','Settled')->whereRaw('DATE(updated_at) = ?', [$date])->count();
        $claimSubmitted=ClaimApplication::where('status','Submitted')->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $claimSubmittedCurrent=ClaimApplication::where('status','Submitted')->whereRaw('DATE(updated_at) = ?', [$date])->count();
		
		if(Sentinel::inRole('service-provider')){
			$claimTotal  = ClaimApplication::where('service_provider_id',$group_id)->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			$claimClosed=ClaimApplication::where('service_provider_id',$group_id)->where('status','Settled')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			$claimApproved=ClaimApplication::where('service_provider_id',$group_id)->where('status','Approved')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			$claimOpen=ClaimApplication::where('service_provider_id',$group_id)->where('status','Open')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			$specialistCOunt  = Specialists::with(["educations","serviceProvider"])->where('service_provider_id',$group_id)->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			//$specialistCOunt=Specialists::where('service_provider_id',$group_id)->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
        
		}else{
			$claimTotal  = ClaimApplication::where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();		
			$claimClosed=ClaimApplication::where('status','Settled')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			$claimApproved=ClaimApplication::where('status','Approved')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			$claimOpen=ClaimApplication::where('status','Open')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->count();
			$specialistCOunt=Specialists::with(["educations","serviceProvider"])->where('created_at','<=',$end_date)->count();
		}
		
		
		
        $claimOpenCurrent=ClaimApplication::where('status','Open')->whereRaw('DATE(updated_at) = ?', [$date])->count();     
        Log::info("Redirecting user to " . $view);
        return view(env('DOMAINCODE').".portal.partials.get-dashboard-common-details", compact('user', "unclaimedTasks", "taskId","building","dayVistCount","hospitalizationCount","claimOpen","claimApproved","claimClosed","dayVistCurrentCount","hospitalizationCurrentCount","claimSettledCurrent",
        "claimSubmittedCurrent","claimOpenCurrent","claimTotal","specialistCOunt"));
        //return view("portal.partials.dashboard-common", compact('user', 'transactions', "unclaimedTasks", "taskId"));
    }

    public function dashboard2() {
        return view(env('DOMAINCODE').".portal.partials.dashboard2");
    }
    public function getDashboard2Details(Request $request){
        $start_date             = $request->start;
        $end_date               = $request->end;

        $customer               = Customer::where('family_head',1)->where('active',1)->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $policyApplication      = Policy::where('status','!=','In progress')->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $serviceProvider        = ServiceProvider::where('active',1)->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $client                 = Client::where('status',1)->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $claimOpenApplication   = ClaimApplication::where('status','Open')->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $claimClosedApplication = ClaimApplication::whereIn('status', array('Settled','Rejected'))->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $totalClaim             = ClaimApplication::whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->count();
        $policyPremiumAmount    = Policy::where('status','!=','In progress')->whereBetween(\DB::raw('date(created_at)'), array($start_date,$end_date))->sum('sum_insured');
        return view(env('DOMAINCODE').".portal.partials.get-dashboard2-details",compact('customer','policyApplication','serviceProvider','client','claimOpenApplication','claimClosedApplication','totalClaim','policyPremiumAmount'));
    }
    public function worksheet() {
        $user = [];
        $building          = Config::get('gis.building');
        return view(env('DOMAINCODE').".portal.partials.worksheet", compact('user',"building"));        
    }
    
    
    public function getRecentTransactions() {
        $transactions = Transaction::with(["buyer", "seller", "property", "service"])->whereNotNull("buyer_id")->take(20)->orderBy('id', 'desc')->get(); 
        
    }

    public function transactions() {

        $down_towns        = Config::get('gis.down_towns');
        $number_of_stories = Config::get('gis.number_of_stories');
        $building          = Config::get('gis.building');
        $service_types     = Config::get('gis.service_types');
        return view(env('DOMAINCODE').".admin.workflow.transactions",compact('down_towns','building','number_of_stories','service_types'));
    }

    public function transactionsDatatable(Request $request) {
        
        $limit            = $request->input('length');
        $start            = $request->input('start');
        $status           = $request->input('status');
        $service_type     = $request->input('service_type');
        $search           = $request->input('search.value');
        $domain_code      = env("DOMAINCODE");
        $user_district_id = Sentinel::getUser()->district_id;
        $user             = Sentinel::getUser()->getRoles()->pluck('slug')->first();
        $district         = '';
        $district_name    = '';
        if($domain_code == "grw"){
            $workflow_status  = ["Payment 1","Payment 2","Payment 3","Payment 1 USD Paid","Payment 2 USD Paid","Payment 1 SOS Paid","Payment 2 SOS Paid"];
        }elseif ($domain_code == "coh") {
            $workflow_status  = ["Payment 1","Payment 2","Payment 3","Payment 1 USD Paid","Payment 2 USD Paid","Payment 1 SOS Paid","Payment 2 SOS Paid","Application Fee Paid","Geometer Fee Paid","Premium of Land Fee Paid","Transfer Tax Paid","Certificate of Ownership Paid","Transfer Tax Fee Paid"];

            if(($user_district_id) && ($domain_code == 'coh')){

                $user_districts   = explode(",", $user_district_id);

                // $district         = District::select('name')->where('id',$user_district_id)->first();
                // $district_name    = $district->name;
                $district_name    = District::whereIn('id',$user_districts)->pluck('name');
               
            }
        }
        
        $query = Transaction::with(["buyer", "seller", "property", "service"])
                            ->whereIn('status',$workflow_status);

        if($domain_code=='coh'){
            $transactions = $query->whereHas('property',function($query) use($district_name,$user,$user_district_id){
                                if(($user!='admin') && ($district_name!= null || $district_name!= '')){
                                   
                                    $query->whereIn("district",$district_name);
                                }
                                
                            });

            if($search){
                $transactions =$query->where('transaction_reference_number','like','%' .$search.'%');
            }
        }else{
            $transactions = $query;
        }        
                    
        if(empty($status)) {
            $transactions = $transactions;
        }else{
            
            $transactions =$transactions->where('status',$status);
        } 

        if(empty($service_type)) {
            $transactions = $transactions;
        }else{
            
            $transactions = $query->whereHas('service',function($query) use($service_type){
                                if(($service_type!= null || $service_type!= '')){
                                   
                                    $query->where("service_type",$service_type);
                                }
                                
                            });
        }  

        $transactions_count = $transactions;
        $transactions_count = $transactions_count->get();
        $transactions       = $transactions->limit($limit)->offset($start)->orderBy('id','DESC')->get();
        $totalData          = count($transactions_count);
        $totalFiltered      = $totalData;
        $data               = [];
        //$values             = [];
       
            foreach ($transactions as $transaction)
            {

                $show                            = route('user.show-transaction',$transaction->id);
                $nestedData['id']                = $transaction->id;
                $nestedData['service_name']      = $transaction->service->name;
                $nestedData['application_date']  = date('d-m-Y', strtotime($transaction->application_date));
                $nestedData['buyer_name']        = isset($transaction->buyer)?$transaction->buyer->full_name:'';
                $nestedData['seller_name']       = isset($transaction->seller)?$transaction->seller->full_name:'';
                $nestedData['parcel_id']         = $transaction->property->parcel_id;
                $nestedData['settlement_amount'] = $transaction->settlement_amount;
                $nestedData['payment_status']    = $transaction->payment_status;
                $nestedData['status']            = $transaction->status;
                if((Sentinel::inRole('admin')) || (Sentinel::hasAccess('admin.invocie.regenarate'))){

                    $nestedData['actions']           = "<a href='{$show}' title='VIEW' class='btn btn-sm btn-raised btn-success  transaction-btn'>Pay</a><a onclick='transactionInvoiceEdit({$transaction->id},{$transaction->property_id})' id='transaction-invoice-edit-{$transaction->id}' href='#invoice-edit-modal-div'  data-toggle='modal' data-transaction_id = '{$transaction->id}' data-settlement_amount = '{$transaction->settlement_amount}' data-parcel_area_sqm = '{$transaction->property->parcel_area_sqm}' data-down_town = '{$transaction->property->down_town}' data-building = '{$transaction->property->building}' data-number_of_stories = '{$transaction->property->number_of_stories}' data-building_area = '{$transaction->property->building_area}' data-no_of_buildings = '{$transaction->property->no_of_buildings}' title='EDIT' class='btn btn-sm btn-raised btn-info  transaction-btn'><span class='fa fa-edit'></span></a>";
                }else{
                    $nestedData['actions']           = "<a href='{$show}' title='VIEW' class='btn btn-sm btn-raised btn-success  transaction-btn'>Pay</a>";
                }


                
                $data[]                          = $nestedData;
                
                //array_push($values,$transaction->status);
            }
                //$statusList = array_unique($values);
       
            $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data,
                    "statusList"      => $workflow_status
                    );

        echo json_encode($json_data);       
    }

    // public function showTransaction($transactionId) {

    //     $domain_code      = env("DOMAINCODE");

    //     if($domain_code == "coh"){
    //         return $this->cohPayment($transactionId);
    //     }elseif ($domain_code == "grw") {
    //        return $this->grwPayment($transactionId);
    //     }
             
    // }

    public function showTransaction($transactionId)
    {
        try {
            
            $user           = Sentinel::getUser();
            $transaction    = Transaction::with(["buyer", "seller", "property"])->where("id","=",$transactionId)->get()->first();
            $service        = service::find($transaction->service_id);
            $serviceFees            = [];
            $usd                    = '-USD';
            $sos                    = '-SOS';
            $sales_invoice          = [];
            $currency_exchange_rate = '';
            $invoice_name           = '';
            $item_price_list        = [];
            $currency               = false;
            $service_code           = ''; 
            $payment_code           = '';
            $invoice_refno          = '';
           
            $invoice_helper = $this->getInvoiceHelper($transaction);
            
            if($invoice_helper){

                $currency = $invoice_helper->currency;
                log::info("currency : ".$currency);
                log::info("payment_type : ".$invoice_helper->payment_type);

                if($currency == "USD"){
                    $currency_symbol = '$ ';
                }elseif ($currency == "SOS") {
                   $currency_symbol = 'Slsh ';
                }

                $payment_code2   = $invoice_helper->next_payment;
                $payment_status  = $invoice_helper->payment_status;
            }

            if($currency){
                $invoice_refno   = $this->getInvoice($transaction,$currency);
            }

            if(empty($invoice_refno)){

                Flash::error($currency." Invoice Not Found");
                return Redirect::back();
            }elseif($invoice_refno){

                $invoice_name  = $this->getInvoiceName($invoice_refno,$currency);
                $sales_invoice = $this->erpHelper->getSalesInvoice($invoice_name);
            }
            
            return view(env('DOMAINCODE').'.admin.workflow.invoice', ['transaction' => $transaction, "serviceFees" => $serviceFees,"payment_code" => $payment_code,"service_code" => $service_code,"payment_code2"=>$payment_code2,"payment_status"=>$payment_status,"currency"=>$currency,"currency_symbol"=>$currency_symbol,"currency_exchange_rate"=>$currency_exchange_rate,"sales_invoice"=>$sales_invoice,"invoice_name"=>$invoice_name,"item_price_list"=>$item_price_list]);

        } catch (Exception $ex) {
             Log::error($ex->getMessage());
             Flash::error("An unexpected error occurred. Message " . $ex->getMessage());
              return redirect()->back();
        }
             
    }


    // public function cohPayment($transactionId)
    // {
    //     try {
            
    //         $user           = Sentinel::getUser();
    //         $transaction    = Transaction::with(["buyer", "seller", "property"])->where("id","=",$transactionId)->get()->first();
    //         $service        = service::find($transaction->service_id);
    //         $serviceFees            = [];
    //         $usd                    = '-USD';
    //         $sos                    = '-SOS';
    //         $sales_invoice          = [];
    //         $currency_exchange_rate = '';
    //         $invoice_name           = '';
    //         $item_price_list        = [];
    //         $currency               = false;
    //         $service_code           = ''; 
    //         $payment_code           = '';
    //         if($transaction->status == env('WORKFLOW_PAYMENT1_STATUS') && $transaction->payment_status == ""){

    //             $payment_type   = 1;
    //             log::info("payment_type : ".$payment_type);
    //             $invoice_helper = $this->getInvoiceHelper($transaction,$payment_type);
    //             if($invoice_helper){
    //                 $currency       = $invoice_helper->currency;
    //                 log::info("currency : ".$currency);
    //             }

    //             if($currency == "USD"){
    //                 $currency_symbol = '$ ';
    //             }elseif ($currency == "SOS") {
    //                $currency_symbol = 'Slsh ';
    //             }

    //             $payment_code2   = $invoice_helper->next_payment;
    //             $payment_status  = 'Payment 1 '.$currency.' Paid';
    //             $invoice_refno   = $this->getInvoice($transaction,$currency);

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno){

    //                 $invoice_name  = $this->getInvoiceName($invoice_refno,$currency);
    //                 $sales_invoice = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }

    //         }elseif (($transaction->status == "Payment 1 SOS Paid" && $transaction->payment_status == "Payment 1 SOS Paid") || ($transaction->status == "Payment 1 USD Paid" && $transaction->payment_status == "Payment 1 USD Paid")) {

    //             $payment_type   = 1;
    //             log::info("payment_type : ".$payment_type);

    //             if($transaction->status == "Payment 1 SOS Paid" && $transaction->payment_status == "Payment 1 SOS Paid"){

    //                 $currency       = "USD";
                   
    //             }elseif ($transaction->status == "Payment 1 USD Paid" && $transaction->payment_status == "Payment 1 USD Paid") {

    //                 $currency       = "SOS";
    //             }
    //             log::info("currency : ".$currency);
    //             if($currency == "USD"){
    //                 $currency_symbol = '$ ';
    //             }elseif ($currency == "SOS") {
    //                $currency_symbol = 'Slsh ';
    //             }

    //             $invoice_helper = $this->getNextInvoiceHelper($transaction,$payment_type,$currency);
                
    //             $payment_code2   = $invoice_helper->next_payment;
    //             $payment_status  = 'Payment 1 '.$currency.' Paid';
    //             $invoice_refno   = $this->getInvoice($transaction,$currency);

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno){

    //                 $invoice_name  = $this->getInvoiceName($invoice_refno,$currency);
    //                 $sales_invoice = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }
                

    //         }elseif (($transaction->status == env('WORKFLOW_PAYMENT2_STATUS') && $transaction->payment_status == "Payment 1 SOS Paid") || ($transaction->payment_status == "Payment 1 USD Paid")) {

    //             $payment_type   = 2;
    //             log::info("payment_type : ".$payment_type);
    //             $invoice_helper = $this->getInvoiceHelper($transaction,$payment_type);
    //             if($invoice_helper){
    //                 $currency       = $invoice_helper->currency;
    //                 log::info("currency : ".$currency);
    //             }

    //             if($currency == "USD"){
    //                 $currency_symbol = '$ ';
    //             }elseif ($currency == "SOS") {
    //                $currency_symbol = 'Slsh ';
    //             }
                
    //             $payment_code2   = $invoice_helper->next_payment;
    //             $payment_status  = 'Payment 2 '.$currency.' Paid';
    //             $invoice_refno   = $this->getInvoice($transaction,$currency);

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno){

    //                 $invoice_name  = $this->getInvoiceName($invoice_refno,$currency);
    //                 $sales_invoice = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }

    //         }elseif (($transaction->status == "Payment 2 SOS Paid" && $transaction->payment_status == "Payment 2 SOS Paid") || ($transaction->status == "Payment 2 USD Paid" && $transaction->payment_status == "Payment 2 USD Paid")) {
                
    //             $payment_type   = 2;
    //             log::info("payment_type : ".$payment_type);

    //             if($transaction->status == "Payment 2 SOS Paid" && $transaction->payment_status == "Payment 2 SOS Paid"){

    //                 $currency       = "USD";
                   
    //             }elseif ($transaction->status == "Payment 2 USD Paid" && $transaction->payment_status == "Payment 2 USD Paid") {

    //                 $currency       = "SOS";
    //             }

    //             log::info("currency : ".$currency);
    //             if($currency == "USD"){
    //                 $currency_symbol = '$ ';
    //             }elseif ($currency == "SOS") {
    //                $currency_symbol = 'Slsh ';
    //             }

    //             $invoice_helper = $this->getNextInvoiceHelper($transaction,$payment_type,$currency);
                
    //             $payment_code2   = $invoice_helper->next_payment;
    //             $payment_status  = 'Payment 2 '.$currency.' Paid';
    //             $invoice_refno   = $this->getInvoice($transaction,$currency);

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno){

    //                 $invoice_name  = $this->getInvoiceName($invoice_refno,$currency);
    //                 $sales_invoice = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }
    //         }
    //         // elseif ($transaction->status == env('WORKFLOW_PAYMENT3_STATUS') && $transaction->payment_status == "Payment 2 USD Paid") {
                
    //         //     $service_code    = $service->payment3.$usd; 
    //         //     $serviceFees     = $this->erpHelper->getServiceFees($service_code);
    //         //     $payment_code    = $service_code;
    //         //     $payment_code2   = $service->payment3.$sos;
    //         //     $payment_status  = 'Payment 3 USD Paid';
    //         //     $currency        = substr($usd, 1);
    //         //     $currency_symbol = '$ ';
    //         // } elseif ($transaction->status == "Payment 3 USD Paid" && $transaction->payment_status == "Payment 3 USD Paid") {
                
    //         //     $service_code    = $service->payment3.$sos; 
    //         //     $serviceFees     = $this->erpHelper->getServiceFees($service_code);
    //         //     $payment_code    = $service_code;
    //         //     $payment_code2   = false;
    //         //     $payment_status  = 'Payment 3 SOS Paid';
    //         //     $currency        = substr($sos, 1);
    //         //     $currency_symbol = 'Slsh ';
    //         //     $currency_exchange = $this->erpHelper->getCurrencyExchange();
    //         //     $currency_exchange_rate =$currency_exchange->exchange_rate;
    //         // } 
            
    //       return view(env('DOMAINCODE').'.admin.workflow.invoice', ['transaction' => $transaction, "serviceFees" => $serviceFees,"payment_code" => $payment_code,"service_code" => $service_code,"payment_code2"=>$payment_code2,"payment_status"=>$payment_status,"currency"=>$currency,"currency_symbol"=>$currency_symbol,"currency_exchange_rate"=>$currency_exchange_rate,"sales_invoice"=>$sales_invoice,"invoice_name"=>$invoice_name,"item_price_list"=>$item_price_list]);

    //     } catch (Exception $ex) {
    //          Log::error($ex->getMessage());
    //          Flash::error("An unexpected error occurred. Message " . $ex->getMessage());
    //           return redirect()->back();
    //     }
                
        
      
    // }

    // public function grwPayment($transactionId)
    // {
    //     try {
            
    //         $user         = Sentinel::getUser();
    //         $transaction  = Transaction::with(["buyer", "seller", "property"])->where("id","=",$transactionId)->get()->first();

    //         $service                = service::find($transaction->service_id);
    //         $serviceFees            = [];
    //         $usd                    = '-USD';
    //         $sos                    = '-SOS';
    //         $sales_invoice          = [];
    //         $currency_exchange_rate = '';
    //         $invoice_name           = '';
    //         $item_price_list        = [];
    //         if($transaction->status == env('WORKFLOW_PAYMENT1_STATUS') && $transaction->payment_status == ""){

    //             $service_code    = $service->payment1.$usd; 
    //             $payment_code    = $service_code;
    //             $payment_code2   = $service->payment1.$sos;
    //             $payment_status  = 'Payment 1 USD Paid';
    //             $currency        = substr($usd, 1);
    //             $currency_symbol = '$ ';
    //             $invoice_refno   = Invocie::select("id","invoice1_refno")
    //                                    ->where('transaction_id','=',$transactionId)
    //                                    ->where('customer_id' , $transaction->buyer_id)
    //                                    ->where('currency' , $currency)
    //                                    ->where('status',"Invoice Created")
    //                                    ->first();

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno->invoice1_refno){
    //                 $invoice_name  = $invoice_refno->invoice1_refno;
    //                 $sales_invoice = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }

    //         }elseif ($transaction->status == "Payment 1 USD Paid" && $transaction->payment_status == "Payment 1 USD Paid") {
                
    //             $service_code    = $service->payment1.$sos; 
    //             //$serviceFees     = $this->erpHelper->getServiceFees($service_code);
    //             $payment_code    = $service_code;
    //             $payment_code2   = false;
    //             $payment_status  = 'Payment 1 SOS Paid';
    //             $currency        = substr($sos, 1);
    //             $currency_symbol = 'Slsh ';
    //             $invoice_refno   = Invocie::select("id","invoice2_refno")
    //                                            ->where('transaction_id' , $transactionId)
    //                                            ->where('customer_id' , $transaction->buyer_id)
    //                                            ->where('currency' , $currency)
    //                                            ->where('status',"Invoice Created")
    //                                            ->first();

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno->invoice2_refno){

    //                 $invoice_name    = $invoice_refno->invoice2_refno;
    //                 $sales_invoice   = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }

    //         }elseif ($transaction->status == env('WORKFLOW_PAYMENT2_STATUS') && ($transaction->payment_status == "Payment 1 USD Paid" || $transaction->payment_status == "Payment 1 SOS Paid")) {

    //             // Payment 1 SOS Paid
    //             $service_code    = $service->payment2.$usd; 
    //             $payment_code    = $service_code;
    //             $payment_code2   = $service->payment2.$sos;
    //             $payment_status  = 'Payment 2 USD Paid';
    //             $currency        = substr($usd, 1);
    //             $currency_symbol = '$ ';
    //             $invoice_refno   = Invocie::select("id","invoice1_refno")
    //                                    ->where('transaction_id','=',$transactionId)
    //                                    ->where('customer_id' , $transaction->buyer_id)
    //                                    ->where('currency' , $currency)
    //                                    ->where('status',"Invoice Created")
    //                                    ->first();

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno->invoice1_refno){
    //                 $invoice_name    = $invoice_refno->invoice1_refno;
    //                 $sales_invoice   = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }

    //         }elseif ($transaction->status == "Payment 2 USD Paid" && $transaction->payment_status == "Payment 2 USD Paid") {
                
    //             $service_code           = $service->payment2.$sos; 
    //             $payment_code           = $service_code;
    //             $payment_code2          = false;
    //             $payment_status         = 'Payment 2 SOS Paid';
    //             $currency               = substr($sos, 1);
    //             $currency_symbol        = 'Slsh ';
    //             $invoice_refno          = Invocie::select("id","invoice2_refno")
    //                                            ->where('transaction_id' , $transactionId)
    //                                            ->where('customer_id' , $transaction->buyer_id)
    //                                            ->where('currency' , $currency)
    //                                            ->where('status',"Invoice Created")
    //                                            ->first();

    //             if(empty($invoice_refno)){

    //                 Flash::error($currency." Invoice Not Found");
    //                 return Redirect::back();
    //             }elseif($invoice_refno->invoice2_refno){

    //                 $invoice_name    = $invoice_refno->invoice2_refno;
    //                 $sales_invoice   = $this->erpHelper->getSalesInvoice($invoice_name);
    //             }
    //         }
    //         // elseif ($transaction->status == env('WORKFLOW_PAYMENT3_STATUS') && $transaction->payment_status == "Payment 2 USD Paid") {
                
    //         //     $service_code    = $service->payment3.$usd; 
    //         //     $serviceFees     = $this->erpHelper->getServiceFees($service_code);
    //         //     $payment_code    = $service_code;
    //         //     $payment_code2   = $service->payment3.$sos;
    //         //     $payment_status  = 'Payment 3 USD Paid';
    //         //     $currency        = substr($usd, 1);
    //         //     $currency_symbol = '$ ';
    //         // } elseif ($transaction->status == "Payment 3 USD Paid" && $transaction->payment_status == "Payment 3 USD Paid") {
                
    //         //     $service_code    = $service->payment3.$sos; 
    //         //     $serviceFees     = $this->erpHelper->getServiceFees($service_code);
    //         //     $payment_code    = $service_code;
    //         //     $payment_code2   = false;
    //         //     $payment_status  = 'Payment 3 SOS Paid';
    //         //     $currency        = substr($sos, 1);
    //         //     $currency_symbol = 'Slsh ';
    //         //     $currency_exchange = $this->erpHelper->getCurrencyExchange();
    //         //     $currency_exchange_rate =$currency_exchange->exchange_rate;
    //         // } 
            
    //     return view(env('DOMAINCODE').'.admin.workflow.invoice', ['transaction' => $transaction, "serviceFees" => $serviceFees,"payment_code" => $payment_code,"service_code" => $service_code,"payment_code2"=>$payment_code2,"payment_status"=>$payment_status,"currency"=>$currency,"currency_symbol"=>$currency_symbol,"currency_exchange_rate"=>$currency_exchange_rate,"sales_invoice"=>$sales_invoice,"invoice_name"=>$invoice_name,"item_price_list"=>$item_price_list]);

    //     } catch (Exception $ex) {
    //          Log::error($ex->getMessage());
    //          Flash::error("An unexpected error occurred. Message " . $ex->getMessage());
    //           return redirect()->back();
    //     }
    // }


    public function getInvoice($transaction,$currency)
    {

        $invoice_refno   = Invocie::select("id","invoice2_refno","invoice1_refno")
                               ->where('transaction_id','=',$transaction->id)
                               ->where('customer_id' , $transaction->buyer_id)
                               ->where('currency' , $currency)
                               ->where('status',"Invoice Created")
                               ->first();

        return $invoice_refno;
    }

    public function getInvoiceHelper($transaction)
    {
        $invoice_helper = InvoiceHelper::where("service_id", "=", $transaction->service_id)
                               // ->where("payment_type", "=", $payment_type)
                                ->where("transaction_status", "=" , $transaction->status)
                                ->orderBy('sort_by', 'asc')
                                ->first();
        return $invoice_helper;
    }

    public function getNextInvoiceHelper($transaction,$payment_type,$currency)
    {
        $invoice_helper = InvoiceHelper::where("service_id", "=", $transaction->service_id)
                                ->where("payment_type", "=", $payment_type)
                                ->where("currency", "=", $currency)
                                ->orderBy('sort_by', 'asc')
                                ->first();
        return $invoice_helper;
    }

    public function getInvoiceName($invoice_refno,$currency)
    {
        if($currency == "USD"){
          $invoice_name  = $invoice_refno->invoice1_refno;
        }elseif ($currency == "SOS") {
           $invoice_name  = $invoice_refno->invoice2_refno;
        }

        return $invoice_name;
    }

    
}