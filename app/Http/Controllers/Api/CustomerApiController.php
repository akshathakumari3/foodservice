<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Api;
use Response;
use Illuminate\Database;
use DB;
use Exception;
use config;
use Illuminate\Support\Facades\Log;
use App\Models\Cateogry;
use App\Models\Menu;
use App\Models\Customer;
class CustomerApiController extends Controller {
	public function __construct() {
 
    }

	public function index(Request $request){
		 $menuList = menu::query('category')->with('category');
		 if($request->has('sort')){
			  $menuList =$menuList->orderBy('menu_name',$request->sort);
			  $menuList =$menuList->orderBy('price',$request->sort);
			  $menuList =$menuList->orderBy('cateogry_id',$request->sort);
		 }
		 $menuList = $menuList->get();
		return response()->json($menuList, 200);
	}

	public function store(Request $request){
		$customer = Customer::create($request->all());

		return response()->json($customer, 201);
	}
	public function update(Request $request, $id)
    {
	
        $customer = Customer::findOrFail($id);
        $customer->update($request->all());

        return $customer;
    }
	public function destroy(Request $request, $id)
    {
		
		$customer = Customer::findOrFail($id);
        $customer->delete();

        return response()->json(null, 204);
    }
}