<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Api;
use Response;
use Illuminate\Database;
use DB;
use Exception;
use config;
use Illuminate\Support\Facades\Log;
use App\Models\Cateogry;

class CategoryApiController extends Controller {
	public function __construct() {
 
    }

	public function index(Request $request){
		 $cateogry = Cateogry::query();
		 if($request->has('sort')){
			  $cateogry =$cateogry->orderBy('name',$request->sort);
		 }
		  $cateogry = $cateogry->get();
		 
		return response()->json($cateogry, 200);
	}

	public function store(Request $request){
		$cateogry = Cateogry::create($request->all());

		return response()->json($cateogry, 201);
	}
	public function update(Request $request, $id)
    {
	
        $cateogry = Cateogry::findOrFail($id);
        $cateogry->update($request->all());

        return $cateogry;
    }
	public function destroy(Request $request, $id)
    {
		
		$cateogry = Cateogry::findOrFail($id);
        $cateogry->delete();

        return response()->json(null, 204);
    }
    
}