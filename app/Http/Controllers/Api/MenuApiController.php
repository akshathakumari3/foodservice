<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Api;
use Response;
use Illuminate\Database;
use DB;
use Exception;
use config;
use Illuminate\Support\Facades\Log;
use App\Models\Cateogry;
use App\Models\Menu;
class MenuApiController extends Controller {
	public function __construct() {
 
    }

	public function index(Request $request){
		 $cateogry = menu::query();
		 if($request->has('sort')){
			  $cateogry =$cateogry->orderBy('menu_name',$request->sort);
			  $cateogry =$cateogry->orderBy('price',$request->sort);
			  $cateogry =$cateogry->orderBy('cateogry_id',$request->sort);
		 }
		  $cateogry = $cateogry->get();
		 
		return response()->json($cateogry, 200);
	}

	public function store(Request $request){
		 $name='';
		 if($request->image){
		$file = $request->file('image');
 
		$format = $request->image->extension();
        //save full adress of image
		$path = $request->image->store('images');

        $name = $file->getClientOriginalName();
		 }
		$menu= new Menu();
		$menu->menu_name=$request->menu_name;
		$menu->price=$request->price;
		$menu->cateogry_id=$request->cateogry_id;
		$menu->image=$path;
		$menu->ingredients=$request->ingredients;
		$menu->save();
		if($menu->id){
			$result=response()->json($menu, 201);
		}else{
			$result=response()->json($menu, 500);
		}
		return $result;
	}
	public function update(Request $request, $id)
    {
	
        $menu = Menu::findOrFail($id);
		if($request->image){
			unlink(storage_path('app/'.$menu->image));
			$file = $request->file('image');
	 
			$format = $request->image->extension();
			//save full adress of image
			$path = $request->image->store('images');

			$name = $file->getClientOriginalName();
		
		}else{
			$path =$menu->image;
		}

		$menu->menu_name=$request->menu_name;
		$menu->price=$request->price;
		$menu->cateogry_id=$request->cateogry_id;
		$menu->image=$path;
		$menu->ingredients=$request->ingredients;
		$menu->save();
		if($menu->id){
			$result=response()->json($menu, 201);
		}else{
			$result=response()->json($menu, 500);
		}
		return $result;
    }
	public function destroy(Request $request, $id)
    {
		
		$menu = Menu::findOrFail($id);
        $menu->delete();

        return response()->json(null, 204);
    }
    
}