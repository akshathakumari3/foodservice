<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Api;
use Response;
use Illuminate\Database;
use DB;
use Exception;
use config;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;
use App\Models\Cateogry;
use App\Models\Menu;
use App\Models\OrderDetails;
use App\Models\Order;
class OrderApiController extends Controller {
 public function __construct() {
 
    }
	public function index(Request $request){
		 $orderDetails = OrderDetails::query()->with('order');
		 if($request->has('sort')){
			  $orderDetails =$orderDetails->orderBy('order_id',$request->sort);
		 }
		 $orderDetails = $orderDetails->get();
		return response()->json($orderDetails, 200);
	}
	public function store(Request $request){
		$customer_id=$request->customer_id;
		$menu_id=explode(",",$request->menu_id);
		if(isset($menu_id)){
			$order						=new Order();
			$order->customer_id			=$customer_id;
			$order->order_date			=date('Y-m-d');
			$order->save();
			$order_id=$order->id;
			if(isset($order_id)){
				$amount=0;
				foreach($menu_id as $row){
					$menu_details=Menu::find($row);
					$amount+=isset($menu_details->price) ? $menu_details->price :0;
					$orderDetails				=new OrderDetails();
					$orderDetails->order_id		=$order_id;
					$orderDetails->menu_id		=$row;
					$orderDetails->amount		=$menu_details->price;
					$orderDetails->save();
				}
				$order = Order::findOrFail($order_id);
				$order->total_amount			=$amount;
				$order->save();
				if($order->id){
					return response()->json("sucessfully created the order", 201);
				}
			}
		}
	}

    public function destroy(Request $request, $id)
    {
		
		$orderDetails = OrderDetails::where("order_id",$id)->delete();
       // $orderDetails->delete();
		$order = Order::findOrFail($id);
        $order->delete();

        return response()->json(null, 204);
    }
}