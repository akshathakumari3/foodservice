<?php


namespace App\Http\Controllers;

use Sentinel;
use App\Http\Requests;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Customer;
use Session;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Property;
use App\Models\PolicyMemberItem;
use App\Models\propertyOwnership;
use App\Models\District;
use App\Models\SubDivision;
use App\Models\Zoning;
use Yajra\DataTables\Facades\DataTables;
use App\Helpers\ErpConnector;
use App\Helpers\ErpHelper;
use Illuminate\Support\Facades\Log;
use DB;
use App\Helpers\DocumentHelper;
use App\Models\TransactionWitness;
use Carbon\Carbon;
use Config;
use Redirect;
use App\Models\CustomerIdentity;
use App\Country;
use App\Models\Guarantor;
use App\Http\Controllers\WorkflowController;
use App\Models\IdentificationDocumentTypes;
use App\Models\CustomerAddress;
use App\Models\Insurance\MedicalHistory;
use App\Models\Insurance\AttributeType;
use App\Models\Family\Family;
use App\Models\Family\FamilyRelation;
use App\Models\Family\Relationship;
use App\Models\Insurance\ClaimApplication;
use App\Models\Client\Client;
use App\Models\Client\MemberGroups;
use App\Models\Client\ClientMember;
use App\Models\Insurance\Policy;
use App\Models\Insurance\Invoice;
use App\Models\Insurance\Payment;
use App\Models\CustomerAdditionalDetails;
use App\Models\Insurance\PolicyProductDetail;
use App\Models\Insurance\PolicyAddons;
class CustomerController extends InfyOmBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;
    
    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
        $this->documentHelper     = new DocumentHelper();
        
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view(env('DOMAINCODE').'.admin.customers.index');
            
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
      $districts           = District::pluck('name','id');
      $educations          = AttributeType::where('code','education')->where('active',1)->first();
      $income_ranges       = AttributeType::where('code','income_range')->where('active',1)->first();
      $professional_status = AttributeType::where('code','professional_status')->where('active',1)->first();
      $occupations         = AttributeType::where('code','occupations')->where('active',1)->first();
      $blood_groups        = AttributeType::where('code','blood_group')->where('active',1)->first();
      $customer_types      = Config::get('insurance.customer_types');
      $phone_carriers      = Config::get('insurance.phone_carriers');
      $states              = Config::get('insurance.states');
      $countries           = Config::get('insurance.countries');
      $employer            = '';
      $form_type           = 'customer';
      $member_groups       = '';
      $customer_type       = 'Primary';

      if(Sentinel::inRole('client')){
        $client_id         = Sentinel::getUser()->group_id;
		
        $client            = client::find($client_id); 
        $employer          = isset($client->client_name) ? $client->client_name : '';
        $form_type         = 'member';
        $member_groups     = MemberGroups::where('status','1')->get();
        $customer_type     = 'Primary';
      }
   
      return view(env('DOMAINCODE').'.admin.customers.create',compact('districts','educations','income_ranges','professional_status','occupations','customer_types','phone_carriers','states','countries','employer','form_type','member_groups','customer_type','blood_groups'));
    }

	
	public function membercreate(){
		$districts           = District::pluck('name','id');
      $educations          = AttributeType::where('code','education')->where('active',1)->first();
      $income_ranges       = AttributeType::where('code','income_range')->where('active',1)->first();
      $professional_status = AttributeType::where('code','professional_status')->where('active',1)->first();
      $occupations         = AttributeType::where('code','occupations')->where('active',1)->first();
      $blood_groups        = AttributeType::where('code','blood_group')->where('active',1)->first();
      $customer_types      = Config::get('insurance.customer_types');
      $phone_carriers      = Config::get('insurance.phone_carriers');
      $states              = Config::get('insurance.states');
      $countries           = Config::get('insurance.countries');
      $employer            = '';
      $form_type           = 'customer';
      $member_groups       = '';
      $customer_type       = 'Primary';
	  $employeid='';
      if(Sentinel::inRole('client') || Sentinel::inRole('insurance-officer')){
        $client_id         = Sentinel::getUser()->group_id;
        $client            = client::find($client_id); 
        $employer          = isset($client->client_name) ? $client->client_name :'';
		$employeid          = isset($client->id) ? $client->id : '';
        $form_type         = 'member';
        $member_groups     = Client::where('status','1')->get();
        $customer_type     = 'Primary';
      }
   
      return view(env('DOMAINCODE').'.admin.customers.create',compact('districts','educations','income_ranges','professional_status','occupations','customer_types','phone_carriers','states','countries','employer','form_type','member_groups','customer_type','blood_groups','employeid'));
	}
    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
		//echo "<pre>";print_r($request->all());die();
		
		try {

			$family_head    = $request->input('family_head');
			$customer_id    = $request->input('customer_id');
			$update_customer= ($request->input('update_customer')) ? $request->input('update_customer') : null;
			$spouse_count=0;
			if($request->customer_type != "Primary") {
				if($request->relation_id==2){
					$member    = Customer::where('id',$customer_id)->first();
					$family_id = $member->familyRelation->family_id;
					$member_count         = FamilyRelation::where('family_id',$family_id)->where('relation_id',2)->count();
					$spouse_count=$member_count;
				}else{
					$spouse_count=0;
				}
			}
			$relationship           = Config::get('insurance.relationship');
			$spouseValue=$relationship['spouse'];
			//echo $spouse_count;echo "asdad".$spouseValue;die();
			if($spouse_count<$spouseValue){

			$customer       = $this->storeOrUpdateCustomer($request,$update_customer);
			//echo "thirdname".$customer->third_name."update_customer".$update_customer;
			$type           = $request->input('form_type');
			$extension_number="";
			if(isset($customer->id)){

				$upload_image    = $this->uploadCustomerImage($request,$customer);
				$medical_history = $this->addMedicalHistory($request,$customer->id,$update_customer);
				$additonalCustomerDetails = $this->additonalCustomerDetails($request,$customer->id,$update_customer);

				if($customer_id || $update_customer){
				  $family    = $this->getCustomerFamilyRealtion(isset($customer_id) ? $customer_id : $update_customer);
				  $family_id = $family->family_id;
				}else if($customer->third_name && $family_head){
				  $family    = $this->createFamily($customer);
				  $family_id = $family->id;
				}

				if($type == 'member'){
				  if ($update_customer) {
					$client_member                = ClientMember::where('customer_id',$customer->id)->first();
					 if(Sentinel::inRole('insurance-officer')){
						$client_member->group_id      = $request->member_group;
						$client_member->client_id=$request->member_group;
					 }else{
						$client_member->group_id      = $request->member_group_id;
						$client_member->client_id=$request->member_group_id; 
					 }
				  }else {
  				  $client_member                = new ClientMember();
  				 // $client_member->client_id     = Sentinel::getUser()->group_id;
  				  $client_member->customer_id   = $customer->id;
  				   if(Sentinel::inRole('insurance-officer')){
						$client_member->group_id      = $request->member_group;
						$client_member->client_id=$request->member_group;
					 }else{
						$client_member->group_id      = $request->member_group_id;
						$client_member->client_id=$request->member_group_id; 
					 }
  				  $client_member->member_number = 'CWW-00'.$customer->id;
  				  $client_member->status        = 1;
  				  $client_member->created_by    = Sentinel::getUser()->id;
				}
				  $client_member->save();
				}

				if(isset($family->id)){
				  if($type == 'member'){
						  $member = ClientMember::where('customer_id',$customer_id)->first();
					if($member){
					  $extension_number = $member->member_number.'-00'.$customer->id;
					}
					
				  }
				  if(!isset($update_customer)) {
					$family_relation = $this->addFamilyRelation($customer->id,$family_id,$request->relation_id,$extension_number);
					if($customer->customer_type != "Primary") {
					  log::info("Not Primary Customer");
					  $family_relation      = FamilyRelation::where('customer_id', $request->input('customer_id'))->first();
					 // echo "<pre>";print_r($family_relation);die();
					  $member_count         = FamilyRelation::where('family_id',$family_relation->family_id)->count();
					  log::info("member_countmember_count".$member_count);
					  $family_head_customer = FamilyRelation::where('family_id',$family_relation->family_id)->where('relation_id',1)->first();
					  $customer_head        = Customer::find($family_head_customer->customer_id);
					  $member_id = $customer_head->employer_id.'/00'.($member_count);
					}else {
					  log::info("Primary Customer");
					  $member_id = $customer->employer_id.'/001';
					}
					$customer->member_id = $member_id;
					
					
					$customer->save();


								
				  }
				}
				Flash::success('Customer saved successfully.');
				return response()->json(['result'=>'pass',"customer"=>$customer]);
			}else{
				Flash::error('Customer not created');
				return response()->json(['result'=>'fail']);
			}
			}else{
				 Session::flash('message', 'Only allow '.$spouseValue.' spouse not more than '.$spouseValue.' spouse allowed');
				 Session::flash('alert-class', 'alert-danger');
				//Flash::error('Only allow 4 spouse not more than 4 spouse allowed');
				return response()->json(['result'=>'fail']);
			}
		} catch(Exception $e)  {
				return abort(500, "Duplicate customer");			
		}
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $customer = $this->customerRepository->findWithoutFail($id);
		
        if (empty($customer)) {

            Flash::error('Customer not found');
            return redirect(route('admin.customers.index'));
        }

        $customer_image = '';
        if($customer->customer_photo_id){
            $get_image      = $this->documentHelper->getDocument($customer->customer_photo_id);
            $customer_image = base64_encode($get_image);
        }

        $family_relation      = FamilyRelation::where('customer_id',$customer->id)->first();
        $family_member_images = [];
        $family_members       = [];
        $image                = [];
        $claim_details        = [];
        $family_head_id       = 0;
		$family_head_name		='';
        if($family_relation){

            $family_members = FamilyRelation::where('family_id',$family_relation->family_id)->get();
            $client_member = ClientMember::where('customer_id',$id)->first();
            
            if(count($family_members)>0){
                foreach ($family_members as $key => $family_member) {
                    
                    if(isset($family_member->customer->customer_photo_id)){
                        $data             = $this->documentHelper->getDocument($family_member->customer->customer_photo_id);
                        $image[$family_member->id] = base64_encode($data);
                    }

                    if(isset($family_member->relation->relationship_name) ){
                      if($family_member->relation->relationship_name == 'FAMILYHEAD' || $family_member->relation->relationship_name == 'FAMILY HEAD'){
                        $family_head_id = $family_member->customer->id;
						$first_name=isset($family_member->customer->first_name) ? $family_member->customer->first_name : '';
						$second_name=isset($family_member->customer->second_name) ? $family_member->customer->second_name : '';
						$third_name=isset($family_member->customer->third_name) ? $family_member->customer->third_name : '';
						$fourth_name=isset($family_member->customer->fourth_name) ? $family_member->customer->fourth_name : '';
						$family_head_name = $first_name." ".$second_name." ".$third_name." ".$fourth_name;
                      }
                    }
                }
                $family_member_images      = $image;
            }
          $all_family_members    = FamilyRelation::where('family_id',$family_relation->family_id)->get();
          $all_members_id        = FamilyRelation::pluck('id');
          if($all_members_id){
            $claim_details   = ClaimApplication::with(['customer','serviceProvider'])->whereIn('customer_id',$all_members_id)->get();
          }
        }
		
		$additional_details=CustomerAdditionalDetails::where("customer_id",$id)->get();
        $medical_history = MedicalHistory::where('customer_id',$customer->id)->first();
        $policy_details  = Policy::with(['customer','PolicyProductDetail'])->where('customer_id',$id)->where('status','Application Submitted')->get();
		$policy_data=[];
		foreach($policy_details as $policy){
			//$policy_data['policy']=$policy;
			$policy_info=Policy::find($policy->id);
			$policy_data[]=array("Policy"=>$policy,"total_suminsured"=>$this->getSumInsured($policy_info));
		    //$policy_data['total_suminsured']  = $this->getSumInsured($policy_info);
		}
	
		
		
        $invoices        = Invoice::with(['payments'])->where('customer_id',$customer->id)->get();
        $product_option  = $this->getProductOption($policy_details);
			//echo "<pre>";print_r($invoices);die();
		$payment_data=array();	
        foreach($invoices as $inv){
			
			$payments=Payment::where('invoice_id',$inv->id)->get();
			
			//$paymentData[]=$payments;
			foreach($payments as $payment){
			
        
			if(isset($payment)){
            $pay['product_option']  = $this->getPorductOption($payment->invoice->policy);
            $pay['payment']         = $payment;
            $payment_data[]         = $pay;
			
				}
			}
        }
		//echo "<pre>";print_r($payment_data);die();
		/*foreach($paymentData as $row){
			foreach($row as $row1){
		echo "<pre>";print_r($row1);
			}
		}
		die();*/
	

        $payment  = $payment_data;
        $report_query             = "CustomerFinancialStatementReport?customerID=".$id;
        $policy_query             = "CustomerPolicyStatementReport?customerID=".$id;
        return view(env('DOMAINCODE').'.admin.customers.show',compact('id','customer','customer_image','family_member_images','family_members','medical_history','claim_details','policy_details','family_head_id','invoices','report_query','policy_query','client_member','additional_details','payment','product_option','policy_data','family_relation','family_head_name'));

    }
	 public function getSumInsured($policy_application) {
		// echo "<pre>";print_r($policy_application);die();
		  $total_sum_insured = 0;
		 //foreach($policy_application as $row){
			  $policy_addons_items = PolicyAddons::with('premiumType','policy')->where("policy_id", $policy_application->id)->get();
				if(isset($policy_application->sum_insured)) {
					$add_on_limit = collect($policy_addons_items)->pluck('addons_limit_amount')->sum();
					$total_sum_insured = $policy_application->sum_insured + $add_on_limit;
				//}			  
		 }
        return number_format($total_sum_insured,2);
    }
    public function getProductOption($policies) {
      if(isset($policies)) {
        foreach($policies as $policy) {
          $product_opt_name = $this->getProductOptionDetails($policy);
          $product_option[$policy->id] = rtrim($product_opt_name,' | ');
        }
        return isset($product_option)? $product_option :'' ;
      }
      return false;
    }

    public function getProductOptionDetails($policy) {
      $product_opt_name = ''; 
      if(isset($policy->PolicyProductDetail)){
        foreach($policy->PolicyProductDetail as $productDetails) {
          $product_opt_name .= $productDetails->productOption->name .' | ';
        }
      }
      return $product_opt_name;
    }
	
	public function getPorductOption($policy){
		//echo "asdasd".$policy->id;
		 $product_opt_name = ''; 
		$productDetail=PolicyProductDetail::where('policy_id',$policy->id)->get();
		foreach($productDetail as $productDetails) {
          $product_opt_name .= $productDetails->productOption->name .' | ';
        }
		
		return $product_opt_name;
	}
    /**
     * Show the form for editing the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer       = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');
            return redirect(route('admin.customers.index'));
        }
        $member_group = ClientMember::where('customer_id',$id)->first();
        $sub_divisions= SubDivision::where('id',$customer->sub_district)->first();
        $medical_history = MedicalHistory::where('customer_id',$id)->first();
        if($customer->customer_photo_id){
            $get_image      = $this->documentHelper->getDocument($customer->customer_photo_id);
            $customer_image = base64_encode($get_image);
        }

        $districts           = District::pluck('name','id');
        $educations          = AttributeType::where('code','education')->where('active',1)->first();
        $income_ranges       = AttributeType::where('code','income_range')->where('active',1)->first();
        $professional_status = AttributeType::where('code','professional_status')->where('active',1)->first();
        $occupations         = AttributeType::where('code','occupations')->where('active',1)->first();
        $blood_groups        = AttributeType::where('code','blood_group')->where('active',1)->first();
        $customer_types      = Config::get('insurance.customer_types');
        $phone_carriers      = Config::get('insurance.phone_carriers');
        $states              = Config::get('insurance.states');
        $countries           = Config::get('insurance.countries');
        $employer            = $customer->employer;
        $form_type           = 'customer';
        $member_groups       = '';
        $customer_type       = 'Primary';
        if(Sentinel::getUser()->group == 'Client'){
          $client_id         = Sentinel::getUser()->group_id;
          $client            = client::find($client_id); 
          $employer          = $client->client_name;
          $form_type         = 'member';
          $member_groups     = MemberGroups::where('status','1')->get();
          $customer_type     = 'Dependent';
        }
		
		
		$additional_details=CustomerAdditionalDetails::where('customer_id',$id)->get();
    $is_gender_display = 1;

        return view(env('DOMAINCODE').'.admin.customers.edit', compact('customer','form_type','customer_type','customer_types','phone_carriers','districts','states','countries','member_groups','employer','occupations','member_group','sub_divisions','medical_history','blood_groups','additional_details','is_gender_display'));
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
	
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('admin.customers.index'));
        }

        //$duplicateCustomerCheck =$this->duplicateCustomerCheck($request);

        //if($duplicateCustomerCheck == "pass"){

        $date_of_birth = date('Y-m-d', strtotime($request->input('date_of_birth')));
        $input         = array_merge($request->all(), ['date_of_birth' => $date_of_birth]);
        $customer_type = $request->input('customer_type');
        
        if($customer_type != 'Personal'){

            $input = array_merge($request->all(), ['first_name' => $request->input('business_name')]);
        }

        $customer = $this->customerRepository->update($input, $id);

        Flash::success('Customer updated successfully.');

        return redirect(route('admin.customers.index'));
        // }else{
        //     Flash::error('Customer Already Created');

        //     return redirect(route('admin.customers.index'));
        // }
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.customers.delete',['id'=>$id]);
          return View(env('DOMAINCODE').'.admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));
      }

       public function getDelete($id)
       {
		  
           $sample = Customer::destroy($id);
           // Redirect to the group management page
           return redirect(route('admin.customers.index'))->with('success', Lang::get('message.success.delete'));
       }
  public function getMemberDelete(Request $request,$id){
	 // echo "familyId".$request->familyId;die();
		$familyId=$request->familyId;
		$familyRelation=FamilyRelation::where('customer_id',$id)->where('family_id',$request->familyId)->delete();
		return $familyId;
  }

       /**
     * insert Identification detail of customer.
     *
     * @param : customer_id,id_type,id_number,
     * issuing_authority,place_of_issue,file,
     * issuing_country,date_of_issue,expiry_date,points
     * 
     * @return Response
     */
      public function insertIdentificationDetail(Request $request) {

        $data['customer_id']       = $request->input('customer_id');
        $data['id_type']           = $request->input('id_type');
        $data['id_number']         = $request->input('id_number');
        $data['issuing_authority'] = $request->input('issuing_authority');
        $data['place_of_issue']    = $request->input('place_of_issue');
        $data['issuing_country']   = $request->input('issuing_country');
        $data['date_of_issue']     = date('Y-m-d', strtotime($request->input('date_of_issue')));
        $data['expiry_date']       = date('Y-m-d', strtotime($request->input('expiry_date')));
        $data['points']            = $request->input('points');

        if ($data['customer_id'] == "") {
            Session::flash('message', 'Please Enter Customer Id');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }

        if ($data['id_number'] == "") {
            Session::flash('message', 'Please Enter Id Number');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }

        if ($data['id_type'] == "") {
            Session::flash('message', 'Please enter Id Type');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
        if ($data['issuing_authority'] == "") {
            Session::flash('message', 'Please enter Issuing Authority');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
        if ($data['place_of_issue'] == "") {
            Session::flash('message', 'Please enter place of issue');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
        if ($data['issuing_country'] == "") {
            Session::flash('message', 'Please enter issuing country');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
        if ($data['date_of_issue'] == "") {
            Session::flash('message', 'Please enter date of issue');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
        if ($data['expiry_date'] == "") {
            Session::flash('message', 'Please enter expiry date');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
        if ($data['expiry_date'] < $data['date_of_issue']) {
            Session::flash('message', 'Expiry Date should be grater than issue date');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
        $file = $request->file('file');
        if (empty($file)) {
            Session::flash('message', 'Please upload file');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }

        if($data['id_type'] != "Others"){

            $duplicate_customer_identity =  CustomerIdentity::select("id")
                                                ->where('id_number' , $data['id_number'])
                                                ->where('id_type' , $data['id_type'])
                                                ->count();
            if($duplicate_customer_identity > 0){

                Session::flash('message', 'This Customer Identification Already Exists in Existing Customer');
                Session::flash('alert-class', 'alert-danger');
                return redirect('app/customers/' . $data['customer_id']);
            }
        }

        if (is_uploaded_file($_FILES['file']['tmp_name'])){

            $extension        = $file->getClientOriginalExtension();
            $file_name        = $request->input('id_type').time().uniqid().'.'.$extension;
            $file             = file_get_contents($_FILES['file']['tmp_name']);
            $path             = "CUSTOMER_PATH"; 
            // $upload_document  = new DocumentHelper();
            $file_upload      = $this->documentHelper->uploadDocument($file,$file_name,$path,$data['customer_id']);

            if(isset($file_upload->document)){
                $data['document_id']   = $file_upload->document->uuid;
                $data['document_type'] = $extension;
                $result                = Customer::inserCustomertIdenty($data);
            } else {

                Session::flash('message', $file_upload);
                Session::flash('alert-class', 'alert-danger');
                return redirect('app/customers/' . $data['customer_id']);

            }
        } 
       
        if ($result) {
            Session::flash('message', 'Identification Details Saved Successfully');
            Session::flash('alert-class', 'alert-success');
            return redirect('app/customers/' . $data['customer_id']);
        } else {
            Session::flash('message', 'Identification Details not Saved something Went wrong');
            Session::flash('alert-class', 'alert-danger');
            return redirect('app/customers/' . $data['customer_id']);
        }
    }

        public function modalTest() {
            return View(env('DOMAINCODE').'.admin.customers.modal-test');
        }
       
        public function getCustomersApi(Request $request) {
           $modal = Customer::with('familyRelation')->where('family_head',1);
           return Datatables::eloquent($modal)
           ->editColumn('created_at',function($business) {
                return date('d-M-Y',strtotime($business->created_at)) ;
                
            })
           ->editColumn('date_of_birth',function($business) {
                return date('d-M-Y',strtotime($business->date_of_birth)) ;
                
            })
           ->addColumn('relationship', function (Customer $customer) {
              if ($customer) {
                if(isset($customer->familyRelation->relation)){
                  return $customer->familyRelation->relation->relationship_name ;
                }else{
                  return '';
                }
                
              }
          })
              ->filter(function($query) use ($request) {
                   if ($request->has('search.value')) {
                        $query->where(function($q) use ($request) {

                                $keyword = $request->input('search.value');
                                  
                                    $dateformat = date('Y-m-d',strtotime($keyword));
                                   
                                    $q->orWhere('first_name', 'like', "%{$keyword}%");
                                    $q->whereRaw("CONCAT(first_name,' ',second_name) like ?", ["%{$keyword}%"]);
                                    $q->orWhereRaw("CONCAT(first_name,' ',second_name,' ',third_name) like ?", ["%{$keyword}%"]);
                                    $q->orWhereRaw("CONCAT(first_name,' ',second_name,' ',third_name,' ',fourth_name) like ?", ["%{$keyword}%"]);
                                    $q->orWhereRaw("mobile_1 like ?", ["%{$keyword}%"]);
                                    $q->orWhereRaw("gender like ?",["%{$keyword}%"]);
                                    $q->orWhereRaw("date_of_birth like ?",["%{$dateformat}%"]);
                                    $q->orWhereRaw("created_at  like ?",["%{$dateformat}%"]);
                        });
                    }
            })
            // ->filterColumn('first_name', function($query, $keyword) {

            //         $keyword = strtoupper($keyword);
            //         $sql1    = "customers.first_name  like ?";
            //         $sql2    = "CONCAT(customers.first_name,' ',customers.second_name)  like ?";
            //         $sql3    = "CONCAT(customers.first_name,' ',customers.second_name, ' ',customers.third_name)  like ?";
            //         $sql4    = "CONCAT(customers.first_name,' ',customers.second_name, ' ',customers.third_name, ' ',customers.fourth_name)  like ?";

            //         $query->whereRaw($sql1, ["%{$keyword}%"]);
            //         $query->orWhereRaw($sql2, ["%{$keyword}%"]);
            //         $query->orWhereRaw($sql3, ["%{$keyword}%"]);
            //         $query->orWhereRaw($sql4, ["%{$keyword}%"]);
            //     })
            // ->filterColumn('date_of_birth', function($query, $keyword) {

            //         $keyword = strtotime($keyword);
            //         $keyword = date('Y-m-d',$keyword);
            //         $sql    = "date(customers.date_of_birth)  = ?";
            //         $query->whereRaw($sql, ["{$keyword}"]);
            // })

           ->toJson();
        }
       
        public function getCustomerApi($id) {
           $customer = Customer::with('familyRelation.relation')->find($id);
           return $customer;
        }       

        public function getMainPartial($id) {
            $customer = Customer::find($id);
            return View(env('DOMAINCODE').'.admin.customers.show_fields', compact('customer'));
        }       
       
        public function syncWithErp($id) {
           $erpHelper = new ErpHelper();
           $erpHelper->createCustomerInErp($id);
           Flash::success('Customer Sync with ERP successfully.');
           return 'Customer Sync with ERP successfully.';
        }

    public function popUp($id)
    {
        $customer = Customer::find($id);
            return View(env('DOMAINCODE').'.admin.customers.customer-main-partial', compact('customer'));
    }
    
    public function filterSubDivisions(Request $request)
    {
        $districtId = $request->input('districtId');
        $sub_divisions = SubDivision::where('DistrictId',$districtId)->select('id','name')->get();
        return $sub_divisions;
    }

    

    public function duplicateCustomerCheck(Request $request) {

        $first_name    = trim($request->input('first_name'));
        $second_name   = trim($request->input('second_name'));
        $third_name    = trim($request->input('third_name'));
        $fourth_name   = trim($request->input('fourth_name'));
        $gender        = $request->input('gender');
        $mobile_1      = $request->input('mobile_1');
        $mobile_2      = $request->input('mobile_2');

        if($request->input('date_of_birth')){
            $date_of_birth = date('Y-m-d', strtotime($request->input('date_of_birth')));
        }else{
            $date_of_birth = '';
        }
        
        $customer = ''; $customer1 = '';
        
        if ($first_name && $second_name && $third_name && $fourth_name && $gender && $date_of_birth) {

            $query = Customer::query();
            $query->where([
                ['first_name',    '=', $first_name],
                ['second_name',   '=', $second_name],
                ['third_name',    '=', $third_name],
                ['fourth_name',   '=', $fourth_name],
                ['date_of_birth', '=', $date_of_birth],
                ['gender',        '=', $gender]
               ]);

            $customer = $query->count();
            
        }
        
        if ($mobile_1) {
            
            $query     = Customer::query();
            $query     = $query->where('mobile_1', '=' ,$mobile_1);
            $customer1 = $query->count();
        }
       
        if($customer > 0 || $customer1 >0){

            return 'fail';
        } else {
            return 'pass';
        }

    }

    public function showCustomerDocuments($id)
    {

       $customer_identities = CustomerIdentity::where('customer_id',$id)->get(); 
       return View(env('DOMAINCODE').'.admin.customers.customer-identitication', compact('customer_identities'));
    }

    public function addInsuranceCustomer(Request $request)
    {
			$spouse_count=0;
		
				if($request->relation_id==2){
					$member    = Customer::where('id',$request->customer_id)->first();
					$family_id = $member->familyRelation->family_id;
					$member_count         = FamilyRelation::where('family_id',$family_id)->where('relation_id',2)->count();
					$spouse_count=$member_count;
				}else{
					$spouse_count=0;
				}
			$relationship           = Config::get('insurance.relationship');
			$spouseValue=$relationship['spouse'];
	//echo "<pre>";print_r($request->all());die();
        // $duplicate_customer_check = $this->duplicateCustomerCheck($request);

        // if($duplicate_customer_check == "pass"){
          if($spouse_count<$spouseValue){
            $date_of_birth                      = date('Y-m-d', strtotime($request->input('date_of_birth')));
            $customer                           = new Customer();
            $customer->first_name               = $request->input('first_name');
            $customer->second_name              = $request->input('second_name');
            $customer->third_name               = $request->input('third_name');
            $customer->fourth_name              = $request->input('fourth_name');
            $customer->date_of_birth            = $date_of_birth;
            $customer->age                      = Carbon::parse($date_of_birth)->age;
            $customer->gender                   = $request->input('gender');
            $customer->height                   = $request->input('height');
            $customer->weight                   = $request->input('weight');
            $customer->mobile_1                 = $request->input('mobile_1');
            // $customer->active                   = 0;  
            $customer->save();

            $family = Family::where('family_head_id',$request->customer_id)->first();

            $id              = $customer->id;
            $relation_id     = $request->input('relation_id');
            $family_relation = $this->addFamilyRelation($id,$family->id,$relation_id,null);
            $get_relation    = FamilyRelation::where('customer_id',$id)->first();
            $relation        = '';
            if(isset($get_relation->relation->relationship_name)){
              $relation      = $get_relation->relation->relationship_name;
            }
            
            if(isset($customer->id)){
                return ['result' => 'pass' ,  'data' => $customer,'relation' => $relation];
            }else{
                return ['result' => 'fail' ,  'data' => 'customer creation failed.'];
            }
		  }else{
			   return ['result' => 'fail' ,  'data' => 'Only allow '.$spouseValue.' spouse not more than '.$spouseValue.' spouse allowed'];
		  }
        // }else{
        //     return ['result' => 'fail' ,  'data' => 'Duplicate customer.'];
        // }
    }

    public function policyShow()
    {
       return View(env('DOMAINCODE').'.admin.customers.policy-show');
    }

    public function claimShow()
    {
       return View(env('DOMAINCODE').'.admin.customers.claim-show');
    }

    public function createFamily($customer)
    {
        $family                      = new Family();
        $family->family_head_id      = $customer->id;
        $family->family_name         = $customer->third_name;
        $family->status              = 'active';
        $family->created_by          = Sentinel::getUser()->id;
        $family->save();
        return $family;
    }

    public function getFamily($family_head_id)
    {
        $family = Family::where('family_head_id',$family_head_id)->first();
        return $family;
    }

    public function getCustomerFamilyRealtion($customer_id)
    {
        $family_relation = FamilyRelation::where('customer_id',$customer_id)->first();
        return $family_relation;
    }

    public function addCustomerAddress($customer_id,Request $request)
    {
        $customer_address                  = new CustomerAddress();
        $customer_address->customer_id     = $customer_id;
        $customer_address->address_line_1  = $request->input('address_line_1');
        $customer_address->address_line_2  = $request->input('address_line_2');
        $customer_address->district        = $request->input('district');
        $customer_address->sub_district    = $request->input('sub_district');
        $customer_address->state           = $request->input('state');
        $customer_address->postal_code     = $request->input('postal_code');
        $customer_address->save();

        return $customer_address;
    }

    public function addFamilyRelation($customer_id,$family_id,$relation_id,$extension_number)
    {
        $family_relation                      = new FamilyRelation();
        $family_relation->customer_id         = $customer_id;
        $family_relation->family_id           = $family_id;
        $family_relation->relation_id         = $relation_id;
        $family_relation->relation_start_date = date('Y-m-d');
        //$family_relation->family_code         = $extension_number;
        $family_relation->relation_status     = 'active';
        $family_relation->created_by          = Sentinel::getUser()->id;
        $family_relation->save();
        return $family_relation;
    }

    public function storeOrUpdateCustomer($request,$update_customer=null)
    {
		//echo "update_customer".$update_customer;die();
        if ($update_customer) {
          $customer = Customer::find($update_customer);
        }else {
          $customer = new Customer();
		  $customer->gender                   = $request->input('gender');
        }
        $date_of_birth  = date('Y-m-d', strtotime($request->input('date_of_birth')));
        $date_of_joining = "";
        if($request->input('date_of_joining')){
          $date_of_joining  = date('Y-m-d', strtotime($request->input('date_of_joining')));
        }

        if(!empty($request->input('employer_id')) && $request->input('employer_id') != null){    $employer_id=$request->input('employer_id'); }else{ $employer_id = ''; }

        $customer->first_name               = $request->input('first_name');
        $customer->second_name              = $request->input('second_name');
        $customer->third_name               = $request->input('third_name');
        $customer->fourth_name              = $request->input('fourth_name');
        $customer->date_of_birth            = $date_of_birth;
        $customer->blood_group              = $request->input('blood_group');
        $customer->age                      = Carbon::parse($date_of_birth)->age;       
        $customer->customer_type            = $request->input('customer_type');
        $customer->height                   = $request->input('height');
        $customer->weight                   = $request->input('weight');
        $customer->email                    = $request->input('email');
        $customer->phone_carrier_1          = $request->input('phone_carrier_1');
        $customer->phone_carrier_2          = $request->input('phone_carrier_2');
        $customer->mobile_1                 = $request->input('mobile_1');
        $customer->mobile_2                 = $request->input('mobile_2');
        $customer->address_line_1           = $request->input('address_line_1');
        $customer->district                 = $request->input('district1');
        $customer->sub_district             = $request->input('sub_district1');
        $customer->state                    = $request->input('state');
        $customer->occupation               = $request->input('occupation');
        $customer->employer                 = $request->input('employer');
        $customer->country                  = $request->input('country');
        $customer->family_head              = $request->input('family_head');
        $customer->employer_id              = $employer_id;
        $customer->date_of_joining          = $date_of_joining;
        $customer->save();
        
        
        return $customer;
    }
   
    public function addMedicalHistory($request,$customer_id,$update_customer=null)
    {
        //add medical history
        $medical_history                      = new MedicalHistory();
        if ($update_customer) {
          $medical_details                      = MedicalHistory::where('customer_id',$update_customer)->first();
          if($medical_details){
            $medical_history  = MedicalHistory::find($medical_details->id);
          }
        } 
        $medical_history->customer_id         = $customer_id;
        $medical_history->medications         = ($request->medication=='Yes') ? 1 : (($request->medication=='No') ? 0 : 'NA');
        $medical_history->current_medications = ($request->current_medication=='Yes') ? 1 : (($request->current_medication=='No') ? 0 : 'NA');
        $medical_history->surgery             = ($request->surgical_history=='Yes') ? 1 : (($request->surgical_history=='No') ? 0 : 'NA');
        $medical_history->tobacco_usage       = ($request->tobacco=='Yes') ? 1 : (($request->tobacco=='No') ? 0 : 'NA');
        $medical_history->drugs_usage_history = ($request->illegal_drugs=='Yes') ? 1 : (($request->illegal_drugs=='No') ? 0 : 'NA');
		
		$medical_history->blood_pressure = ($request->blood_pressure=='Yes') ? 1 : (($request->blood_pressure=='No') ? 0 : 'NA');
		$medical_history->blood_circulatory = ($request->blood_circulatory=='Yes') ? 1 : (($request->blood_circulatory=='No') ? 0 : 'NA');
		$medical_history->respiratory_disorders = ($request->respiratory_disorders=='Yes') ? 1 : (($request->respiratory_disorders=='No') ? 0 : 'NA');
		$medical_history->neurological_disorders = ($request->neurological_disorders=='Yes') ? 1 : (($request->neurological_disorders=='No') ? 0 : 'NA');
		$medical_history->related_problem	 = ($request->related_problem=='Yes') ? 1 : (($request->related_problem=='No') ? 0 : 'NA');
		$medical_history->eye_disorders = ($request->eye_disorders=='Yes') ? 1 : (($request->eye_disorders=='No') ? 0 : 'NA');
		$medical_history->gynecological_disorders = ($request->gynecological_disorders=='Yes') ? 1 : (($request->gynecological_disorders=='No') ? 0 : 'NA');
		$medical_history->kidney_disorders = ($request->kidney_disorders=='Yes') ? 1 : (($request->kidney_disorders=='No') ? 0 : 'NA');
		$medical_history->musuloskeletal_disorders = ($request->musuloskeletal_disorders=='Yes') ? 1 : (($request->musuloskeletal_disorders=='No') ? 0 : 'NA');
		$medical_history->endocrine_disease = ($request->endocrine_disease=='Yes') ? 1 : (($request->endocrine_disease=='No') ? 0 : 'NA');
		$medical_history->surgical = ($request->surgical=='Yes') ? 1 : (($request->surgical=='No') ? 0 : 'NA');
		$medical_history->other_diseases = ($request->other_diseases=='Yes') ? 1 : (($request->other_diseases=='No') ? 0 : 'NA');
		$medical_history->Caesarean = ($request->Caesarean=='Yes') ? 1 : (($request->Caesarean=='No') ? 0 : 'NA');

		$medical_history->pregnant_status = ($request->pregnant_status=='Yes') ? 1 : (($request->pregnant_status=='No') ? 0 : 'NA');

		$medical_history->pregnant_detail = ($request->pregnant_detail) ? $request->pregnant_detail : '';
		
		$medical_history->drug_allergic = ($request->drug_allergic=='Yes') ? 1 : (($request->drug_allergic=='No') ? 0 : 'NA');
		$medical_history->drug_details = ($request->drug_details) ? $request->drug_details : '';
		$medical_history->medical_insurance = ($request->medical_insurance=='Yes') ? 1 : (($request->medical_insurance=='No') ? 0 : 'NA');
		$medical_history->medical_insurance_detail = ($request->medical_insurance_detail) ? $request->medical_insurance_detail : '';



        // $medical_history->alcohol             = $request->alcohol;
        $medical_history->save();

        return $medical_history;
    }
	public function additonalCustomerDetails($request,$customer_id,$update_customer=null){     
		$additional_items = json_decode($request->additional_items);
		//if($additional_items){
			CustomerAdditionalDetails::where('customer_id',$customer_id)->delete();

		//}
		if(isset($additional_items)){
		foreach($additional_items as $row){
		$additonalCustomerDetails                       = new CustomerAdditionalDetails();
		$additonalCustomerDetails->customer_id			= $customer_id;
		// $additonalCustomerDetails->applicant_name		= $row->applicant_name;
		$additonalCustomerDetails->disorder				= $row->disorder;
		$additonalCustomerDetails->date_diagnosed		= date('Y-m-d',strtotime($row->dateDiagnosed));
		$additonalCustomerDetails->doctor_address		= $row->doctorAddress;
		$additonalCustomerDetails->current_status		= $row->currentStatus;
		$additonalCustomerDetails->save();
		}
		}
	}
    public function uploadCustomerImage($request,$customer)
    {
        //upload photo
        $image_type      = 'png';
		
        $customer_image  = ($request->input('customer_image')) ? $request->input('customer_image') : $request->input('customer_old_image');
		if($request->input('customer_image')!=""){
			$customer_image=$request->input('customer_image');
		//	echo "asdasd";die();
        $image           = base64_decode($customer_image);
        $file_name       = time().$customer->id.$customer->first_name.'.'.$image_type;
        $path            = "CUSTOMER_PATH"; 
        $file_upload     = $this->documentHelper->uploadDocument($image,$file_name,$path,$customer->id);

        if(isset($file_upload->document)){
            $customer->customer_photo_id   = $file_upload->document->uuid;
            $customer->customer_photo_type = $image_type;
            $customer->update();
            return $customer;
        }
		}
        return false;
    }

    public function addFamilyMember($family_head_id,Request $request)
    {

        $customer = $this->customerRepository->findWithoutFail($family_head_id);
		//echo "<pre>";print_r($customer);die();

        $family_head = $this->customerRepository->findWithoutFail($family_head_id);
        if (empty($family_head)) {
            Flash::error('Customer not found');
            return redirect(route('admin.customers.index'));
        }

        $districts           = District::get();
        $educations          = AttributeType::where('code','education')->where('active',1)->first();
        $income_ranges       = AttributeType::where('code','income_range')->where('active',1)->first();
        $professional_status = AttributeType::where('code','professional_status')->where('active',1)->first();
        $occupations         = AttributeType::where('code','occupations')->where('active',1)->first();
        $blood_groups        = AttributeType::where('code','blood_group')->where('active',1)->first();
        $customer_types      = Config::get('insurance.customer_types');
        $phone_carriers      = Config::get('insurance.phone_carriers');
        $states              = Config::get('insurance.states');
        $countries           = Config::get('insurance.countries');
        $relationships       = Relationship::where('id', '!=' , 1)->where('active', 1)->get();
        $sub_divisions       = SubDivision::get();
        $form_type           = 'customer';
        if(Sentinel::getUser()->group == 'Client'){
          $form_type         = 'member';
        }
		$family_customer_id='';
        return view(env('DOMAINCODE').'.admin.customers.add-family-member',compact('customer','districts','educations','income_ranges','professional_status','occupations','customer_types','phone_carriers','states','countries','family_head','relationships','sub_divisions','form_type','blood_groups','family_customer_id'));
    }

    public function getCustomerNomineeDetails(Request $request,$id) {

      $policy_member  = PolicyMemberItem::where('policy_id',$id)->where('reference_type','nominee')->get();

      $nominee_members = [];

      foreach ($policy_member as $key => $value) {
        if($value){
          $customerid = $value->reference_id;
          $nominee_members[] = Customer::where('id',$customerid)->first();
        }
      }
      
      return Datatables::of($nominee_members)->toJson();
                 
                //->filterColumn('first_name', function($query, $keyword) {

                //           $keyword = strtoupper($keyword);
                //           $sql1    = "customers.first_name  like ?";
                //           $sql2    = "CONCAT(customers.first_name,' ',customers.second_name)  like ?";
                //           $sql3    = "CONCAT(customers.first_name,' ',customers.second_name, ' ',customers.third_name)  like ?";
                //           $sql4    = "CONCAT(customers.first_name,' ',customers.second_name, ' ',customers.third_name, ' ',customers.fourth_name)  like ?";

                //           $query->whereRaw($sql1, ["%{$keyword}%"]);
                //           $query->orWhereRaw($sql2, ["%{$keyword}%"]);
                //           $query->orWhereRaw($sql3, ["%{$keyword}%"]);
                //           $query->orWhereRaw($sql4, ["%{$keyword}%"]);
                //           $query->orWhereRaw($sql4, ["%{$keyword}%"]);
                //       })
                 
    }
    

  public function getClaimDetails(Request $request,$id) {

    $family_relation      = FamilyRelation::where('customer_id',$id)->first();
    $claim_details        = "";

    if($family_relation){
      $all_members_id    = FamilyRelation::where('family_id',$family_relation->family_id)->pluck('customer_id');
      //$all_members_id        = FamilyRelation::pluck('customer_id');
      if($all_members_id){
        $claim_details   = ClaimApplication::with(['customer','serviceProvider'])->whereIn('customer_id',$all_members_id);
		if ($claim_details) {			
			return Datatables::eloquent($claim_details)
				->addColumn('serviceProvider_name', function (ClaimApplication $claim_applications) {
					if ($claim_applications->serviceProvider) {
						return $claim_applications->serviceProvider->name;
					}else{
						return "";
					}
				})
				->addColumn('customers.first_name', function (ClaimApplication $claim_applications) {
					if (isset($claim_applications->customer->full_name)) {
						return $claim_applications->customer->full_name;
					}else{
						return " ";
					}
				})
			->toJson();
		}
      } else {
			return Datatables::of([])->toJson();
	  }		  
		  
    } else {
		return Datatables::of([])->toJson();
	}
   
  }

   public function getCustomerAgainstMembers($id)
   {

      $family_relation  = FamilyRelation::where('customer_id',$id)->first();
      $customer_id      = $id;
      if(isset($family_relation->family_id)){
        $customer_id      = FamilyRelation::where('family_id',$family_relation->family_id)->pluck('customer_id');
      }
      
      $family_members   = Customer::with(['familyRelation.relation'])->whereIn('id',$customer_id);

      return Datatables::eloquent($family_members)

     
                ->editColumn('date_of_birth', function (Customer $customer) {
                    if ($customer) {
                      return isset($customer->date_of_birth) ? date('d-M-Y', strtotime($customer->date_of_birth)) :"" ;
                    }
                })
               ->addColumn('relationship', function (Customer $customer) {
                  if ($customer) {
                    return isset($customer->familyRelation->relation->relationship_name) ? $customer->familyRelation->relation->relationship_name :"" ;
                  }
              })
			   ->addColumn('mobile_1', function (Customer $customer) {
                  if ($customer) {
                    return isset($customer->mobile_1) ? $customer->mobile_1 :"" ;
                  }else{
					  return "";
				  }
              })
                 
                ->filterColumn('first_name', function($query, $keyword) {

                          $keyword = strtoupper($keyword);
                          $sql1    = "customers.first_name  like ?";
                          $sql2    = "CONCAT(customers.first_name,' ',customers.second_name)  like ?";
                          $sql3    = "CONCAT(customers.first_name,' ',customers.second_name, ' ',customers.third_name)  like ?";
                          $sql4    = "CONCAT(customers.first_name,' ',customers.second_name, ' ',customers.third_name, ' ',customers.fourth_name)  like ?";

                          $query->whereRaw($sql1, ["%{$keyword}%"]);
                          $query->orWhereRaw($sql2, ["%{$keyword}%"]);
                          $query->orWhereRaw($sql3, ["%{$keyword}%"]);
                          $query->orWhereRaw($sql4, ["%{$keyword}%"]);
                          $query->orWhereRaw($sql4, ["%{$keyword}%"]);
                      })
                  ->rawColumns(['first_name','relationship'])
                 ->toJson();
    }

    public function editFamilyMember($id)
    {

      $customer       = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');
            return redirect(route('admin.customers.index'));
        }
      $districts           = District::get();
      $educations          = AttributeType::where('code','education')->where('active',1)->first();
      $income_ranges       = AttributeType::where('code','income_range')->where('active',1)->first();
      $professional_status = AttributeType::where('code','professional_status')->where('active',1)->first();
      $occupations         = AttributeType::where('code','occupations')->where('active',1)->first();
       $blood_groups        = AttributeType::where('code','blood_group')->where('active',1)->first();
      $customer_types      = Config::get('insurance.customer_types');
      $phone_carriers      = Config::get('insurance.phone_carriers');
      $states              = Config::get('insurance.states');
      $countries           = Config::get('insurance.countries');
      $relationships       = Relationship::where('id', '!=' , 1)->where('active', 1)->get();
      $sub_divisions       = SubDivision::get();
      $form_type           = 'customer';
      if(Sentinel::getUser()->group == 'Client'){
        $form_type         = 'member';
      }

      $relation_id  = '';
      if(isset($customer->familyRelation->relation)){
        $relation_id  = $customer->familyRelation->relation->id;
      }
      $customer_image = '';
      if($customer->customer_photo_id){
          $get_image      = $this->documentHelper->getDocument($customer->customer_photo_id);
          $customer_image = base64_encode($get_image);
      }
	  $additional_details=CustomerAdditionalDetails::where('customer_id',$id)->get();
      $medical_history = MedicalHistory::where('customer_id',$customer->id)->first();
	  $family_customer_id=$customer->id;
    $is_edit = 1;
    $is_gender_display = 1;
      return view(env('DOMAINCODE').'.admin.customers.edit-family-member',compact('districts','educations','income_ranges','professional_status','occupations','customer_types','phone_carriers','states','countries','customer','relationships','sub_divisions','form_type','blood_groups','relation_id','customer_image','medical_history','additional_details','family_customer_id','is_edit','is_gender_display'));
    }
	
	public function getMedicalAdditionDetails(Request $request,$id){
		$additional_details=CustomerAdditionalDetails::where("customer_id",$id)->get();
		if(isset($additional_details)){
			$result=["result"=>$additional_details];
		}
		echo json_encode($result);
	}
	public function validateEmployeeId(Request $request){
		//echo $request->member_group_id."employeer".$request->employer_id;
		$customer_detail=Customer::where('employer_id',$request->employer_id)->get();
		$result='';$value=false;
		if(isset($customer_detail)){
			 if(Sentinel::inRole('insurance-officer')){
				 foreach($customer_detail as $row){
					$client_member_detail=ClientMember::where('customer_id',$row->id)->where('client_id',$request->member_group)->first();
					if($client_member_detail){
					 $value=true;
					}
				 }
				
			 }else{
				foreach($customer_detail as $row){
				 $client_member_detail=ClientMember::where('customer_id',$row->id)->where('client_id',$request->member_group_id)->first();
					if($client_member_detail){
					 $value=true;
					}
				}
			 }
			//echo "<pre>";print_r($value);die();
			if($value==true){
				$result=["result"=>"Fail"];
			}else{
				$result=["result"=>"Pass"];	
			}
		}
		echo json_encode($result);
	}
	public function checkMobileExist(Request $request){
		$customer_detail=Customer::where('mobile_1',$request->mobile)->get()->toArray();
		if(empty($customer_detail)){
			$result=["result"=>"Pass"];
		}else{
			$result=["result"=>"Fail"];
		}
		echo json_encode($result);
	}
}
