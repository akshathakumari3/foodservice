<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\PremiumType;
use DB;
class AddonExport implements FromCollection,WithHeadings
{
    private $headings = ['lineOfBusiness','Products','Premium Type','Type','Currency','Sum Insured','Premium'];
    function __construct($id) {
        $this->id = $id;
       
    }
    public function collection()
    {
        
        $data = DB::table('premium_types')
            ->where('premium_types.id',$this->id)
            ->join('line_of_business', 'line_of_business.id', '=', 'premium_types.line_of_business_id')
            ->join('products', 'products.id', '=', 'premium_types.product_id')
            ->select('line_of_business.name as lob','products.name','premium_type','type','currency','sum_insured','premium')
            ->get();
        return collect($data);
    }

    public function headings() : array
    {
        return $this->headings;
    }
    
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; 
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(20);
            },
        ];
    }

}
