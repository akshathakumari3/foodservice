<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\WorkflowController;
use Log;
use Sentinel;
class RejectNotary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RejectNotary:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Reject Notary Cron Start");

        $WorkflowController     = new WorkflowController();
        $rejectNotary           = $WorkflowController->getNotaryApplication();

        Log::info("Reject Notary Cron Stop");
    }
}
