<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Insurance\InvoiceController;
use Log;

class InvoiceGenerateCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoicegenerate:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invoice generate per year each tenure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		Log::info("Invoice Generates Cron Started");
		$invoiceController=new InvoiceController();
		$invoiceController->invoiceGenerate();
		Log::info("Invoice Generates Cron Stop");
    }
}
