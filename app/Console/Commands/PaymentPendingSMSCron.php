<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\WorkflowController;
use Log;

class PaymentPendingSMSCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PaymentPendingSMS:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Payment Pending SMS For Customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        Log::info("Payment Pending SMS Cron Start");

        $WorkflowController     = new WorkflowController();
        $rejectNotary           = $WorkflowController->paymentPendingSMS();

        Log::info("Payment Pending SMS Cron Stop");
    }
}
