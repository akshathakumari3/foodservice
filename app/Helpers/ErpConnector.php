<?php

namespace App\Helpers;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;
use Cache;
use Carbon\Carbon;
use Exception;
use App\Models\ApiLog;
use App\Models\ErpPayment;
use App\Models\ModeOfPayments;
use App\Models\ErpSerielRefNumbers;
use App\Models\ErpItemGroup;
use App\Models\ErpItem;
use App\Models\ErpItemPrice;

class ErpConnector {
    
    protected static $CSRF_TOKEN = null;
    protected static $SERVICE_FEE_CACHE_KEY = "erp.service";
    protected static $CSRF_TOKEN_CACHE_KEY = "erp.csrftoken";
    protected static $LOGIN_CACHE_KEY = "erp.login";
    protected static $CACHE_EXPIRY_TIME = 10; // to be moved to settings later 

    public function __construct() {
        Log::info('Initializing ErpConnector');
        $this->login();
    }
    
    public function login() {
        Log::info('Erp Login ');
        if (Cache::has(self::$CSRF_TOKEN_CACHE_KEY)) {
            Log::info('Erp Login Params from Cache');
            self::$CSRF_TOKEN = Cache::get(self::$CSRF_TOKEN_CACHE_KEY);
            $content = Cache::get(self::$LOGIN_CACHE_KEY);
            return $content;                    
        }

        $startTime = date_create();
        
        $response = Curl::to($this->getApiUrl("ERP_LOGIN_API", ""))
            ->withData( array('usr'=>env("ERP_LOGIN_USER"),'pwd'=>env("ERP_LOGIN_PASSWORD")) )
            ->returnResponseObject()
            ->withResponseHeaders()
            ->setCookieJar("erpcookie.txt")
            ->post();        

        Log::info('Erp Login Execution Time : ' . date_diff($startTime, date_create())->format("%I:%S"));
        $content = $response->content;
        $headers = $response->headers;
        
        self::$CSRF_TOKEN = self::initCsrfToken();
        Cache::put(self::$CSRF_TOKEN_CACHE_KEY, self::$CSRF_TOKEN, Carbon::now()->addMinutes(self::$CACHE_EXPIRY_TIME));        
        Cache::put(self::$LOGIN_CACHE_KEY, $content, Carbon::now()->addMinutes(self::$CACHE_EXPIRY_TIME));        
        //var_dump($content);
        //var_dump($headers["Set-Cookie"]); 
        return $content;
    }

    public function getDefaultTax() {  
        
        // return the default tax item        
    }

    
    public function getCurrencyExchange()
    {
        $currecy_exchage     = env('CURRENCY_EXCHANGE_NAME');
        $getCurrencyExchange = $this->getApiUrl("ERP_CURRENCY_EXCHANGE_API", rawurlencode($currecy_exchage));
        $getCurrencyExchange = $this->doGet($getCurrencyExchange); 
        return $getCurrencyExchange;
    }
        
    public function getServiceFee($serviceId) {             
        
        $productBundleUrl = $this->getApiUrl("ERP_BUNDLEFEE_API", rawurlencode($serviceId));
        $productBundle = $this->doGet($productBundleUrl); 

        if(isset($productBundle->name)){
            $itemcodes = [];
                foreach ($productBundle->items as $bundleitem) {
                    arraY_push($itemcodes, $bundleitem->item_code);
                }

                $filters = urlencode('"' . implode ('"," ', $itemcodes) . '"');
                $params = 'fields=[%22standard_rate%22,%22name%22,%22item_code%22,%22description%22,%22stock_uom%22]&filters=[[%22name%22,%22in%22,[' . $filters . ']]]';
                $feeitemsurl = $this->getApiUrl("ERP_ITEMFEE_API", $params);
                $feeitems = $this->doGet($feeitemsurl); 
                Cache::put(self::$SERVICE_FEE_CACHE_KEY, $feeitems,  Carbon::now()->addMinutes(self::$CACHE_EXPIRY_TIME));            
                return $feeitems;  
        }else{
            $feeitems = [];
            return $feeitems;
        }
    }
    
    public function createPayment($model) {
        $result = self::doPost(self::getApiUrl("ERP_PAYMENT_API"), json_encode($model));
        return $result;
    }    
    
    public function createInvoice($model) {
        $result = self::doPost(self::getApiUrl("ERP_INVOICE_API"), json_encode($model));
        return $result;
    }

    public function updateInvoice($name, $data) {

        $url    = self::getApiUrl("ERP_INVOICE_API") . rawurlencode($name);
        $result = self::doPut($url, json_encode($data));
        return $result;
    }
    
    public function createCustomer($erpcustomer) {
        $result = self::doPost(self::getApiUrl("ERP_CUSTOMER_API"), json_encode($erpcustomer));
        return $result;
    }
         
    public function updateCustomer($erpid, $erpcustomer) {
        $url = self::getApiUrl("ERP_CUSTOMER_API") . rawurlencode($erpid);
        $result = self::doPut($url, json_encode($erpcustomer));
        return $result;
    }

    public function getCustomer($name) {
        $url    = self::getApiUrl("ERP_CUSTOMER_API") . rawurlencode($name);
        $result = self::doGet($url);
        return $result;
    }

    public function searchCustomerPaymentHistory($data)
    {

        $erpModel  = new ErpPayment();
        $url       = $this->getApiFilterByMethod($erpModel, $data);
        $payments  = $this->doGet($url);
        return  $payments;
        
    }

    public function getDocument($params)
    {

        $url    = self::getApiUrl("GET_DEFAULT_DOCUMENT").$params;
        $result = $this->doGetAPI($url);
        return $result;
    }

    public function modeOfPayments($filters)
    {
        $erpModel       = new ModeOfPayments();
        $url            = $this->getApiFilterByMethod($erpModel,$filters);
        $modeOfPayments = $this->doGet($url);
        return $modeOfPayments;
    }

    public function getDefaultAccount($name)
    {
        $url    = self::getApiUrl("MODEOFPAYMENTS").$name;
        $result = $this->doGetAPI($url);
        return $result;
    }

    public function getSerielRefNumbers($filters)
    {
        $erpModel       = new ErpSerielRefNumbers();
        $url            = $this->getApiFilterByMethod($erpModel,$filters);
        $modeOfPayments = $this->doGet($url);
        return $modeOfPayments;
    }

    public function updateSerialRefNumber($name, $data) {
        $url    = self::getApiUrl("GETSERIELREFNUMBERS") . rawurlencode($name);
        $result = self::doPut($url, json_encode($data));
        return $result;
    }

    public function getSalesInvoice($name) {
        $url    = self::getApiUrl("ERP_INVOICE_API") . rawurlencode($name);
        $result = self::doGet($url);
        return $result;
		
    }

    
    public function submitErpInvoice($name, $data) {
        $url    = self::getApiUrl("ERP_INVOICE_API") . rawurlencode($name);
        $result = self::doPut($url, json_encode($data));
        return $result;
    }

    
    public function itemPriceList($serviceFees,$currency) {

        $itemcodes = [];
        foreach ($serviceFees as $serviceFee) {
            arraY_push($itemcodes, $serviceFee->item_code);
        }

        $filters        = urlencode('"' . implode ('"," ', $itemcodes) . '"');
        $filters        = '[["item_code","in",[' . $filters . ']],["currency","=","'.$currency.'"]]';
        $erpModel       = new ErpItemPrice();
        $url            = $this->getApiFilterByMethod($erpModel,$filters);
        $itemPriceList  = $this->doGet($url);
        return $itemPriceList;
    }

    public function getPayment($name) {
        $url    = self::getApiUrl("ERP_PAYMENT_API") . rawurlencode($name);
        $result = self::doGet($url);
        return $result;
    }

    public function getApiFilterByMethod($erpModel, $fields) {

        $calledMethodname = debug_backtrace()[1]['function']; 
        $params = '?limit_page_length=' . $erpModel->count . '&filters=' . $fields. '&fields=' . $erpModel->searchOutputFields;
        $apiname = strtoupper($calledMethodname);  
        $api = env("ERP_HOST") . env($apiname) . $params;
      

        return $api;
    }

    public function filterItemGroups($filter)
    {
        $erpModel          = new ErpItemGroup();
        $params            = $this->getFilters($erpModel, $filter);
        $api               = env('ERP_ITEM_GROUP');
        $url               = env("ERP_HOST") . $api . $params;
        $result            = $this->doGet($url);
        return $result;
    }

    public function filterItems($filter)
    {
        $erpModel          = new ErpItem();
        $params            = $this->getFilters($erpModel, $filter);
        $api               = env('ERP_ITEM_API');
        $url               = env("ERP_HOST") . $api . $params;
        $result            = $this->doGet($url);
        return $result;
    }

    public function filterItemsPriceList($filter) {
        $erpModel          = new ErpItemPrice();
        $params            = $this->getFilters($erpModel, $filter);
        $api               = env('ITEMPRICELIST');
        $url               = env("ERP_HOST") . $api . $params;
        $result            = $this->doGet($url);
        return $result;
    }

    public function taxAndCharges($name) {
        
        $url    = self::getApiUrl("SALES_TAXES_AND_CHARGES_TEMPLATE") . rawurlencode($name);
        $result = self::doGet($url);
        return $result;
    }

    public function billNumberDuplicateCheck($filter) {
        $erpModel          = new ErpPayment();
        $params            = $this->getFilters($erpModel, $filter);
        $api               = env('ERP_PAYMENT_API');
        $url               = env("ERP_HOST") . $api . $params;
        $result            = $this->doGet($url);
        return $result;
    }

    

    public function getFilters($erpModel, $params)
    {
        $show_fields       = '?limit_page_length=' . $erpModel->count .'&fields=' . $erpModel->searchOutputFields . '&filters=' . $params;
        return $show_fields;
    }

    public function getErpList($limit, $offset, $order_by,$erpModel,$api,$filter = null)
    {   

        $params = '?limit_page_length=' . $limit . '&limit_start=' . $offset . '&order_by=' . $order_by . '&filters=' . $filter. '&fields=' . $erpModel->searchOutputFields;
        $url      = env("ERP_HOST") . $api . $params;
        return $url;
    }

    
    public function doGet($url) {         
        Log::info('Invoking GET : ' . $url);        
        $startTime = date_create();
        $apilog = ApiLog::addApiLogRequest("GET", $url);                
        $response = Curl::to($url)
            ->setCookieFile("erpcookie.txt")        
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->get();        
        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));
        
        
        Log::info('Erp Get Execution Time for : ' . date_diff($startTime, date_create())->format("%I:%S"));
        if ($response->status == 200){
            $content = json_decode($response->content);
            $headers = $response->headers;         
            return $content->data; 
        } else {
            //throw new Exception("Response Code :" . $response->status . ", Message: " . json_decode($response));
            return $response;
        }
    }

    public function doGetAPI($url) { 
        Log::info('Invoking GetAPI : ' . $url); 
        $startTime = date_create();
        $apilog    = ApiLog::addApiLogRequest("GET", $url);
        $response  = Curl::to($url)
            ->setCookieFile("erpcookie.txt")        
            ->returnResponseObject()
            ->withResponseHeaders()
            ->withTimeout(1000) 
            ->withOption('FAILONERROR', true)
            ->withOption('VERBOSE', true)           
            ->get();

        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));
        Log::info('Erp Get Execution Time for : ' . date_diff($startTime, date_create())->format("%I:%S"));
        if ($response->status == 200){

            return json_decode($response->content);
            
        } else {

            return $response;
            //throw new Exception("Response Code :" . $response->status . ", Message: " . json_decode($response));
        }
    }

    public function doGetErpAPI($url) { 
        Log::info('Invoking GetAPI : ' . $url); 
        $apilog    = ApiLog::addApiLogRequest("GET", $url);
        $startTime = date_create();

        $response  = Curl::to($url)
            ->setCookieFile("erpcookie.txt")        
            ->returnResponseObject()
            ->withResponseHeaders()
            ->withTimeout(1000) 
            ->withOption('FAILONERROR', true)
            ->withOption('VERBOSE', true)           
            ->get();

        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));

        Log::info('Erp Get Execution Time for : ' . date_diff($startTime, date_create())->format("%I:%S"));
        if ($response->status == 200){

            return $response->content;
            
        } else {

            return $response;
            //throw new Exception("Response Code :" . $response->status . ", Message: " . json_decode($response));
        }
    }
    
    public function doPost($url, $data = null) {
              

        Log::info('Invoking POST : ' . $url); 
        Log::info('Invoking POST data: ' . $data);        

        $startTime = date_create();
        $apilog = ApiLog::addApiLogRequest("POST", $url, json_encode([ 'data' => $data ]));  
             
        $response = Curl::to($url)
            ->setCookieFile("erpcookie.txt")   
            ->withData( array( 'data' => $data ) )
            ->withHeader('X-Frappe-CSRF-Token: ' . self::$CSRF_TOKEN)
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->post(); 
                   
        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));
        
        Log::info("Erp Post Execution Time for ".$url. " : " . date_diff($startTime, date_create())->format("%I:%S"));
       
        if ($response->status == 200){
            $content = json_decode($response->content);
            $headers = $response->headers;         
            return $content->data; 
        } else {
          //  throw new Exception("Response Code :" . $response->status . ", Message: " . json_decode($response));
           
           $error_response = json_decode( json_encode($response), true);
           Log::info('Invoking POST : ' . $url);
           log::info("Response Code :" . $response->status );
           log::info(print_r($error_response,true));
            return $response;
        }
    }

    public function doPostAPI($url, $data=null) { 

        Log::info('Invoking PostAPI : ' . $url);
        Log::info('data : ' . $data);
        $startTime = date_create();
        $apilog = ApiLog::addApiLogRequest("POST", $url, json_encode([ 'data' => $data ])); 
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_COOKIEFILE, 'erpcookie.txt');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
       
        curl_close ($ch);

        Log::info('Erp Get Execution Time for : ' . date_diff($startTime, date_create())->format("%I:%S"));
        
      // return $response = json_decode($response);
        $apilog->addApiLogResponse(json_encode(json_decode($response, true)));
        if (isset($response->message)){
            
            return $response;
            
        } else {

            return $response;
            //throw new Exception("Response Code :" . $response->status . ", Message: " . json_decode($response));
        }
    }
	public function cancelInvoice($id, $data) {

        $url    = self::getApiUrl("ERP_INVOICE_API") . rawurlencode($id);
        $result = self::doPut($url, json_encode($data));
        return $result;
    }
	
    public function doPostParamAPI($url, $data=null) { 

        Log::info('Invoking PostAPI : ' . $url);
        Log::info('data : ' . $data);
        $startTime = date_create();
        $apilog    = ApiLog::addApiLogRequest("POST", $url, json_encode([ 'data' => $data ]));
        $ch        = curl_init();

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_COOKIEFILE, 'erpcookie.txt');
        curl_setopt($ch, CURLOPT_POST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);

        $apilog->addApiLogResponse(json_encode(json_decode($response, true)));


        Log::info('Erp Get Execution Time for : ' . date_diff($startTime, date_create())->format("%I:%S"));

        return $response;
        
    }
    
    public function doPut($url, $data = null) {
        Log::info('Invoking PUT : ' . $url);
        Log::info('PUT data : ' . $data);        
        $startTime = date_create();
        $apilog = ApiLog::addApiLogRequest("PUT", $url, json_encode([ 'data' => $data ]));                                
        $response = Curl::to($url)
            ->setCookieFile("erpcookie.txt")   
            ->withData( array( 'data' => $data ) )
            ->withHeader('X-Frappe-CSRF-Token: ' . self::$CSRF_TOKEN)
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->put();        
        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));
        
        Log::info('Erp Put Execution Time for $url : ' . date_diff($startTime, date_create())->format("%I:%S"));
        
        if ($response->status == 200){
            $content = json_decode($response->content);
            $headers = $response->headers;         
            return $content->data; 
        } else {
          //  throw new Exception("Response Code :" . $response->status . ", Message: " . json_decode($response));
           // log::info("Response Code :" . $response->status . ", Message: " . json_decode($response));
            return $response;
        }
    }
    
    public function getApiUrl($api, $params = null) {
        $api = env("ERP_HOST") . env($api) . $params;
        return $api;
    }
    
    protected function initCsrfToken() {       
        $response = Curl::to(self::getApiUrl("ERP_HOME_URL"))
            ->setCookieFile("erpcookie.txt")        
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->get();          
        
        Log::info('Extracting CSRF Token');
        $csrfstart = strpos($response->content, "frappe.csrf_token");
        $csrfassignment = strpos($response->content, "=\"", $csrfstart);
        Log::info('CSRF Assignment ' . $csrfassignment);
        $csrfend = strpos($response->content, "\";", $csrfstart);
        $token = substr($response->content, $csrfassignment+2, $csrfend - $csrfassignment - 2);
        Log::info('CSRF Token ' . $token);
        return $token;
    }


}
