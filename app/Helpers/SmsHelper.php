<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use App\Models\ApiLog;

class SmsHelper {
    
    public function sendSms($mobile_no,$message,$provider_id,$due_period){
      
        $token  = env('BEARERTOKEN');
        $url    = env('SMSAPIURL');
        
        $data   = [  "to_number"    => $mobile_no,
                     "sms_message"  => $message,
                     "origin"       => env('SMSORIGIN'),
                     "provider_id"  => $provider_id,
                     "due_period"   => $due_period
                ];
        $apilog    = ApiLog::addApiLogRequest("POST", $url);

        $headers = array(
            'Accept:application/json',
            'Authorization:Bearer '.$token
            );

        $response = Curl::to($url)
                    ->withData($data)
                    ->withHeaders($headers)
                    ->asJson()
                    ->post();

       // log::info(print_r($response,true));
        return $response;
        
    }
         
}
