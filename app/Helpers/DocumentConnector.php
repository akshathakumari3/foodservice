<?php

namespace App\Helpers;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Session;

class DocumentConnector {

    public function __construct() {

    }
   
    public function uploadDocument($file,$file_name,$path,$id) {

        $folder_path      = $this->createFolder($path,$id);
        if(isset($folder_path->content)){
            return $folder_path->content;
        } else {
            // log::info('uploadDocument folder_path :' .$folder_path);
            $doc_path         = $folder_path.'/'.$file_name;
            $data             = ['docPath' =>$doc_path , 'content' => $file];
            $api              = 'CREATE_DOCUMENT';
            $url              = $this->getApiUrl($api);
            $file_upload      = $this->doPostFile($url,$data);
            return $file_upload;
        }
    }

    public function storeDocument($file,$file_name,$path)
    {
        $city             = env("CITY_NAME");
        $folder_name      = $this->getPath($path);
        $doc_path         = $folder_name.'/'.$file_name;
        $data             = ['docPath' =>$doc_path , 'content' => $file];
        $api              = 'CREATE_DOCUMENT';
        $url              = $this->getApiUrl($api);
        $file_upload      = $this->doPostFile($url,$data);
        return $file_upload;
    }

    public function getDocument($id)
    {
        $api      = 'GET_DOCUMENT';
        $url      = $this->getApiUrl($api,["id" =>$id]);
        $get_file = $this->doGet($url);
        return $get_file;
    }

    public function createFolder($path,$id)
    {

        $city             = env("CITY_NAME");
        $folder_name      = $this->getPath($path);
        $folder_name      = $folder_name.'/'.$id;
        log::info('fodername :'.$folder_name);
        $api              = 'CREATE_FOLDER';
        $url              = $this->getApiUrl($api);
        $create_folder    = $this->doPost($url,$folder_name);
        log::info('folder creation function');
        if(isset($create_folder->folder)){
            log::info('folder created name is :'.$create_folder->folder->path);
            return $create_folder->folder->path;
        } elseif($create_folder->status == 500){

            if($create_folder->content){
                // log::info('folder creation error ');
                //log::info($response->status);
                $data = explode(' ', $create_folder->content);
                if($data[0] == "ItemExistsException:"){
                   return $data[1];
                }else{
                   return $create_folder;
                }
            }
        }
    }

    public function doPost($url, $data = null)
    {
        log::info('post url '.$url);
        $basic_auth  = $this->basicAuth();
        $response    = Curl::to($url)
                        ->withData($data)
                        ->returnResponseObject()
                        ->withResponseHeaders()
                        ->withOption('USERPWD',$basic_auth)
                        ->withHeaders( array( 'Accept: application/json') )
                        ->withContentType('application/json')
                        ->post(); 
        log::info('doPost function response');
        log::info($response->status);
        if ($response->status == 200){

            return json_decode($response->content);  
        }  else {
           return $response;
        }
    }

    public function doPostFile($url, $data = null)
    {
        log::info('doPostFile url '.$url);
        $basic_auth       = $this->basicAuth();
        $response         = Curl::to($url)
                                ->withData($data)
                                ->returnResponseObject()
                                ->withTimeout(1000)
                                ->withResponseHeaders()
                                ->withOption('USERPWD', $basic_auth)
                                ->withHeaders( array( 'Accept: application/json'))
                                ->withContentType('multipart/form-data')
                                ->containsFile()
                                ->post(); 
        log::info('doPostFile function response');
        log::info($response->status);
        if ($response->status == 200){

            return json_decode($response->content);  
        }else{
            return $response;
        }
    }

    public function doGet($url)
    {
        log::info('doGet url '.$url);
        $basic_auth  = $this->basicAuth();
        $response    = Curl::to($url)
                        ->returnResponseObject()
                        ->withResponseHeaders()
                        ->withOption('USERPWD', $basic_auth)
                        ->get();
        log::info('doGet function response');
        log::info($response->status);
        if ($response->status == 200){

            return $response->content;  
        } else {

            return $response->content;

        }
    }

    public function basicAuth()
    {
        
        $user_name = env('OPENKM_LOGIN_USERNAME');
        $password  = env('OPENKM_LOGIN_PASSWORD');
        return $user_name.":".$password;
    }

    public function getApiUrl($api,$params=null)
    {

        $apiurl = env($api); 
        if ($params != null) {
            foreach ($params as $key => $value) {
                $searchParam = "#" . $key ;
                $apiurl = str_replace($searchParam, $value, $apiurl);
            }
        }
        $api = env("OPENKM_HOST") . $apiurl;
        return $api;
    }

    public function getPath($api,$params=null)
    {

        $apiurl = env($api); 
        if ($params != null) {
            foreach ($params as $key => $value) {
                $searchParam = "#" . $key ;
                $apiurl = str_replace($searchParam, $value, $apiurl);
            }
        }
        $api = env("OPENKM_ROOT_PATH") . $apiurl;
        return $api;
    }
}