<?php

namespace App\Helpers;

use App\Helpers\DocumentConnector;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class DocumentHelper {
    
    protected $documentConnector;
    
    public function __construct() {
        $this->documentConnector = new DocumentConnector();         
    }

    public function uploadDocument($file,$file_name,$path,$id) {

        $file_upload = $this->documentConnector->uploadDocument($file,$file_name,$path,$id);
        return $file_upload;
    } 

    public function storeDocument($file,$file_name,$path) {

        $file_upload = $this->documentConnector->storeDocument($file,$file_name,$path);
        return $file_upload;
    }   

    public function getDocument($id) {

        $get_file = $this->documentConnector->getDocument($id);
        return $get_file;
    }  

    public function createFolder($path,$folder)
     {
        $create_folder = $this->documentConnector->createFolder($path,$folder);
        return $create_folder;
     } 
}
