<?php

namespace App\Helpers;

use App\Models\Customer;
use App\Models\Transaction;
use App\Helpers\ErpConnector;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\service;
use App\Models\Insurance\Invoice;
use App\Models\Insurance\Payment;
use App\Models\Client\Client;
use Flash;
use Redirect;
use Sentinel;
use App\Models\Insurance\Policy;
use App\Models\Insurance\PolicyGroupDetails;
class ErpHelper {
    
    protected $erpConnector;
    
    public function __construct() {
        $this->erpConnector = new ErpConnector();         
    }
    
    public function getServiceFees($code) {
        return $this->erpConnector->getServiceFee($code);
    }
    
    public function createOrUpdateCustomerInErp($customerId) {
        $customer = Customer::find($customerId); 
        $erpcustomer = $this->getCustomerModel($customer);
        if ($customer) {
            if ($customer->erp_customer_id) {
                 $result = $this->erpConnector->updateCustomer($customer->erp_customer_id, $erpcustomer);
            } else {
                 $result = $this->erpConnector->createCustomer($erpcustomer);
                 $customer->erp_customer_id = $result->name;
                 $customer->update();
            }
        }   
        return $customer; 
    }

	  public function createOrUpdateClientInErp($clientId) {
        $customer = Client::find($clientId); 
        $erpcustomer = $this->getClientModel($customer);
		log::info('erpcustomer ');
        if ($customer) {
            if ($customer->erp_client_id) {
				log::info('erp_client_id : '.$customer->erp_client_id);
                 $result = $this->erpConnector->updateCustomer($customer->erp_client_id, $erpcustomer);
            } else {
				log::info('create customer ');
                 $result = $this->erpConnector->createCustomer($erpcustomer);
                 $customer->erp_client_id = $result->name;
                 $customer->update();
            }
        }   
        return $customer; 
    }
	
	
    public function getCurrencyExchange()
    {
        $result = $this->erpConnector->getCurrencyExchange();
        return $result;
    }
    public function fileUpload($params) {
        $result = $this->erpConnector->fileUpload($params);
        return $result; 
    }
    
    public function createErpInvoiceAndPayment($transaction,$service_code,$payment_status,$currency,$gov_bill_number) {
       
        $serviceFees    = $this->erpConnector->getServiceFee($service_code);
        $customer       = $this->createOrUpdateCustomerInErp($transaction->buyer_id);
        
        Log::info("Creating Invoice for transaction " .  $transaction->id);
        $invoiceModel =  $this->erpConnector->createInvoice($this->getInvoiceModel($transaction, $customer, $serviceFees,$currency));
        
        $transaction->erp_invoice_id = $invoiceModel->name;

        //invoice table insert
            $invoice = $this->storeInvoiceDetails($transaction,$invoiceModel,$currency);

        //end

        if($currency == "SOS"){
            $paid_amount     = $invoiceModel->base_grand_total; 
            $received_amount =  $transaction->payment_amount;//$this->sosCurrencyConversion($transaction,$currency);
             
        }else{

            $paid_amount     = $transaction->payment_amount; 
            $received_amount = $transaction->payment_amount;
        }
        
        $transaction->update();
        Log::info("Update Erp Invoice Id for transaction " .  $transaction->id . " Invoice Number " . $transaction->erp_invoice_id);
         
        $paymentModel =  $this->erpConnector->createPayment($this->getPaymentModel($transaction, $customer,$currency,$paid_amount,$received_amount,$gov_bill_number,null));
       
        $transaction->erp_payment_id = $paymentModel->name;
        $transaction->status = $payment_status;
        $transaction->payment_status = $payment_status;
        $transaction->update();  

        //invoice table update 
        $updateInvoiceStatus =substr($payment_status,0,-5);
        $invoiceUpdate = $this->updateInvoiceDetails($invoice,$paymentModel->name,$updateInvoiceStatus,$received_amount,$currency);

        //end              
        Log::info("Update Erp Invoice Id for transaction " .  $transaction->id . " Payment Receipt No " . $transaction->erp_payment_id . " Status " . $transaction->status);
        
        return $transaction;
    }    
    
    public function storeInvoiceDetails($model,$data,$currency,$type=null){
	
		$invoices=$this->getInvoice($model);

		if(isset($invoices) && $invoices->status=="Unpaid"){			
			$invoice=Invoice::find($invoices->id);
			$invoice->commence_date=$invoice->commence_date;
		}else if(isset($invoices) && $invoices->status=="Paid Fully"){
			$invoice                 = new Invoice();
			$invoice->commence_date	=isset($model->commence_date) ?  $model->commence_date : $invoices->commence_date;
		}
		else{
			$invoice                 = new Invoice();
			$invoice->commence_date	=$model->start_date;
		}
		//echo "<pre>asdas";print_r($invoices);die();
        if($type =='claim'){
            $invoice->claim_id   = $model->id;
        }else{
            $invoice->policy_id  = $model->id;
            $invoice->variant_id = isset($model->product_variant_id)? $model->product_variant_id : ''; 
        }
		
		
		
        $invoice->customer_id     = $model->customer_id;
        $invoice->invoice_refno   = $data->name;
        $invoice->total_amount    = $data->net_total;
        $invoice->pending_amount  = $data->net_total;
        $invoice->invoice_date    = date("Y-m-d H:i:s");
        $invoice->due_date        = $data->due_date;
        $invoice->status          = 'Unpaid';
        $invoice->currency        = 'USD'; // 'SOS'
        $invoice->created_by      = isset(Sentinel::getUser()->id) ? Sentinel::getUser()->id :'';
        $invoice->save();
		

        return $invoice->id;
    }
	public function getInvoice($policy_detail){
		log::info("policy_detail".$policy_detail->id);
		//echo "policy_detail".$policy_detail->policy_group_id;die();
		$date=date('Y');
		//$invoice_detail=Invoice::where('policy_id',$policy_detail->id)->whereYear('invoice_date',$date)->orderBy('id', 'DESC')->first();
		
		if(isset($policy_detail->policy_group_id)!="" && isset($policy_detail->policy_group_id)!=null){
			$group_id="GP".$policy_detail->policy_group_id;
			$invoice_detail=Invoice::where('policy_id',$group_id)->whereYear('invoice_date',$date)->orderBy('id', 'DESC')->first();
		}else{
			$invoice_detail=Invoice::where('policy_id',$policy_detail->id)->whereYear('invoice_date',$date)->orderBy('id', 'DESC')->first();
		}
		
		//echo "<pre>";print_r($invoice_detail);die();
		//$result = $this->erpHelper->getSalesInvoice($invoice_detail->invoice_refno);
		return $invoice_detail;
		
	}

	
	 public function storeInvoiceGroupDetails($model,$data,$currency,$type=null,$policy_group_details){
		

        //$invoice                 = new Invoice();
		if(isset($model->id)){
			log::info("model id is".$model->id);
		$policy=Policy::find($model->id);
        $invoices=$this->getInvoice($policy);
		//echo "invoices";print_r($invoices);die();
		//echo "status is".$invoices->status;die();
		if(isset($invoices) && $invoices->status=="Unpaid"){		
					
			$invoice=Invoice::find($invoices->id);
			$invoice->commence_date=$invoice->commence_date;
		//	$invoice->due_date    = $invoice->due_date;
		}else if(isset($invoices) && $invoices->status=="Paid Fully"){
			
			$invoice                 = new Invoice();
			 $invoice->status          = 'Unpaid';
			 $invoice->commence_date=isset($model->commence_date) ? $model->commence_date :$invoices->commence_date;
			// $invoice->due_date        = $invoices->due_date;
		}
		else{
			$invoice                 = new Invoice();
			 $invoice->status          = 'Unpaid';
			 $invoice->commence_date=$invoices->commence_date;
			
		}
		}
		else{
			$polciy_detail=Policy::where('policy_group_id',$policy_group_details->id)->first();
			$invoice                 = new Invoice();
			$invoice->status          = 'Unpaid';
			$invoice->commence_date=$polciy_detail->start_date;
		}
        
        $invoice->client_id   	  = $model->client_id;
		$invoice->policy_id       = $policy_group_details->reference_no;
        $invoice->invoice_refno   = $data->name;
		$invoice->due_date        = $data->due_date;
        $invoice->total_amount    = $data->net_total;
        $invoice->pending_amount  = $data->net_total;
        $invoice->invoice_date    = date("Y-m-d H:i:s");
        
       
        $invoice->currency        = 'USD'; // 'SOS'
        $invoice->created_by      = isset(Sentinel::getUser()->id) ? Sentinel::getUser()->id :'';
        $invoice->save();

        return $invoice->id;
    }

    public function updateInvoiceDetails($id,$name,$status,$amount,$currency){
        $invoiceUpdate                  = Invocie::find($id);
        $invoiceUpdate->status          = $status;

        if($currency == "USD"){
            $invoiceUpdate->payment1_refno  = $name;
            $invoiceUpdate->payment1_amount = $amount;
        }elseif($currency == "SOS"){
            $invoiceUpdate->payment2_refno  = $name;
            $invoiceUpdate->payment2_amount = $amount;
        }
        $invoiceUpdate->save();
        return $invoiceUpdate;
    }
    protected function getCustomerModel($customer) {

        // if($customer->customer_type == "Personal"){
        //     $customer_name = $customer->full_name;
        // }else{
        //     $customer_name = $customer->business_name;
        // }

        $model = [ 
             "customer_name" => $customer->full_name,
             //"second_name" => $customer->second_name,
             //"third_name" => $customer->third_name,
             //"fourth_name" => $customer->fourth_name
         ];       
         return $model;                    
    }

  protected function getClientModel($client) {


        $model = [ 
             "customer_name" => $client->client_name,
             //"second_name" => $customer->second_name,
             //"third_name" => $customer->third_name,
             //"fourth_name" => $customer->fourth_name
         ];       
         return $model;                    
    } 	

    protected function getInvoiceModel($transaction,$customer, $serviceFees,$currency) {
         $model = [ 
             "customer"  => $customer->erp_customer_id, 
             "docstatus" => 1,
             "currency"  => $currency,
             "items"     => $this->getInvoiceLineItemsModel($transaction, $serviceFees,$currency)
         ];       
         return $model;                    
    }   
    
    protected function getInvoiceLineItemsModel($transaction, $serviceFees,$currency) {
        $invoiceItems = [];

        // log::info('$transaction->payment_amount'.$transaction->payment_amount);
        foreach ($serviceFees as $serviceFee) {
            $invoiceItem = [ 
                "item_code"  =>  $serviceFee->name 
            ]; 

            if (strpos($serviceFee->item_code, 'LEVY') !== false) {
                if($currency == "SOS"){
                    $rate1                    = $transaction->payment_amount;//$this->sosCurrencyConversion($transaction,$currency);
                    $rate2                    = $transaction->settlement_amount * 0.025;
                    $invoiceItem["rate"]      = $rate1+$rate2;
                }else{
                    $invoiceItem["rate"]  = $transaction->settlement_amount * 0.025;
                }
                
            } elseif($currency == "SOS"){
                $rate1                    = $transaction->payment_amount;//$this->sosCurrencyConversion($transaction,$currency);
                $invoiceItem["rate"]      = $rate1;
            }           
               
            array_push($invoiceItems, $invoiceItem);
        }        
        return $invoiceItems;
    }

    public function sosCurrencyConversion($transaction,$currency)
    {
        $payment_amount    = $transaction->payment_amount;
        $currency_exchange = $this->getCurrencyExchange();
        if(isset($currency_exchange->name) && ($currency == "SOS")){

            $payment_amount              = $transaction->payment_amount*$currency_exchange->exchange_rate;
           return (int)$payment_amount; 
        } 
        return $payment_amount;
    }

    protected function getPaymentModel($invoice, $customer,$paid_amount,$received_amount,$account,$request,$type=null) {
		
if($type=="client"){
	$model = [ 
                "payment_type" => "Receive",
                "party_type"=> "Customer", 
                "party" => $customer->erp_client_id,
                "paid_amount" => (float)$received_amount,
                "received_amount"=> (float)$received_amount,
                "paid_to"=> $account, 
                "references" => [[
                        "reference_doctype" => "Sales Invoice",
                        "reference_name"=> $invoice->invoice_refno,
                        "allocated_amount"=> (float)$paid_amount
                ]], 
                "reference_no"=> $request->input('reference_no'),
                "reference_date"=> date("Y M d"), 
                "payment_customer" => $customer->client_name, 
                "payment_customer_mobile" => isset($customer->primary_contact_no)?$customer->primary_contact_no:'',
                "docstatus" => 1,
                //"gov_bill_number" => $gov_bill_number
              ];
}else{
        $model = [ 
                "payment_type" => "Receive",
                "party_type"=> "Customer", 
                "party" => $customer->erp_customer_id,
                "paid_amount" => (float)$received_amount,
                "received_amount"=> (float)$received_amount,
                "paid_to"=> $account, 
                "references" => [[
                        "reference_doctype" => "Sales Invoice",
                        "reference_name"=> $invoice->invoice_refno,
                        "allocated_amount"=> (float)$paid_amount
                ]], 
                "reference_no"=> $request->input('reference_no'),
                "reference_date"=> date("Y M d"), 
                "payment_customer" => $customer->full_name, 
                "payment_customer_mobile" => $customer->mobile_1,
                "docstatus" => 1,
                //"gov_bill_number" => $gov_bill_number
              ];
}			  
         return $model;                    
    }  

    public function paymentModel($payment_data) {
        $model = [ 
                "payment_type"    => "Receive",
                "party_type"      => "Customer", 
                "party"           => $payment_data['erp_customer_id'],
                "paid_amount"     => (float)$payment_data['paid_amount'],
                "received_amount" => (float)$payment_data['paid_amount'],
                //"paid_to"         => $payment_data['payment_method'], 
                'paid_to'         => 'Cash - G',
                "references"      => [[
                    
                        "reference_doctype" => "Sales Invoice",
                        "reference_name"    => $payment_data['invoice_name'],
                        "allocated_amount"  => (float)$payment_data['paid_amount']
                    ]], 
                "reference_no"            => $payment_data['payment_reference_number'],
                "reference_date"          => Carbon::parse($payment_data['payment_date'])->format('Y-m-d'), 
                "payment_customer"        => $payment_data['payment_customer'], 
                "payment_customer_mobile" => $payment_data['payment_customer_mobile'],
                "docstatus"               => 1,
                "gov_bill_number" => ''
              ];      
         return $model;                    
    }  
    
    public function searchCustomerPaymentHistory($data) {
        $result = $this->erpConnector->searchCustomerPaymentHistory($data);
        return $result; 
    }

    public function getDocument($data)
    { 
        $result = $this->erpConnector->getDocument($data);
        return $result; 
    }

    public function modeOfPayments($filters) {
        $result = $this->erpConnector->modeOfPayments($filters);
        return $result; 
    }

    public function filterItemGroups($filters) {
        $result = $this->erpConnector->filterItemGroups($filters);
        return $result; 
    }
    public function filterItems($filters) {
        $result = $this->erpConnector->filterItems($filters);
        return $result; 
    }

    public function filterItemsPriceList($filters) {
        $result = $this->erpConnector->filterItemsPriceList($filters);
        return $result; 
    }
    
    public function getDefaultAccount($name) {
        $result = $this->erpConnector->getDefaultAccount($name);
        return $result; 
    }

    public function getSerielRefNumbers($filters) {
        return $this->erpConnector->getSerielRefNumbers($filters);
    }

    public function updateSerialRefNumber($name,$data) {
        return $this->erpConnector->updateSerialRefNumber($name,$data);
    }

    public function getSalesInvoice($name)
    {
        $result= $this->erpConnector->getSalesInvoice($name);
		return $result;
    }
    
    public function createInvoice($data) {
        return $this->erpConnector->createInvoice($data);
    }

    public function submitErpInvoiceAndPayment($invoice_details,$account,$request) {

	//echo "invoice_details<pre>";print_r($invoice_details);echo "account";print_r($account);die();
        $data             = ['docstatus' => 1];
        $submitErpInvoice = $this->erpConnector->submitErpInvoice($invoice_details->invoice_refno,$data);
	
        if(isset($submitErpInvoice->name)){
			if($invoice_details->customer_id!=''){
            $customer         = $this->createOrUpdateCustomerInErp($invoice_details->customer_id);
			$type='customer';
			}
			else{			
			 $customer         =$this->createOrUpdateClientInErp($invoice_details->client_id);
			 	$type='client';
			}
            Log::info("Submit Invoice for transaction " .  $invoice_details->invoice_refno);
            $paid_amount     = str_replace(',', '', $request->input('paid_amount'));
            $received_amount =  $invoice_details->total_amount;

            $paymentModel =  $this->erpConnector->createPayment($this->getPaymentModel($invoice_details, $customer,$paid_amount,$received_amount,$account,$request,$type));
			//echo "<pre>";print_r($paymentModel);die();
            if(isset($paymentModel->name)){
				Log::info("payment name".$paymentModel->name);
                $type   = $request->input('amount_payment_type');
                $status =  ($type == 'Full') ? 'Paid Fully' : 'Paid Partially' ;
				if($type == 'Full'){
					$policy_detail=Policy::find($invoice_details->policy_id);
					if(isset($policy_detail)){
						$old_premium_amount=isset($policy_detail->old_premium_amount) ? $policy_detail->old_premium_amount : 0;
						$policy_detail->old_premium_amount=$old_premium_amount+$paid_amount;
						//$policy_detail->dependent_count=0;
						$policy_detail->total_premium_due=0;
						$policy_detail->save();
						
					}else{
						$policy_group_detail=PolicyGroupDetails::where('reference_no',$invoice_details->policy_id)->first();
						//echo "<pre>";print_r($policy_group_detail);die();
						if(isset($policy_group_detail)){
							$old_premium_amount=isset($policy_group_detail->old_total_premium) ? $policy_group_detail->old_total_premium : 0;
							$policy_group_detail->old_total_premium=$old_premium_amount+$paid_amount;
							$policy_group_detail->save();
						}
					}
				}
                $update_invoice_data = [   
                   "status"         => $status,
                   "payment_refno"  => $paymentModel->name,
                   "paid_amount"    => $paid_amount,
                   "installment"    => $type,
                   "pending_amount" => $paymentModel->references[0]->outstanding_amount,//$invoice_details->total_amount - $paymentModel->unallocated_amount,//$invoice_details->total_amount - $paid_amount,
                   "mode_of_payment" => $request->input('mode_of_payment'),
                   "payment_account" => $account,
                   "updated_by" => Sentinel::getUser()->id
                ];
           
                $invoiceUpdate = $this->invoiceStatusUpdate($invoice_details->invoice_refno,$update_invoice_data);
				if($request->input('reference_no')!=""){
					$reference_no=$request->input('reference_no');
				}else{
					$reference_no=$invoice_details->id;
				}
                $payment_data       = [   
                   "invoice_id"         => $invoice_details->id,
                   "payment_reference_id"  => $paymentModel->name,
				   "payment_mode"		   =>$request->mode_of_payment,
                   "reference_no"    => $reference_no,
                   "amount"          => $paid_amount,
                   "paid_by"         => "",
                   "date"            => date('Y-m-d'),
                   "status"          => $status,
                   "created_by"      => Sentinel::getUser()->id
                ];

                $invoiceUpdate = $this->storePaymentStatus($payment_data);
                return $paymentModel->name;
            } else{
                log::info("Payment  API Issue");
                Flash::error("Payment Issue");
                return Redirect::back();
            }

        }else{
          log::info("Invoice Update Issue");
          Flash::error("Invoice Update Issue");
          return Redirect::back();  
        }
    } 

    public function invoiceStatusUpdate($invoice_name,$update_invoice_data)
    {
      
        $update_invoice = Invoice::where("invoice_refno", $invoice_name)->update($update_invoice_data);
        return $update_invoice;
    }

    public function storePaymentStatus($payment_data)
    {
      
        $create_payment = Payment::create($payment_data);
        return $create_payment;
    }

    public function itemPriceList($items,$currency){
    
       return $this->erpConnector->itemPriceList($items,$currency);
    }

    public function taxAndCharges($name){
   
       return $this->erpConnector->taxAndCharges($name);
    }

    public function billNumberDuplicateCheck($filter){
   
       return $this->erpConnector->billNumberDuplicateCheck($filter);
    }

    public function getPayment($name){
   
       return $this->erpConnector->getPayment($name);
    }

    public function updateInvoice($name,$data)
    {
      return $this->erpConnector->updateInvoice($name,$data);
    }

    public function getCustomer($name)
    {
      return $this->erpConnector->getCustomer($name);
    }

    public function submitErpInvoice($name)
    {
        $data             = ['docstatus' => 1];
        $submitErpInvoice = $this->erpConnector->submitErpInvoice($name,$data);
        return $submitErpInvoice;
    }
	 public function cancelInvoice($id, $invoice) {
       
        $result = $this->erpConnector->cancelInvoice($id, $invoice);
        return $result; 
    }
	
}
