<?php

namespace App\Helpers;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;
use Cache;
use Carbon\Carbon;
use Sentinel;
use Config;
use Session;
use App\Models\Subscriber;
use App\Models\Notifications;

class PushNotification {

    public function  push($msg, $notificationMessage, $registrationIds) {
		// API access key from Google API's Console
		$API_ACCESS_KEY = env('NOTIFICATION_ACCESS_KEY');
		$serverurl = "https://fcm.googleapis.com/fcm/send";
		
		$fields = array (
			'registration_ids' 	=> array($registrationIds),
			'data'			    => $msg, 
			'notification'		=> $notificationMessage,
		);
		
		$headers = array (
			'Authorization: key=' . $API_ACCESS_KEY,
			'Content-Type: application/json'
		);
		 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, $serverurl );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result   = curl_exec($ch );
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close( $ch );
		return $result;		
	}
	
	public function sendMessage($message,$id,$msg,$text,$username,$reference_type){
		$notificationMessage =  [ "title" => $text, "body" => $message ];
		$subscribeId=Subscriber::where('customer_id',$id)->where('username',$username)->get();
		//echo "<pre>";print_r($subscribeId);die();
		foreach($subscribeId as $value){
		$notifcationMess=$this->push($msg,$notificationMessage,$value->device_key);
		$notification=new Notifications();
		$notification->subscriber_id=$value->id;
		$notification->reference_id=$id;
		$notification->reference_type=$reference_type;
		$notification->message=$message;
		$notification->status=1;
		$notification->created_by=Sentinel::getUser()->id;
		$notification->save();
		}

	}
}

