<?php

namespace App\Repositories\Customeridentity;

use App\Models\Customeridentity\CustomerIdentity;
use InfyOm\Generator\Common\BaseRepository;

class CustomerIdentityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerIdentity::class;
    }
}
