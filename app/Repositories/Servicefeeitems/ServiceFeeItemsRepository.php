<?php

namespace App\Repositories\Servicefeeitems;

use App\Models\Servicefeeitems\ServiceFeeItems;
use InfyOm\Generator\Common\BaseRepository;

class ServiceFeeItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceFeeItems::class;
    }
}
