<?php

namespace App\Repositories;

use App\Models\RightTable;
use InfyOm\Generator\Common\BaseRepository;

class RightTableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RightTable::class;
    }
}
