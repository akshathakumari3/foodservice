<?php

namespace App\Repositories;

use App\Models\DocumentShelf;
use InfyOm\Generator\Common\BaseRepository;

class DocumentShelfRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DocumentShelf::class;
    }
}
