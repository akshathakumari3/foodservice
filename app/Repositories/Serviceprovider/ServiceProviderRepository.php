<?php

namespace App\Repositories\Serviceprovider;

use App\Models\ServiceProvider\ServiceProvider;
use App\Models\ServiceProvider\ServiceFacility;
use InfyOm\Generator\Common\BaseRepository;

class ServiceProviderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceProvider::class;
    }
}
