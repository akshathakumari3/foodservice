<?php

namespace App\Repositories;

use App\Models\ServiceFeeItems;
use InfyOm\Generator\Common\BaseRepository;

class ServiceFeeItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceFeeItems::class;
    }
}
