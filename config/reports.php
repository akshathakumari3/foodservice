<?php

return [

    'insurance' => [
        'ClaimReport' => 'Claim Details Report',
        'ClientMembersReport'  => 'Client Members Report',
        'CustomerFinancialStatementReport' => 'Customer Financial Statement',
        'CustomerPolicyStatementReport' => 'Customer Policy Statement',
        'ServiceProviderClaimsReport' => 'Service Provider Claims Report',
        'ServiceProviderDetailsReport' => 'Service Provider Details Report',
        'CustomerRegistrationReport'=> 'Customer Registration Report',
        'PolicyDetailsReport' => 'Policy Details Report',
        'ServiceDetailsReport' => 'Services Details Report',
        'ClientDetailsReport' => 'Clients Details Report',
        'ProductDetailsReport'  => 'Product Details Report',
        'ProductVariantsDetailsReport' => 'Product Variant Details Report',
        'PricingDetailsReport'=> 'Pricing Details Report',
    ],

    'client' => [
        'ClientMembersReport'  => 'Client Members Report',
        'CustomerPolicyStatementReport' => 'Customer Policy Statement',
        'PolicyDetailsReport' => 'Policy Details Report',
        'CustomerFinancialStatementReport' => 'Customer Financial Statement Report',
    ],

    'insurance-officer' => [
        'ClaimReport' => 'Claim Details Report',
        'ClientMembersReport'  => 'Client Members Report',
        'CustomerFinancialStatementReport' => 'Customer Financial Statement',
        'CustomerPolicyStatementReport' => 'Customer Policy Statement',
        'ServiceProviderClaimsReport' => 'Service Provider Claims Report',
        'ServiceProviderDetailsReport' => 'Service Provider Details Report',
        'CustomerRegistrationReport'=> 'Customer Registration Report',
        'PolicyDetailsReport' => 'Policy Details Report',
        'ServiceDetailsReport' => 'Services Details Report',
        'ClientDetailsReport' => 'Clients Details Report',
        'ProductDetailsReport'  => 'Product Details Report',
        'ProductVariantsDetailsReport' => 'Product Variant Details Report',
        'PricingDetailsReport'=> 'Pricing Details Report',
        'InvoiceDetailsReport'=> 'Invoice Details Report',
        'PaymentDetailsReport'=> 'Payment Details Report',
        'ClientGroupPolicyReport'=> 'Client Group Policy Report',
        'DependentMembersReport'=> 'Primary And Dependent Members Report',
        'ProductOptionDetailsReport'=> 'Product Option Details Report',
    ],

    'service_provider_reports'=> [
        
        'ServiceProviderClaimsReport' => 'Service Provider Claims Report',
        'CustomerPolicyStatementReport' => 'Customer Policy Statement',
        'PolicyDetailsReport' => 'Policy Details Report',
    ],
];