<?php 

return [
"date-format"               => "d-M-Y",
"time-format"               => "h:i A",
"date-time-format"          => "d-M-Y h:i:s",
];