<?php

return [

    'relationships' => [
 		
        'Father'   => 'Father',
        'Mother'   => 'Mother',
        'Brother'  => 'Brother',
        'Sister'   => 'Sister'
    ],

    'phone_carriers' => [

        "Telesom"    => "Telesom",
        "Somtelwifi" => "Somtelwifi",
        "Golis"      => "Golis"
    ],

    'states' => [

        "Puntland"    => "Puntland",
        "Somaliland" => "Somaliland"
    ],

    'countries' => [

        "Somalia"    => "Somalia",
        "Somaliland" => "Somaliland"
        
    ],
    'client_type' => [

        "NGO"      => "NGO",
        "NON-NGO"  => "NON-NGO",
    ],

    'customer_types' => [

        "PrimaryPolicyHolder"    => "Primary Policy Holder",
    ],

    'status' => [

        "1"    => "Active",
        "0"    => "Deactive",
    ],

    'service_type' => [

        "General"  => "General",
        "ICD10"    => "ICD10",
    ],

    'bed_type' => [

        "Ward Bed"              => "Ward Bed",
        "Standard Private Room" => "Standard Private Room",
        "Private Room"          => "Private Room",
        "En suite"              => "En suite",
        "Pavillion"             => "Pavillion",
    ],
    'doctor_types' => [
        "General Practitioner"  => "General Practitioner",
        "Specialist"            => "Specialist"
    ],
    'months' => [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ],

        "payment_type" => [
            "Paid Fully"      => "Paid Fully",
            "Paid Partially"  => "Paid Partially",
        ],

        "claim_status" => [
            "Settled" => "Settled",
            "Open"    => "Open",
            "Approved" => "Approved",
            "Rejected" => "Rejected",
        ],

        "claim_type" => [
            "In Patient" => "In Patient",
            "Out Patient" => "Out Patient",
        ],
		"relationship" => [
            "spouse" => 4,
        ],
		"age_valid"=>[
		   "spouse" => 18,
		   "child"=>18
		]
    
    
];
