<?php

return [
    "client_member" => [
        "Employee ID",
        "Date of Joining(dd-mm-yyyy)",
        "First Name",
        "Second Name",
        "Third Name",
        "Gender",
        "E-Mail",
        "Height",
        "Weight",
        "Mobile provider",
        "Mobile Number",
        "Address",
        "District",
        "Sub District",
        "State",
        "Country"
    ],
    "dependent_member" => [
        "Employee ID",
        "Relationship",
        "First Name",
        "Second Name",
        "Third Name",
        "Date of Birth(dd-mm-yyyy)",
        "Gender"
    ],
];
