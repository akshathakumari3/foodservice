"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");
    var base_url = $('#base_url').val();
    var form     = $("#pricing_form"); 

    // manage wizard 
    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {

            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            
            // dont change the order of the checks
            // Customer  Information
            if (newIndex === 1) {

                if(!$('#line_of_business').val()){
                    $("#wizard-error").html("Please Select Line of Business.").show();
                    return false;
                }

                if(!$('#product_name_id').val()){
                    $("#wizard-error").html("Please Select Product.").show();
                    return false;
                }

                if(!$('#product_option').val()){
                    $("#wizard-error").html("Please Select Product Option.").show();
                    return false;
                }

                if(!$('#product_variant').val()){
                    $("#wizard-error").html("Please Select Product Variant.").show();
                    return false;
                }
                if($('#sum_insured').val()){
                    var pricingExists = false;
                    var business         = $("#line_of_business").val();
                    var product_name_id  = $("#product_name_id").val();
                    var product_variant  = $("#product_variant").val();
                    var product_option  = $("#product_option").val();
                    if($("#pricing_id").val() == null || $("#pricing_id").val() == ''){
                        $.ajax({
                            async: false,
                            type: "post",
                            url : $("#baseurl").val()+"/app/pricing/pricingExists",
                            data : {"line_of_business":business,
                                    "product_name":product_name_id,
                                    "product_variant":product_variant,
                                    "product_option":product_option
                            },
                            success : function (data) {
                                if(data.status == 'fail') {
                                    pricingExists = true;
                                } else {
                                    $("#wizard-error").html("").hide();
                                }
                            }
                        });
                    }
                    if(pricingExists) {
                        $("#wizard-error").html("Pricing Details Already Exists").show();
                        return false;
                    }
                }

                var line_of_business = $("#line_of_business option:selected").text().toUpperCase();

                if(line_of_business !="HEALTH"){
                    $("#wizard-error").html("Line of Business Allow Health Only.").show();
                    return false;
                }

                $("#wizard-error").html("").hide();
                return true;
            }


            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },

        onFinished: function (event, currentIndex) {
            $("#wizard-error").html("").hide();
            var product_name = $("#product_option :selected").text();
			var product_name_id=$("#product_name_id").val();
            if(product_name_id==3 && product_name ==="Inpatient- Single & Family"){
                if( $("#sme_pricing_table_count").val() != $("#sme_table_count").val()){
                    $("#wizard-error").html("Enter All Health Pricing Details and Add Details.").show();
                    return false;
                }
                tabl_name    = $('table#sme_pricing_table thead tr th');
                tabl_content = $('table#sme_pricing_table tbody tr');
            }
			else if(product_name_id==3 && product_name ==="Outpatient- Single & Family"){
                if( $("#sme_pricing_outpatient_table_count").val() != $("#sme_table_outpaticount").val()){
                    $("#wizard-error").html("Enter All Health Pricing Details and Add Details.").show();
                    return false;
                }
                tabl_name    = $('table#sme_pricing_outpatient_table thead tr th');
                tabl_content = $('table#sme_pricing_outpatient_table tbody tr');
            }
			else if(product_name_id==7 && (product_name ==="Inpatient" || product_name ==="Outpatient" || product_name ==="Dental- Add-on" || product_name ==="Optical- Add-on")){
                if( $("#medical_pricing_table_count").val() != $("#medical_table_count").val()){
                    $("#wizard-error").html("Enter All Health Pricing Details and Add Details.").show();
                    return false;
                }
                tabl_name    = $('table#medical_pricing_table thead tr th');
                tabl_content = $('table#medical_pricing_table tbody tr');
            }else{
                if( $("#pricing_table_count").val() != $("#table_count").val()){
                    $("#wizard-error").html("Enter All Health Pricing Details and Add Details.").show();
                return false;
                }

                var tabl_name    = $('table#pricing_table thead tr th');
                var tabl_content = $('table#pricing_table tbody tr');
            }

            var pricing_datas    = [];
            var headers          = [];
            var business         = $("#line_of_business").val();
            var product_name_id  = $("#product_name_id").val();
            var pricing_id       = $("#pricing_id").val();
            var product_variant  = $("#product_variant").val();
			var product_option  = $("#product_option").val();
            

            tabl_name.each(function(index, item) {
                headers[index] = $(item).html();             
            });
            tabl_content.each(function(row, tr) {
                var arrayItem = {};
                $(this).find("td").each(function (index, item) {
					console.log('asd');
					console.log(index);
                    arrayItem[headers[index]] = $(item).html();
                });
                pricing_datas.push(arrayItem);
            });
            if(pricing_datas.length == 0){
                $("#wizard-error").html("Enter Health pricing Details.").show();
                return false;
            }

            var bed_type = $("#bed_type").val();

            $.ajax({
                type: "post",
                url : $("#baseurl").val()+"/app/pricing",
                data : {"data": pricing_datas,"line_of_business":business,"product_name":product_name_id,
                        "product_variant":product_variant,"pricing_id":pricing_id,"bed_type":bed_type,product_option:product_option},
                success : function (data) {
                    $("#gis-loader").css("display", "block");
                    $("#wizard-error").html("").hide();
                    window.location = base_url+"/app/pricing/"+data;
                }
            });
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            console.log('error last'+error);
            element.before(error);
        }
    });  

});

