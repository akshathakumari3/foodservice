"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");

    $('#specialists-form').click(function(){
    
    if(!$('#service_provider_id').val()){
        $("#wizard-error").html("Please Select Service Provider.").show();
        goTop();
        return false;
    }

    if(!$('#sp_name').val()){
        $("#wizard-error").html("Please Enter Specialist Name.").show();
        goTop();
        return false;
    }

    if(!$('#education').val()){
        $("#wizard-error").html("Please Select Education.").show();
        goTop();
        return false;
    }

    if(!$('#designation').val()){
        $("#wizard-error").html("Please Enter Designation.").show();
        goTop();
        return false;
    }

    if(!$('#license_number').val()){
        $("#wizard-error").html("Please Enter License Number.").show();
        goTop();
        return false;
    }

    if(!$('#specialisation').val()){
        $("#wizard-error").html("Please Select Specialisation.").show();
        goTop();
        return false;
    }

    if(!$('#contact_number').val()){
        $("#wizard-error").html("Please Enter Contact Number.").show();
        goTop();
        return false;
    }

        return true;
    });

    function goTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
});

