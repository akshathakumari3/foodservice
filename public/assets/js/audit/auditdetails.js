function auditDetails(auditable_id,auditable_type){
    var url = $('#baseurl').val()+"/app/audit/showAuditDetails";
    $.ajax({
        type: 'post', 
        url : url, 
        data : {"auditable_id":auditable_id,"auditable_type":auditable_type},
        success : function (data) {
            $("#audit-details").html(data);
        }
    }); 

}