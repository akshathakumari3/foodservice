"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");

    $('#clientsave').click(function(){
        if(!$('#client_name').val()){
        $("#wizard-error").html("Please Enter Client Name.").show();
        goTop();
        return false;
    }

    if(!$('#no_of_employee').val()){
        $("#wizard-error").html("Please Enter No. Of Employee.").show();
        goTop();
        return false;
    }

    if(!$('#primary_conatct_person').val()){
        $("#wizard-error").html("Please Enter Primary Contact Person.").show();
        goTop();
        return false;
    }

    if(!$('#primary_person_designation').val()){
        $("#wizard-error").html("Please Enter Designation.").show();
        goTop();
        return false;
    }

    if(!$('#primary_contact_no').val()){
        $("#wizard-error").html("Please Enter Primary Contact Number.").show();
        goTop();
        return false;
    }

    if(IsEmail($('#email').val())==false){
        $("#wizard-error").html("Please Enter Valid Format Email ID.").show();
        goTop();
        return false;
    }

    if(!$('#address').val()){
        $("#wizard-error").html("Please Enter the Address.").show();
        goTop();
        return false;
    }

    if(!$('#district').val()){
        $("#wizard-error").html("Please Select District.").show();
        goTop();
        return false;
    }

    if(!$('#sub_district1').val()){
        $("#wizard-error").html("Please Select Sub District.").show();
        goTop();
        return false;
    }

    if(!$('#state').val()){
        $("#wizard-error").html("Please Select State.").show();
        goTop();
        return false;
    }

    if(!$('#country').val()){
        $("#wizard-error").html("Please Select Country.").show();
        goTop();
        return false;
    }

        return true;
    });

    function goTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
           return false;
        }else{
           return true;
        }
      }
});

$(document).ready(function () {
    $(".wrapper").addClass("hide_menu");

    $('.bulkUpload').click(function(){
        if(!$('#client_id').val()){
            $("#wizard-error").html("Please Select the Client ID.").show();
            goTop();
            return false;
        }

        if(!$('#client_file').val()){
            $("#wizard-error").html("Please Upload the Client file.").show();
            goTop();
            return false;
        }
        return true;
    });

    function goTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
});


    function formatValue (value) {
        if (!value.id) { return value.text; }
        var $value = $(
          '<span><img src="'+assets_url+'/insurance/images/product/' + value.text.toLowerCase() + '.png" class="img-select-icon" width="20px"/> ' + value.text + '</span>'
        );
        return $value;
      };

    var base_url = $("#baseurl").val();
    $.fn.select2.defaults.set("theme","bootstrap");

    $('select[name="line_of_business"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/policy/get-line-of-business',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
                }
                return query;
            }
        },
        templateResult: formatValue,
        templateSelection: formatValue,
    }).on('select2:select', function (e) {  
        var referenceNameOption = $("<option selected='selected'></option>").val("").text("");
        $("#product_name").append(referenceNameOption);
    });

    $('select[name="product_name"]').select2({
            placeholder: 'Please Select',
            allowClear: true,
            ajax: {
                url         : base_url+'/app/clientMember/productDetails',
                dataType    : 'json',
                method      : 'get',
                quietMillis : 1000,
                data:function (params) {
                    var query = {
                        search     : params.term,
                        policy_type: $("#sme_policy").val(),
                        type       : 'public',
                        business   : $("#line_of_business").val()
                    }
                    return query;
                }
            }
        }).on('select2:select', function (e) {  
    });

    $('select[name="add_on_name"]').select2({
            placeholder: 'Please Select',
            allowClear: true,
            ajax: {
                url         : base_url+'/app/get-addon-details',
                dataType    : 'json',
                method      : 'get',
                quietMillis : 1000,
                data:function (params) {
                    var query = {
                        search     : params.term,
                        type       : 'public'
                    }
                    return query;
                }
            }
        }).on('select2:select', function (e) {  
    });
	
	
	$('select[name="product_option"]').select2({
      placeholder: 'Select a Value',
      allowClear: true,
      ajax: {
         url: base_url+'/app/get-product-option-details',
         dataType: 'json',
         method:'get',
         data:function (params) {
            var query = {
               search  : params.term,
               type    : 'public',
               product_id: $("#product_name").val()
            }
           return query;
         }
      }
   }).on('select2:select', function (e) {  
      //checkProductVarient();
    })
	
	$('select[name="product_name"]').change(function(){
		  var $option = $("<option selected></option>").val('').text('');
			$('#product_option').append($option).trigger('change');
	 });
