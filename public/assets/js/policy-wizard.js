"use strict";

$(document).ready(function () {
	var gender=0;
var spouse_count=0;
var flag=0;
    var modalCustomerAgainstNominee='';
    $(".wrapper").addClass("hide_menu");
    var base_url             = $('#base_url').val();
    var form                 = $("#policyform"); 
	
    var selectedCustomer     = '';
    var selectedInsurer      = [];
    var selectedNominee      = '';
    var addedInsurerDetails  = []; 
    var addedNomineeDetails  = []; 

    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            
            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }

            // dont change the order of the checks
            // Customer
            if (newIndex === 1 && !$("#selected_customer_id").val()) {
                $("#wizard-error").html("Please Select Customer.").show();
                return false;
            }

            if (newIndex === 1 ) {

               getCustomerAgainstMembers();
               getCustomerAgainstNominee();
			 
			 
            }

            // Product Variant
           /* if (newIndex === 2 && !$("#product_details").val()) {
            
                $("#wizard-error").html("Please Select Product All Details").show();
                return false;
            }*/

            if (newIndex === 2 && !$("#line_of_business").val()) {
            
                $("#wizard-error").html("Please Select Line of Business").show();
                return false;
            }
			
			 if (newIndex === 2 && !$("#product_name").val()) {
            
                $("#wizard-error").html("Please Select product Name").show();
                return false;
            }
			var selected_product_table = $('#selected-product-table tbody tr').length;
            if (newIndex === 2 && selected_product_table==0) {           
                $("#wizard-error").html("Please add product option details.").show();
                return false;
            }

           /* if (newIndex === 2 && !$("#sum_insured").val()) {
            
                $("#wizard-error").html("Please Select Sum Insured.").show();
                return false;
            }*/

            if (newIndex === 3 && selectedInsurer.length <= 0) {
                $("#wizard-error").html("Please Select Member.").show();
                return false;
            }

            if (newIndex === 3 && !$("#selected_nominee_id").val()) {
            
                $("#wizard-error").html("Please Select Nominee.").show();
                return false;
            }
            var line_of_business = $("#line_of_business_text").val();
            
            var member_table_count = $('#selected-insurers-table tbody tr').length;
            var product_name = $("#product_name_select").val();
            $("#premium_amount").attr("readonly", true);
			
            if(newIndex === 3 && line_of_business == "HEALTH"  && product_name==2 && member_table_count > 1){
                $("#wizard-error").html("Please Select Only One Member.").show();
                return false;
            }
            if(newIndex === 3 && line_of_business == "HEALTH"  && product_name==1 && member_table_count < 2){
                $("#wizard-error").html("Please Select Mimimum Two Member.").show();
                return false;
            }

            if(line_of_business =="HEALTH"){
                if(newIndex === 3){
                    var variant_id =  $("#policy_product_detail").val();
                    getHealthPricingDetails(variant_id);
					

                }
            }else{
                $('#pricing_div').html(""); 
                $("#premium_amount").attr("readonly", false); 
            }

            if(newIndex === 2){
                $("#wizard-error").html("").hide();
            }

            if (newIndex === 4 && !$("#tenure").val()) {
           
                $("#wizard-error").html("Please Select Tenure").show();
                return false;
            }

            if (newIndex === 4 && !$("#start_date").val()) {
            
                $("#wizard-error").html("Please Select Start Date").show();
                return false;
            }

            if (newIndex === 4 && !$("#end_date").val()) {
            
                $("#wizard-error").html("Please Select End Date.").show();
                return false;
            }

            if (newIndex === 4 && !$("#premium_amount").val()) {
            
                $("#wizard-error").html("Premium Amount Required.").show();
                return false;
            }
			 if (newIndex === 4 && !$("#installment_type").val()) {
            
                $("#wizard-error").html("Installment Type is Required.").show();
                return false;
            }

            if(newIndex === 4){

                $("#wizard-error").html("").hide();
                var wizardSection = "wizard-summary-view";
                var nextView      = "admin.policies.summary-details";
                processNextStep(form, nextView, wizardSection);
				
            }
			
            if(newIndex === 2){
                var insurer_id  = $("#selected_customer_id").val();
                var inArray     = jQuery.inArray(eval(insurer_id), selectedInsurer);
                if (inArray < 0) {
                    getPolicyMember(insurer_id);
                    insurerDataTable.rows().remove().draw();
                }
            }
            if(newIndex === 2){
				caluclation_permium();
			}
			if(newIndex === 3){
				var variant_id =  $("#policy_product_detail").val();
				getHealthPricingDetails(variant_id);
				caluclation_permium();
			}
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
				var variant_id =  $("#policy_product_detail").val();
				getHealthPricingDetails(variant_id);
				caluclation_permium();
				
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        // onFinishing: function (event, currentIndex) {

        //     // form.validate().settings.ignore = ":disabled";
        //     // $("#wizard-error").html("Form Not valid").show();
        //     return form.valid();

        // },
        onFinished: function (event, currentIndex) {
            var formData = form.serialize();
            var url      = base_url+"/app/policy/finish-wizard";
            $.post(url,  formData, 
            function (data) {
                //console.log(base_url+"/app/policy/"+data);
                $("#gis-loader").css("display", "block");
                $("#wizard-error").html("").hide();
                window.location = base_url+"/app/policy/"+data;
            }).fail(function(){
                console.log("error");
            });  
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            console.log('error last'+error);
            element.before(error);
        }
    });  

    //wizard end
    var customerDataTable = $('#selected-customers-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Customer Are Selected."
            }, 
        "columnDefs": [{ targets: [5], className: 'text-center' }],
    });
    
    var insurerDataTable = $('#selected-insurers-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Members Are Selected."
            }, 
        "columnDefs": [{ targets: [6], className: 'text-center' }],
    }); 

    var nomineeDataTable = $('#selected-nominee-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Nominees Are Selected."
            }, 
    });

    var vehicleDataTable = $('#selected-vehicle-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Vehicle Are Selected."
            }, 
    });

    $('#customer-modal-div').on('hidden.bs.modal', function () {
        var customer_id = $("#customer_id").val();
        if(customer_id){
            $.ajax({
                type    : 'GET', 
                url     : base_url+"/admin/api-get-customer/"  + customer_id, 
                success : function (data) {
                    $("#wizard-error").html("").hide();
                    customerDataTable.clear();

                    var first_name    = (data.first_name) ? data.first_name: "";
                    var second_name   = (data.second_name) ? data.second_name: "";
                    var third_name    = (data.third_name) ? data.third_name: "";
                    var fourth_name   = (data.fourth_name) ? data.fourth_name: "";
                    var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                    var date          = new Date(data.date_of_birth).toDateString();
                    var dob           = date.split(" ");
					if(data.gender=="Female"){
						gender=gender+1;
							
                        }
						$('#gender_count').val(gender);
                    customerDataTable.row.add( [
                            1,
                            customer_name,
                            data.mobile_1,
                            data.gender,
                            dob[2]+'-'+dob[1]+'-'+dob[3],
                            "<a href= '#' class='' id=''><i class='fa fa-trash delete-icon'></i></a>", 

                    ]).draw( false );
					$('#insurer_sno').val(0);
					spouse_count=0;
					flag=0;
					selectedInsurer      = [];
					
					$('#selected_insurers_id').val(selectedInsurer.join(","));
					var nominee_id='';
					
					$('#selected-nominee-table').DataTable().rows().clear().draw();
					$('#selected_nominee_id').val('');
					//alert("add"+spouse_count);
                    $('#selected_customer_id').val(customer_id); 
					$('#selected_customer_gender').val(data.gender);
                }
            }); 
        } 
        $("#customer_id").val('');        
    }); 

    $('#insurer-modal-div').on('hidden.bs.modal', function () {
		flag=0;
        var insurer_id = $("#insurer_id").val();
        getPolicyMember(insurer_id);
		$(".addons-multiple").val("").text("");
		$('.selected_policy_plan_details').html("");
    });
    function getPolicyMember(insurer_id){
        if(insurer_id){
            var inArray    = jQuery.inArray(eval(insurer_id), selectedInsurer);
            //console.log("Adding insurer_id " + insurer_id);
            if (inArray >= 0) {
                $('#warning-message-modal').modal('show');
                $("#warning-message-modal .modalMessage").html("<p> Member Already Selected. </p>");  
            } else if(insurer_id) {

                $.ajax({
                    type: 'GET', 
                    url :  base_url+"/admin/api-get-customer/"  + insurer_id,
                    success : function (data) {
                        $("#wizard-error").html("").hide();
					
						
                        var first_name        = (data.first_name) ? data.first_name: "";
                        var second_name       = (data.second_name) ? data.second_name: "";
                        var third_name        = (data.third_name) ? data.third_name: "";
                        var fourth_name       = (data.fourth_name) ? data.fourth_name: "";
                        var customer_name     = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                        var insurer_sno       = $('#insurer_sno').val();
                        var insurer_serial_no = parseInt(insurer_sno)+1;
                        var relation = "";
                        var date          = new Date(data.date_of_birth).toDateString();
                        var dob           = date.split(" ");
                        if(typeof(data.family_relation) != "undefined" && data.family_relation !== null) {
                            relation = data.family_relation.relation.relationship_name;
							if(relation != "undefined" && relation =="SPOUSE" && flag==0){
								
							spouse_count=spouse_count+1;
						
							}
							if(data.gender=="Female" && relation =="SPOUSE" && flag==0){
								gender=gender+1;
							}
                        }
						if((spouse_count<=4 && flag==0)|| relation !="SPOUSE"){
						$('#gender_count').val(gender);
                        insurerDataTable.row.add( [
                            insurer_serial_no,
                            customer_name,
                            data.mobile_1,
                            data.gender,
                            dob[2]+'-'+dob[1]+'-'+dob[3],
                            relation,
                            "<a href='#'><i class='fa fa-trash delete-icon' data-id='"+data.id+"'></i></a>" 
                        ] ).draw( false );   
                        selectedInsurer.push(data.id);
                        $('#insurer_sno').val(insurer_serial_no);
                        $('#selected_insurers_id').val(selectedInsurer.join(","));
						}else if((spouse_count<=4 && flag==1) || relation !="SPOUSE"){
							$('#gender_count').val(gender);
                        insurerDataTable.row.add( [
                            insurer_serial_no,
                            customer_name,
                            data.mobile_1,
                            data.gender,
                            dob[2]+'-'+dob[1]+'-'+dob[3],
                            relation,
                            "<a href='#'><i class='fa fa-trash delete-icon' data-id='"+data.id+"'></i></a>" 
                        ] ).draw( false );   
                        selectedInsurer.push(data.id);
                        $('#insurer_sno').val(insurer_serial_no);
                        $('#selected_insurers_id').val(selectedInsurer.join(","));
							
						}else{
							
							spouse_count=spouse_count-1;
							
							$('#warning-message-modal').modal('show');
							$("#warning-message-modal .modalMessage").html("<p>Only allow 4 spouse not more than 4 spouse allowed</p>")
						}
                    }
                });  
            }          
        }
        $("#insurer_id").val('');
    }
    $('#nominee-modal-div').on('hidden.bs.modal', function () {
        var nominee_id = $("#nominee_id").val();
        if(nominee_id){
            $.ajax({
                type    : 'GET', 
                url     : base_url+"/admin/api-get-customer/"  + nominee_id, 
                success : function (data) {
                    $("#wizard-error").html("").hide();
                    nomineeDataTable.clear();

                    var first_name    = (data.first_name) ? data.first_name: "";
                    var second_name   = (data.second_name) ? data.second_name: "";
                    var third_name    = (data.third_name) ? data.third_name: "";
                    var fourth_name   = (data.fourth_name) ? data.fourth_name: "";
                    var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                    var relation = "";
                    if(typeof(data.family_relation) != "undefined" && data.family_relation !== null) {
                        relation = data.family_relation.relation.relationship_name;
                    }
                    var date          = new Date(data.date_of_birth).toDateString();
                    var dob           = date.split(" ");
                    nomineeDataTable.row.add( [
                            1,
                            customer_name,
                            data.mobile_1,
                            data.gender,
                            dob[2]+'-'+dob[1]+'-'+dob[3],
                            relation,
                            "",
                    ]).draw( false ); 
                    $('#selected_nominee_id').val(nominee_id); 
                }
            });  
        } 
        $("#nominee_id").val('');         
    });

    $('#selected-insurers-table').on("click", ".delete-icon", function(event){
        event.preventDefault();
      
		var row = $(this).closest("tr");      // Finds the closest row <tr> 
		var tds = row.find("td:eq(5)");
		var relation=tds.text();		
		if(relation=="FAMILY HEAD"){	
			$('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p>Primary Member Not Able to Delete </p>"); 
			return false;
       
		}else{
			

			var genderType=row.find("td:eq(3)");
			if(genderType.text()==="Female"){
				gender=gender-1;
			$('#gender_count').val(gender);	
		flag=1;	
			}
	
			if(relation ==="SPOUSE"){
				spouse_count=spouse_count-1;
				flag=1;
			}

					var delete_id = $(this).attr('data-id');
					var $row = $("table#selected-insurers-table tr .delete-icon[data-id='" + delete_id + "']").	closest('tr');
					$row.remove();
					var values=[];
			        var table = $("table#selected-insurers-table tr .delete-icon");					
					table.each(function(){
					var href = $(this).attr("data-id");
					values.push(href);				
					});
					selectedInsurer  = [];
					$('#selected_insurers_id').val('');   
					$('#insurer_sno').val(0);
					insurerDataTable.rows().remove().draw();					
					values.forEach(function (data) { 
					  getPolicyMember(data);
					  $(".addons-multiple").val("").text("");
					  $('.selected_policy_plan_details').html("");
				});

			/*var selectedRow  = insurerDataTable.row($(this).parents('tr'));
			selectedInsurer.splice( $.inArray(selectedRow.data()[0], selectedInsurer), 1 );
			var datas        = selectedInsurer;
			selectedInsurer  = [];
			$('#selected_insurers_id').val('');         
			$('#insurer_sno').val(0);
			insurerDataTable.clear().draw();
			datas.forEach(function (data) { 
				getPolicyMember(data);
				$(".addons-multiple").val("").text("");
			    $('.selected_policy_plan_details').html("");
			});*/
		}
	
    });  

    $('#selected-customers-table').on("click", "a", function(event){
        event.preventDefault();
        var selectedRow = customerDataTable.row($(this).parents('tr'));        
        selectedRow.remove().draw(false);
        $("#selected_customer_id").val('');
    });  

    $('#add_insurer').click(function(){
	
       
        var first_name    = $('#first_name').val();
		var relation_id   = $('#relation_id').val();
		var relation_text   = $('#relation_id').find("option:selected").text()
        var second_name   = $('#second_name').val();
        var third_name    = $('#third_name').val();
        var fourth_name   = $('#fourth_name').val();
        var gender_value        = $("input[name='insurer_gender']:checked"). val();
        var mobile_1      = $('#mobile1').val();
        var height        = $('#height').val();
        var weight        = $('#weight').val();
        var customer_id   = $("#selected_customer_id").val();
        var date_of_birth = $("#date_of_birth").val();
		

        if(relation_id== '' || first_name == '' || second_name == '' || ! gender_value || mobile_1.length <= 8 || height == '' || weight == '' || relation_id == '' || date_of_birth == '') {

            insurer_add();

        } else {

            $('#insurer-modal-add').modal('hide');

            $.ajax({
                type : 'POST', 
                url  : base_url+"/app/customers/insurer-store",
                data : {"first_name":first_name,"second_name":second_name,"third_name":third_name,
                        "fourth_name":fourth_name,"gender":gender_value,"mobile_1":mobile_1,
                        "height":height,"weight":weight,"customer_id":customer_id,
                        "relation_id":relation_id,"date_of_birth":date_of_birth}, 
                success : function (data) {

                    if(data.result == "pass"){
                    
                        var insurer_sno       = $('#insurer_sno').val();
                        var insurer_serial_no = parseInt(insurer_sno)+1;
                        var customer_name     = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
						if(gender_value=="Female" &&  relation_text =="SPOUSE"){
							gender=gender+1;
							
                        }
						$('#gender_count').val(gender);
						
                        insurerDataTable.row.add( [
                            insurer_serial_no,
                            customer_name,
                            mobile_1,
                            gender_value,
                            date_of_birth,
                            relation_text,
                            "<a href='#'><i class='fa fa-trash delete-icon'></i></a>" 
                        ]).draw( false );

                        $('#insurer_sno').val(insurer_serial_no);
                        selectedInsurer.push(data.data.id);
                        $('#selected_insurers_id').val(selectedInsurer.join(","));
                        
                    }else{
                        
                        $('#warning-message-modal').modal('show');
                        $("#warning-message-modal .modalMessage").html("<p> "+data.data+"</p>");
                    }
                }
            });
        }
	
    }); 

    function insurer_add() {

        var first_name    = $('#first_name').val();
        var second_name   = $('#second_name').val();
        var gender        = $("input[name='insurer_gender']:checked"). val();
        var mobile_1      = $('#mobile1').val();
        var height        = $('#height').val();
        var weight        = $('#weight').val();
        var relation_id   = $("#relation_id").val();
        var date_of_birth = $("#date_of_birth").val();

        if(first_name == '') {
           $("#firstNameError").html("Please Enter First Name"); 
        } 
        if(second_name == '') {
           $("#secondNameError").html("Please Enter Second Name"); 
        } 
        if(date_of_birth == '') {
           $("#dobError").html("Please Enter Date of Birth"); 
        } 
        if(! gender) {
           $("#genderError").html("Please Enter Gender"); 
        } 
        if(height == '') {
           $("#heightError").html("Please Enter Height"); 
        } 
        if(weight == '') {
           $("#weightError").html("Please Enter Weight"); 
        } 
        if(mobile_1.length <= 8) {
           $("#mobileNoError").html("Please Enter Mobile Number"); 
        } 
        if(relation_id == '') {
           $("#relationshipError").html("Please Enter Relationship"); 
        } 
			
    } 

    $('#add_nominee').click(function(){
       
        var first_name    = $('#nominee_first_name').val();
        var second_name   = $('#nominee_second_name').val();
        var third_name    = $('#nominee_third_name').val();
        var fourth_name   = $('#nominee_fourth_name').val();
        var age_group     = $('#nominee_age_group').val();
        var gender        = $("input[name='nominee_gender']:checked"). val();
        var mobile_1      = $('#nominee_mobile').val();
        var height        = $('#nominee_height').val();
        var weight        = $('#nominee_weight').val();
        var relationship  = $('#nominee_relationship').val();
        var customer_id   = $("#selected_customer_id").val();
        var date_of_birth = $("#date_of_birth2").val();

        if(first_name == '' || second_name == '' || ! gender || mobile_1.length <= 8 || height == '' || weight == '' || relationship == '' || date_of_birth == '') {

            nominee_add();

        } else {

            $('#nominee-modal-add').modal('hide');

            $.ajax({
                type : 'POST', 
                url  : base_url+"/app/customers/insurer-store",
                data : {"first_name":first_name,"second_name":second_name,"third_name":third_name,
                        "fourth_name":fourth_name,"gender":gender,"mobile_1":mobile_1,
                        "height":height,"weight":weight,"relation_id":relationship,
                        "date_of_birth":date_of_birth,"customer_id":customer_id}, 
                success : function (data) {

                    if(data.result == "pass"){
                      
                        var customer_name  = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                        
                        nomineeDataTable.clear();
                        nomineeDataTable.row.add( [
                            1,
                            customer_name,
                            mobile_1,
                            gender,
                            date_of_birth,
                            $("#nominee_relationship option:selected").text(),
                        ]).draw( false );
                       
                        $('#selected_nominee_id').val(data.data.id);
                        $('#nominee_relationship_with_insurer').val(nominee_relationship);
                    }else{
                        
                        $('#warning-message-modal').modal('show');
                        $("#warning-message-modal .modalMessage").html("<p> Duplicate Entry.Nominee Already Created. </p>");
                    }
                }
            });
        }
		

    }); 


    function nominee_add() {

        var first_name    = $('#nominee_first_name').val();
        var second_name   = $('#nominee_second_name').val();
        var gender        = $("input[name='nominee_gender']:checked"). val();
        var mobile_1      = $('#nominee_mobile').val();
        var height        = $('#nominee_height').val();
        var weight        = $('#nominee_weight').val();
        var relationship  = $('#nominee_relationship').val();
        var date_of_birth = $("#date_of_birth2").val();

        if(first_name == '') {
           $("#nomineeFirstNameError").html("Please Enter First Name"); 
        } 
        if(second_name == '') {
           $("#nomineeSecondNameError").html("Please Enter Second Name"); 
        } 
        if(date_of_birth == '') {
           $("#nomineeDobError").html("Please Enter Date of Birth"); 
        } 
        if(! gender) {
           $("#nomineeGenderError").html("Please Enter Gender"); 
        } 
        if(height == '') {
           $("#nomineeHeightError").html("Please Enter Height"); 
        } 
        if(weight == '') {
           $("#nomineeWeightError").html("Please Enter Weight"); 
        } 
        if(mobile_1.length <= 8) {
           $("#nomineeMobileNoError").html("Please Enter Mobile Number"); 
        } 
        if(relationship == '') {
           $("#nomineeRelationshipError").html("Please Enter Relationship"); 
        } 
			
    } 

    $('#selected-customers-table').on( 'click', '#customer_insurer', function () {
        var customer_id = $('#selected_customer_id').val();
        if(customer_id){
            $.ajax({
                type: 'GET', 
                url :  base_url+"/admin/api-get-customer/"  + customer_id,
                success : function (data) {
                    $("#wizard-error").html("").hide();
                    var first_name        = (data.first_name) ? data.first_name: "";
                    var second_name       = (data.second_name) ? data.second_name: "";
                    var third_name        = (data.third_name) ? data.third_name: "";
                    var fourth_name       = (data.fourth_name) ? data.fourth_name: "";
                    var customer_name     = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                    var insurer_sno       = $('#insurer_sno').val();
                    var insurer_serial_no = parseInt(insurer_sno)+1;
					if(data.gender=="Female" &&  flag==0){
						gender=gender+1;
					}
                    insurerDataTable.row.add( [
                        insurer_serial_no,
                        customer_name,
                        data.mobile_1,
                        data.gender,
                        data.age_group,
                        "<a href='#'><i class='fa fa-trash delete-icon'></i></a>" 
                    ] ).draw( false );   
                    selectedInsurer.push(data.id);
                    $('#insurer_sno').val(insurer_serial_no);
                    $('#selected_insurers_id').val(selectedInsurer.join(",")); //store array
                    //console.log("Selected Insurer List " + $('#selected_insurers_id').val());
                }
            });
        }      
			
    }); 

    $.fn.select2.defaults.set("theme","bootstrap");
    $('select[name="sum_insured"]').select2({
        //dropdownParent: '',
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url: base_url+'/app/policyAttributes/get-attributes',
            dataType: 'json',
            method:'get',
            quietMillis:1000,
            data:function (params) {
                var policy_type_id    = $("#policy_type").val();
                var attribute_type_id = 1;
                var query = {
                    search            : params.term,
                    policy_type_id    : policy_type_id,
                    attribute_type_id : attribute_type_id,
                    type              : 'public',
                }
                return query;
            }
        }
    });

    function processNextStep(form, nextView, wizardSection, dataModalDiv) {
        var formData = form.serialize();
        formData     += "&next_view=" + nextView;
        formData     += "&age_group=" + $("#member_age_goup").text();
        formData     += "&member_count=" + $("#member_count").text();
        var url      = base_url+"/app/policy/process-wizard";
        $.post(url,  formData, 
            function (data) {
                $("#wizard-error").html("").hide();
                $("#"  + wizardSection).html(data);
                
                //var tenure = $('.tenure_sum').text();
                //var year   = $('.tenure_sum').text().match(/\d/g);
                //year = year.join(""); // 1,2,3,4,5
                //var tenure_addons = parseInt(year);
                //var tot_pre_amt=parseInt($('.tot_pre_amount_sum').text());
                //$('.summary_tenure_premium').val(tot_pre_amt*tenure_addons);
                //$("#" + dataModalDiv).show();
                //return false;
            }).fail(function(){
                console.log("error");
        });      
    }

    function getHealthPricingDetails(variant_id) {
        $.ajax({
            type: "get",
            url: base_url+'/app/policy/get-pricing-details',
            data : {"id" : variant_id,"members_id":$('#selected_insurers_id').val() },
            success : function (data) {
                $('#pricing_div').html(data);   
                getPremiumAmount();
            }
        })  
    }

    function getCustomerAgainstMembers(){
        var customer_id = $("#selected_customer_id").val();
        var select      =  "Select";
        var url         = $("#baseurl").val()+'/admin/get-customer-against-members/'+customer_id;
        console.log(url);
        var modal  = $('#insurer-modal').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": url,
        "columns": [
                { data: "id", name:"id" },
                {data: "first_name", name:"first_name",render: function ( data, type, full ) {
                    var first_name  = (full["first_name"]) ? full["first_name"]: "";
                    var second_name = (full["second_name"]) ? full["second_name"]: "";
                    var third_name  = (full["third_name"]) ? full["third_name"]: "";
                    var fourth_name = (full["fourth_name"]) ? full["fourth_name"]: "";
                    var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

                    return name }},       
                { data: "mobile_1", name:"mobile_1" },
                { data: "gender", name:"gender" }, 
                { data: "date_of_birth", name:"date_of_birth" },
                { data: "relationship", name:"relationship"},          
                { data : undefined, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>", searchabe: false }                        
                ],            
            colReorder: true,
            "bDestroy": true
        });

        $('#insurer-modal').on( 'click', 'button', function () {
            var data = modal.row( $(this).parents('tr') ).data();
            $("#insurer_id").val(data.id); 
            $('#insurer-modal-div').modal('hide');
        });    
    }
    //var modalCustomerAgainstNominee='';
	$('.nominees_details').click(function(){
		if ( $.fn.DataTable.isDataTable('#nominee-modal') ) {
		    modalCustomerAgainstNominee.ajax.reload();

		}
	});
    function getCustomerAgainstNominee(){ 
        var customer_id = $("#selected_customer_id").val();
        var select      =  "Select";
        var url         = $("#baseurl").val()+'/admin/get-customer-against-members/'+customer_id;
        modalCustomerAgainstNominee  = $('#nominee-modal').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": url, 

        "columns": [
                { data: "id", name:"id" },
                {data: "first_name", name:"first_name",render: function ( data, type, full ) {
                    var first_name  = (full["first_name"]) ? full["first_name"]: "";
                    var second_name = (full["second_name"]) ? full["second_name"]: "";
                    var third_name  = (full["third_name"]) ? full["third_name"]: "";
                    var fourth_name = (full["fourth_name"]) ? full["fourth_name"]: "";
                    var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

                    return name }},       
                { data: "mobile_1", name:"mobile_1" },
                { data: "gender", name:"gender" }, 
                { data: "date_of_birth", name:"date_of_birth" },
                { data: "relationship", name:"relationship"},         
                { data : undefined, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>", searchabe: false }                        
                ],            
            //colReorder: true,
           "bDestroy": true
        });

        $('#nominee-modal').on( 'click', 'button', function () {			
            var data = modalCustomerAgainstNominee.row( $(this).parents('tr') ).data();
            $("#nominee_id,#selected_nominee_id").val(data.id); 
            $('#nominee-modal-div').modal('hide');
        });    
    }
	
	
	function caluclation_permium(){
		 var installment_type = $('#installment_type').val();
			
            var tenure = $('#tenure').val();
            var period_year = ($('#tenure').val()).split(' ');
            // var year = tenure.match(/\d/g);
            var year = period_year[0];
            // year = year.join(""); // 1,2,3,4,5
            var tenure_addons = parseInt(year);
			if(installment_type==1){
				year = year;
				var total_installment = year;
				var tenure_installment=1;
			}else if(installment_type==12){
				 year = 1;
				 var total_installment =1;
				 var tenure_installment=1;
				//var total_installment = year/installment_type;
			}
           // year = year*12;
          //  var total_installment = year/installment_type;
			if(!isNaN(total_installment)){

            $('#no_of_installment').val(total_installment);
			}
            var total_premium_amount = $('#total_premium_amount').val();
            var total_amount = (total_premium_amount/total_installment).toFixed(2);
           // $('#installment_amount').val(total_amount);
             var pre_amt=$('#original_premium_amount').val();
			 var total_premium=	$("#premium_amount").val();
            var sum = 0;
            var values=$("input[name='pre[]']").map(function(){return $(this).val();}).get();
            for (var i=0;i<values.length;i++) {
                sum += parseInt(values[i]);
            }   
			if(sum!=0){			
			var addonSum=parseFloat(pre_amt.replace(",",""))+parseFloat(sum);
			var total_monthly=parseFloat(addonSum)/parseInt(12);
			var total_premium=parseFloat(total_monthly)*parseInt(year);
			}
		
			var addonSum=parseFloat(pre_amt.replace(",",""))+parseFloat(sum);	
			//new installment amount calculation
			if(!isNaN(addonSum)){	
			$('#premium_addons').val(parseFloat(total_premium).toFixed(2));
			var total_premium_amount=total_premium;
			//var total_premium_amount=parseInt(addonSum)*parseInt(tenure_addons);
			
            //$('#premium_addons').val(parseFloat(addonSum).toFixed(2));
            $('#total_premium_amount').val(parseFloat(total_premium_amount).toFixed(2));
			}
			endDate();
	}
	 function endDate() {
        var period_year = ($('#tenure').val()).split(' ');
        var start_date_picker = ($('#start_date').val()) ? $('#start_date').val() : null;
        getPremiumAmount();
        if (start_date_picker != null) {
            var start_date = moment(start_date_picker.format('DD-MM-YYYY'));
            var end_date = start_date.clone();
            if (period_year[0]) {
               end_date.add(period_year[0], 'years');
               end_date.subtract(1,'d');
            }
            $("#end_date").datepicker("setDate", end_date.format('DD-MM-YYYY'));
        }
    }

    function getPremiumAmount(){
        var period_year = ($('#tenure').val()).split(' ');
        var line_of_business = $("#line_of_business_text").val();

        if(line_of_business =="HEALTH" && period_year[0]){
            var member_count     = $("#member_count").text();
            var member_age_goup  = $("#member_age_goup").text();
            var pricing_id       = $("#pricing_id").val();
			var period_year = ($('#tenure').val()).split(' ');
			var year = period_year[0];
            $.ajax({
                type: "get",
                url : $("#base_url").val()+"/app/policy/get-premium-amount",
                data : {"age_group":member_age_goup ,"member_count":member_count,"pricing_id":pricing_id,"year":year},
                success : function (data) {
                         $("#original_premium_amount").val(data);
					 var tenure = $('#tenure').val();
					var period_year = ($('#tenure').val()).split(' ');
					//var total_premium = parseFloat(data) * period_year[0];
                    var total_premium = parseFloat(data);
                   // var add_on_amount = $("#premium_addons").val();
					  var sum = 0;
					 
					 var total_premiums =parseFloat(data.replace(",",""));
					var total_monthly=parseFloat(total_premiums)/parseInt(12);
					var total_premium=parseFloat(total_monthly)*parseInt(period_year[0]);
					$("#premium_amount").val(total_premium.toFixed(2));
					var values=$("input[name='pre[]']").map(function(){return $(this).val();}).get();
						for (var i=0;i<values.length;i++) {
						sum += parseInt(values[i]);
					} 
					
					var add_on_amount = sum;
					// var year = tenure.match(/\d/g);
					var year = period_year[0];
					// year = year.join(""); // 1,2,3,4,5
					//var tenure_addons = parseInt(year);
					var tenure_addons = 1;
			
					var addonSum=0;
                    if(sum!=""){
						
					var premium_amount =parseFloat(data.replace(",",""));	
					var total_premium_amounts=parseFloat(premium_amount)+parseFloat(sum)*tenure_addons;
                    total_premiums = total_premium_amounts;
					var total_monthly=parseFloat(total_premiums)/parseInt(12);
					var total_premium=parseFloat(total_monthly)*parseInt(year);
					
						
					//addonSum=parseFloat(premium_amount.replace(",",""))+parseFloat(add_on_amount);
		
				    //$('#premium_addons').val(parseFloat(addonSum).toFixed(2));
                    }
					$('#premium_addons').val(total_premium.toFixed(2));
					//var total_premium_amount=parseFloat(premium_amount.replace(",",""))+parseInt(add_on_amount);
					//var total_premium_amounts=parseFloat(total_premium_amount)*tenure_addons;
				var total_premium_amounts=total_premium;
					
					if(!isNaN(total_premium_amounts)){
                    $("#total_premium_amount").val(total_premium_amounts.toFixed(2));
					
					}
					
					var total_premium_amount=$("#total_premium_amount").val();
					
					
			var no_of_installment=$("#no_of_installment").val();
			if(no_of_installment!=''){
				var installement_amount=total_premium_amount/no_of_installment;			
				$('#installment_amount').val(installement_amount.toFixed(2));
			}
                }
            });
        }
    }

});

