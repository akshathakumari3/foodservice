  require([
      "esri/Map",
      "esri/layers/FeatureLayer",
      "esri/views/MapView",
      "esri/layers/ImageryLayer",
      "esri/widgets/LayerList",
      "esri/widgets/Search",
      "esri/widgets/DistanceMeasurement2D",
      "esri/widgets/AreaMeasurement2D"
      
    ], function(
      Map,
      FeatureLayer,
      MapView,
      ImageryLayer,
      LayerList,
      Search,
      DistanceMeasurement2D,
      AreaMeasurement2D  
    ) {

        var image_api = "{!! env('ARCGIS_MAP_SERVER_HOST') !!}"+"{!! env('ARCGIS_IMAGE_SERVER_API') !!}";
      var layer = new ImageryLayer({
          url: image_api,
          format: "jpgpng" 
        });

      var map = new Map({
        basemap: "satellite",
        //layers: [layer]
      });

      var view = new MapView({
        container: "viewDiv",
        map: map,
        zoom: 12,
        center: [44.060063, 9.561173] 
      });
      //Identify task code
      var template = {
        title: "Identify",
        content: "Name: {Street_Name}"
       
      };
    var map_server = "{!! env('ARCGIS_MAP_SERVER_HOST') !!}"+"{!! env('ARCGIS_MAP_API') !!}";
    var x          = '0';
    var y          = '1';
    var z          = '2';
      streets = new FeatureLayer({

        url: map_server+x,
        outFields: ["*"],
        popupTemplate: template //identify task enabled to this layer
      });

      var Parcel = new FeatureLayer({
          url: map_server+y,
          outFields: ["*"]
        });
       var DistrictBoundary = new FeatureLayer({
          url: map_server+z,
          outFields: ["*"]
        });
      map.add(streets);
      map.add(Parcel);
      map.add(DistrictBoundary);

       //Identify task code
      view.popup.on("trigger-action", function(event) {
      });


      //Layer List/ table of content code
      var layerList = new LayerList({
        view: view
      });

      view.ui.add(layerList, {
        position: "top-right"
      });


      // Measure distance and area code
      var activeWidget = null;
      view.ui.add("topbar", "top-left");

      document.getElementById("distanceButton").addEventListener("click",
        function () {
          setActiveWidget(null);
          if (!this.classList.contains('active')) {
            setActiveWidget('distance');
          } else {
            setActiveButton(null);
          }
        });

      document.getElementById("areaButton").addEventListener("click",
        function () {
          setActiveWidget(null);
          if (!this.classList.contains('active')) {
            setActiveWidget('area');
          } else {
            setActiveButton(null);
          }
        });

      function setActiveWidget(type) {
        switch (type) {
          case "distance":
            activeWidget = new DistanceMeasurement2D({
              view: view,
              unit: "meters",
              mode: "planar"
            });

            activeWidget.viewModel.newMeasurement();

            view.ui.add(activeWidget, "top-left");
            setActiveButton(document.getElementById('distanceButton'));
            break;
          case "area":
            activeWidget = new AreaMeasurement2D({
              view: view
            });

            // skip the initial 'new measurement' button
            activeWidget.viewModel.newMeasurement();

            view.ui.add(activeWidget, "top-left");
            setActiveButton(document.getElementById('areaButton'));
            break;
          case null:
            if (activeWidget) {
              view.ui.remove(activeWidget);
              activeWidget.destroy();
              activeWidget = null;
            }
            break;
        }
      }

      function setActiveButton(selectedButton) {
        // focus the view to activate keyboard shortcuts for sketching
        view.focus();
        var elements = document.getElementsByClassName("active");
        for (var i = 0; i < elements.length; i++) {
          elements[i].classList.remove("active");
        }
        if (selectedButton) {
          selectedButton.classList.add("active");
        }
      }
     
    });

