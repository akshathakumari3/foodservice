"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");

    $('#save-product').click(function(){
        if(!$('#line_of_business_id').val()){
            $("#wizard-error").html("Please Enter Line Of Business.").show();
            goTop();
            return false;
        }

        if(!$('#product_name').val()){
            $("#wizard-error").html("Please Enter Product.").show();
            goTop();
            return false;
        }
		//$('#product-create-form').submit();
        return true;
    });
	
	 $('#save-product-option').click(function(){
        if(!$('#line_of_business_id').val()){
            $("#wizard-error").html("Please Enter Line Of Business.").show();
            goTop();
            return false;
        }

        if(!$('#product_id').val()){
            $("#wizard-error").html("Please Enter Product.").show();
            goTop();
            return false;
        }
		  if(!$('#name').val()){
            $("#wizard-error").html("Please Enter product option.").show();
            goTop();
            return false;
        }
        return true;
    });

    function goTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
});

