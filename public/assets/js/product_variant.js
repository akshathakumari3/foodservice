"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");

    $('#product_variants').click(function(){
    if(!$('#line_of_business').val()){
        $("#wizard-error").html("Please Enter Line Of Business.").show();
        goTop();
        return false;
    }

    if(!$('#product_name').val()){
        $("#wizard-error").html("Please Enter Product.").show();
        goTop();
        return false;
    }

    if(!$('#product_option').val()){
        $("#wizard-error").html("Please Enter Product Option.").show();
        goTop();
        return false;
    }

    if(!$('#product_variant').val()){
        $("#wizard-error").html("Please Enter Product Variant.").show();
        goTop();
        return false;
    }
    return true;
    });

    function goTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
           return false;
        }else{
           return true;
        }
      }
});

