"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");
    var base_url = $('#base_url').val();
    var form     = $("#customer_form"); 

    // manage wizard 
    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            
            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            
            // dont change the order of the checks
            // Customer  Information
            if (newIndex === 1) {

                // if(!$('#customer_type').val()){
                //     $("#wizard-error").html("Please Select Customer Type.").show();
                //     return false;
                // }

                if(!$('#relation_id').val()){
                    $("#wizard-error").html("Please Select Relationship.").show();
                    return false;
                }

                if(!$('#first_name').val()){
                    $("#wizard-error").html("Please Enter First Name.").show();
                    return false;
                }

                if(!$('#second_name').val()){
                    $("#wizard-error").html("Please Enter Second Name.").show();
                    return false;
                }

                if(!$('#third_name').val()){
                    $("#wizard-error").html("Please Enter Third Name.").show();
                    return false;
                }

                if(!$('#date_of_birth').val()){
                    $("#wizard-error").html("Please Select Date Of Birth.").show();
                    return false;
                }
                
                if(!$("input[name='gender']:checked"). val()){
                    $("#wizard-error").html("Please Select Gender.").show();
                    return false;
                }

                
                var relation = $("#relation_id").children("option:selected").text().toLowerCase();
                
                if(relation != 'child' && !$('#customer_id').val() && !$('#occupation').val()){
                    $("#wizard-error").html("Please Select Occupation.").show();
                    return false;
                }

                if(relation != 'child' && !$('#customer_id').val() && !$('#employer').val()){
                    $("#wizard-error").html("Please Enter Employer.").show();
                    return false;
                }

                if($("#form_type").val() == 'member' && !$('#member_group').val() && $("#relation_id").val() == 1 ){
                    $("#wizard-error").html("Please Select Client Member Group.").show();
                    return false;
                }
				
				
                $("#wizard-error").html("").hide();
                return true;
            }

            // Photo
            if (newIndex === 2) {

             /*   if(!$('#customer_image').val() && !$('#customer_old_image').val()){
                    $("#wizard-error").html("Customer Image is Required.").show();
                    return false;
                }*/
                
                $("#wizard-error").html("").hide();
                return true;
            }

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex) {
            $("#wizard-error").html("").hide();
            if(!$("input[name='medication']:checked"). val()){
                $("#wizard-error").html("Please Fill Health Questions 1 to 5.").show();
                return false;
            }

            if(!$("input[name='current_medication']:checked"). val()){
                $("#wizard-error").html("Please Fill Health Questions 1 to 5.").show();
                return false;
            }

            if(!$("input[name='surgical_history']:checked"). val()){
                $("#wizard-error").html("Please Fill Health Questions 1 to 5.").show();
                return false;
            }

            if(!$("input[name='tobacco']:checked"). val()){
                $("#wizard-error").html("Please Fill Health Questions 1 to 5.").show();
                return false;
            }

            if(!$("input[name='illegal_drugs']:checked"). val()){
                $("#wizard-error").html("Please Fill Health Questions 1 to 5.").show();
                return false;
            }
			
			
			
			var rowCount = $('#selected_details_table tr').length;
			var added=false;
			$('.medicalQuestion:checked').each(function() {
				if(this.value=="Yes"){
					added=true;
					
				}
			});
			if(added==true){
				if(rowCount==1){
					$("#wizard-error").html("Please fill the additional Details.").show();
					return false;
				}			
			}
			
			if($("input[name='pregnant_status']:checked").val()=="Yes"){
				
				if($('#pregnant_detail').val()==""){
					$("#wizard-error").html("Please Enter number of weeks of pregnancy").show();
					return false;
				}
			}
			if($("input[name='drug_allergic']:checked").val()=="Yes"){
				if($('#drug_details').val()==""){
					$("#wizard-error").html("Please Enter allergic details ").show();
					return false;
				}
			}
			
			if($("input[name='medical_insurance']:checked").val()=="Yes"){
				if($('#medical_insurance_detail').val()==""){
					$("#wizard-error").html("Please Enter Medical Insurance details ").show();
					return false;
				}
			}
			
           // form.validate().settings.ignore = ":disabled";
            //$("#wizard-error").html("Form Not valid").show();
            return form.valid();

        },
        onFinished: function (event, currentIndex) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: base_url+'/app/customers/store', 
                type: 'post',
                data: form.serialize(),
                success : function (data) {
                    if(data.result == 'pass'){
                        $("#gis-loader").css("display", "block");
                        $("#wizard-error").html("").hide();
                        window.location = base_url+"/app/customers/"+data.customer.id;
                    }else{
                        window.location = window.location;
                    }
                }, 
				error : function(XMLHttpRequest, textStatus, errorThrown) { 
							alert("Error: " + XMLHttpRequest.responseJSON.message); 
						}       
            }); 
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            console.log('error last'+error);
            element.before(error);
        }
    });  
    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
           return false;
        }else{
           return true;
        }
      }
});

