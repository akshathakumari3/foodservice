$('#title_serial_number_update').click(function () {
	var title_serial_no = $("#title_certificate_serial_no").val();
	if(title_serial_no){
	var transaction_id  = $("#transaction_id").val();
	var property_id     = $("#property_id").val();
	var type            = "print";
	var url             = $('#baseurl').val()+"/admin/properties/serialNumber";

    $.ajax({
        type: 'post', 
        url : url, 
        data : {"transaction_id":transaction_id, "property_id":property_id,
               "title_serial_no":title_serial_no, "type":type},
        success : function (data) {
            if(data == "pass"){
            	$("#property-seriel-number-modal-div").hide();
                $("#transaction-checklist-modal-div").show();
            }else{
            	$("#warning-message-modal .modalMessage").html("Error");
                $("#warning-message-modal").modal("show");
            }
            
        }
    });

	}else{
		
		var text ='Please Select Title Certificate Serial No';
        $("#warning-message-modal .modalMessage").html(text);
        $("#warning-message-modal").modal("show");
	}

	 

});