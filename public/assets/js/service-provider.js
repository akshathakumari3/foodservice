"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");

    $('#servicep').click(function(){
        if(!$('#service_provider_name').val()){
        $("#wizard-error").html("Please Enter Service Provider Name.").show();
        goTop();
        return false;
    }

    if(!$('#contact_person_name').val()){
        $("#wizard-error").html("Please Enter Contact Person.").show();
        goTop();
        return false;
    }

    if(!$('#contact_number_1').val()){
        $("#wizard-error").html("Please Enter Contact Number.").show();
        goTop();
        return false;
    }

    if(!$('#contact_email').val()){
        $("#wizard-error").html("Please Enter Contact Email.").show();
        goTop();
        return false;
    }

    if(IsEmail($('#contact_email').val())==false){
        $("#wizard-error").html("Please Enter Valid Format Email ID.").show();
        goTop();
        return false;
    }

    if(!$('#contact_number_2').val()){
        $("#wizard-error").html("Please Enter Alt Contact Number.").show();
        goTop();
        return false;
    }


    if(!$('#contact_address').val()){
        $("#wizard-error").html("Please Enter the Address.").show();
        goTop();
        return false;
    }

    if(!$('#district').val()){
        $("#wizard-error").html("Please Select District.").show();
        goTop();
        return false;
    }

    if(!$('#sub_district').val()){
        $("#wizard-error").html("Please Select Sub District.").show();
        goTop();
        return false;
    }

    if(!$('#state').val()){
        $("#wizard-error").html("Please Select State.").show();
        goTop();
        return false;
    }

    if(!$('#country').val()){
        $("#wizard-error").html("Please Select Country.").show();
        goTop();
        return false;
    }

    if(!$('#contact_postcode').val()){
        $("#wizard-error").html("Please Enter Post Code.").show();
        goTop();
        return false;
    }

    if(!$('#latitude').val()){
        $("#wizard-error").html("Please Enter Latitude.").show();
        goTop();
        return false;
    }

    if(!$('#longitude').val()){
        $("#wizard-error").html("Please Enter Longitude.").show();
        goTop();
        return false;
    }

    if(!$('#number_of_doctors').val()){
        $("#wizard-error").html("Please Enter Numer of Doctors.").show();
        goTop();
        return false;
    }

    if(!$('#number_of_rooms').val()){
        $("#wizard-error").html("Please Enter Number of Rooms.").show();
        goTop();
        return false;
    }

    if(!$('#number_of_beds').val()){
        $("#wizard-error").html("Please Enter Number of Beds.").show();
        goTop();
        return false;
    }

    return true;
    });

    function goTop(){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
           return false;
        }else{
           return true;
        }
      } 
});

