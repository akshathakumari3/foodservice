// Grab elements, create settings, etc.
var video = document.getElementById('video');

function callBuyerVideoPlay() {
    $("#customer_edit_image").hide();
    $(".customer-camera").show();
    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            //video.src = window.URL.createObjectURL(stream);
            video.srcObject = stream;
            video.play();
        });
    }
}

// Elements for taking the snapshot
var canvas  = document.getElementById('canvas');
var context = canvas.getContext('2d');
var video   = document.getElementById('video');

// Trigger photo take
document.getElementById("snap").addEventListener("click", function() {
    //document.getElementById("store_claim_photo").style.display = "block";
    context.drawImage(video, 0, 0, 190, 190);
    html2canvas([document.getElementById('canvas')], {
          width: 1200,
          height: 1200,
        onrendered: function (canvas) {
            var photo_data = canvas.toDataURL('image/png'); 
            customer_photo = photo_data.replace(/^data:image\/(png|jpg);base64,/, "");
            $("#claimed_photo").val(customer_photo);
        }
    });

    // document.getElementById("snap").addEventListener('click', function() {
    // context.drawImage(video, 0, 0, 300, 300);
    // var photo_data = canvas.toDataURL('image/png');
    // console.log(photo_data);
    // customer_photo    = photo_data.replace(/^data:image\/(png|jpg);base64,/, "");
    // $("#customer_image").val(customer_photo);
    // console.log(customer_photo);
      
    // });

    
});

