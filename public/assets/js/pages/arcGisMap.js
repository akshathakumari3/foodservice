$(document).ready(function () {
	var baseurl           = $('#baseurl').val();
	var gis_reference_id  = $("#gis_reference_id").val();
	var imagery_layer     = $("#imagery_layer").val();
	var imagery_layer_2   = ($("#imagery_layer_2").val()) ? $("#imagery_layer_2").val() : null;
	var imagery_layer_3   = ($("#imagery_layer_3").val()) ? $("#imagery_layer_3").val() : null;
	var imagery_layer_4   = ($("#imagery_layer_4").val()) ? $("#imagery_layer_4").val() : null;
	var imagery_down      = ($("#imagery_down").val()) ? $("#imagery_down").val() : null;
	var center_point      = $("#center_point").val();
	var view_page		  = $("#view_page").val();

	if(view_page=="summary_page_map"){
			container  = "summary_property_map";
		}else{
			container  = "viewDiv";
		}

	if(gis_reference_id!="" || gis_reference_id == null){
		
		$.ajax({
			type: "get",
            url : baseurl+'/admin/arcGis/getPropertyPolygon',
            data : {"globalId":gis_reference_id},
            success : function (data) {
            	
            	if(data != 'false'){

            		if(data[0]){

            			center_point = data[0][0];

            			require([
					        "dojo/promise/Promise",
					        "esri/Map",
					        "esri/views/MapView",
					        "esri/layers/ImageryLayer",
					        "esri/Graphic",
					      	"esri/geometry/Polygon",
					      	"esri/symbols/SimpleFillSymbol",
					      	"esri/layers/GraphicsLayer",
					      	"esri/core/watchUtils",
					      	"esri/tasks/PrintTask",
					      	"esri/tasks/support/PrintTemplate",
					      	"esri/tasks/support/PrintParameters",
					      	"dojo/on",
					      	"dojo/domReady!",
		      	
		      			], function(Promise,Map, MapView, ImageryLayer,Graphic, Polygon,
		      				SimpleFillSymbol,GraphicsLayer,watchUtils,PrintTask, PrintTemplate, PrintParameters,on) {
		        /********************
		         * Create image layer
		         ********************/
		         
				        var layer = new ImageryLayer({
				          url:[imagery_layer],
				          format: "jpgpng" // server exports in either jpg or png format
				        });
					
						imagery_layers = [layer];
						
						if(imagery_layer_2 != null){
							var layer_2 = new ImageryLayer({
								url:[imagery_layer_2],
								format: "jpgpng" // server exports in either jpg or png format
							  });
							imagery_layers.push(layer_2);
						}

						if(imagery_layer_3 != null){
							var layer_3 = new ImageryLayer({
								url:[imagery_layer_3],
								format: "jpgpng" // server exports in either jpg or png format
							  });
							imagery_layers.push(layer_3);
						}

						if(imagery_layer_4 != null){
							var layer_4 = new ImageryLayer({
								url:[imagery_layer_4],
								format: "jpgpng" // server exports in either jpg or png format
							  });
							imagery_layers.push(layer_4);
						}

						if(imagery_down != null){
							var imagery_down_layer = new ImageryLayer({
								url:[imagery_down],
								format: "jpgpng" // server exports in either jpg or png format
							});
							imagery_layers.push(imagery_down_layer);
						}

						

			        /**************************
			         * Add image layer to map
			         *************************/

					        var map = new Map({
					          basemap: "gray",
					          layers: imagery_layers
					        });

					        var view = new MapView({
					          container: container,
					          map: map,
					          center: data[0][0],
					          zoom: 20
					        });

					        view.when(function(){
						        var polygon = {
						          type: "polygon",
						          rings :data
					         	};
						       
					         	var simpleFillSymbol = {
						           type: "simple-fill",
						           //color: [255, 255, 255, 0.8],  // orange, opacity 80%
						           outline: {
						             color: [0, 0, 255],
						             width: 2
						           }
					         	};
					          	var polygonGraphic = new Graphic({
						           geometry: polygon,
						           symbol: simpleFillSymbol
					          	});
						          
					          	var layers = new GraphicsLayer({
						            graphics: [polygonGraphic]
					          	});

					      		map.layers.add(layers);
					    	});

				      	});
            	    }
            	}
            }
    	});
	}
});