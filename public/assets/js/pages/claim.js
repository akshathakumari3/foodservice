var baseurl                 = $("#baseurl").val();
var claim_service_fee_items = [];

$("#search_policy_details").on("click",function(){
	
	var policy_no  = $("#policy_no").val();
	$("#policy_no_error").text("");

	if(policy_no == "" || policy_no == null){
		$("#policy_no_error").text("Enter Policy Number");
		$("#customers_div").css("display", "none");
		$("#submit_div").css("display", "none");
		return false;
	}
    searchPolicyDetails(policy_no,'notverify');
    getBedType(policy_no);
})

function getBedType(policy_no){
         $.ajax({
        type: 'get',
        dataType:'json', 
        url : baseurl+"/app/claim/get-bed-types", 
        data : {"policy_no":policy_no,'claim_id':$('#claim_id').val()},
        success : function (data) {
            if(data.option_value){
                $.each(data.option_value, function(key, value) {   
                $('#bed_type')
                 .append($("<option></option>")
                    .attr("value", value)
                    .text(value)); 
                });
            }
        }
         }); 
    }

function searchPolicyDetails(policy_no,type,id) {
    document.getElementById("policy_no").readOnly = true;
    $.ajax({
        type: 'get', 
        url : baseurl+"/app/claim/get-policy-details", 
        data : {"policy_no":policy_no,"type":type},
        success : function (data) {
            
            if(data == 'fail'){
                document.getElementById("policy_no").readOnly = false;
                $("#policy_no_error").text("No Result.");
                $("#customers_div").css("display", "none");

            }else if(data=="notpaid"){
				document.getElementById("policy_no").readOnly = false;
                $("#policy_no_error").text("Policy Not Fully Paid");
                $("#customers_div").css("display", "none");
			}else{  
                
                $("#policy_customer_details").html(data);
                $("#policy_application_id").val($("#policy_details").val());
                $("#customers_div").css("display", "block");
				$("#claim_select_option_"+id).prop('checked', true);
            }
			
        }
    }); 
}






$("#save_service").on("click",function(){
	var sl      	 = parseInt($("#sl_no").val())+1;
	var service 	 = $("#service_code").val();
	var service_name = $("#service_code").children("option:selected").text();
	var service_description  = $("#service_description").val();
	var amount_limit         = $("#amount_limit").val();
    var session_limit        = $("#session_limit").val();
    //var service_details      = $("#service_details").val();
    var amount_billed        = $("#amount_billed").val();
    var service_charges      = $("#service_charges").val();
    var consultation_charges = $("#consultation_charges").val();
	$("#service_error").text("");
    $("#service_details_error").text("");
    $("#total_amount_error").text("");
    $("#service_charges_error").text("");
    $("#consultation_charges_error").text("");
	amount_billed = $("#amount_billed").val();

	var balance_sum_insured = $("#balance_sum_insured").val();
	if(service == "" || service == null){
		$("#service_error").text('Select Service');
		return false;
	}
    /*if(service_details == "" || service_details == null){
        $("#service_details_error").text('Enter Service Details');
        return false;
    }*/
    if(consultation_charges == "" || consultation_charges == null){
        $("#consultation_charges_error").text('Enter Consultation Amount');
        return false;
    }
    
    if(amount_billed == "" || amount_billed == null){
        $("#total_amount_error").text('Enter Total Amount');
        return false;
    }
		if(parseFloat(amount_billed) > parseFloat(balance_sum_insured) ){
		swal({
            title: "Available Claim Amount $"+balance_sum_insured+" Only",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        }); 

        return false;
	}

    
	 // console.log(claim_service_fee_items);
	var service_fee = { }; 
	service_fee["inclusion_id"] 	    = $("#service_inclusion_id").val();
    service_fee["service_id"]           = service;
    service_fee["service_code"]         = service_name;
    service_fee["service_description"]  = service_description;
    service_fee["amount_limit"]         = amount_limit;
	service_fee["session_limit"]        = session_limit;
	service_fee["service_details"]      = "";
    service_fee["consultation_charges"] = $("#consultation_charges").val();
    service_fee["bed_charges"]          = $("#bed_charges").val();
    service_fee["service_charges"]      = $("#service_charges").val();
    service_fee["drug_charges"]         = $("#drug_charges").val();
    service_fee["other_charges"]        = $("#other_charges").val();
    service_fee["amount_billed"]        = amount_billed;
	claim_service_fee_items.push(service_fee);
	var services_count =parseInt($("#services_count").val());
	$("#service_fee_items").val(JSON.stringify(claim_service_fee_items));

	var markup = "<tr class='serviceTable'><td>" + service_name + "</td><td align='right'><input type='hidden' name='service_id' id='service_id' value='"+service+"'><input type='hidden' name='inclusion_id' id='inclusion_id' value='"+$("#service_inclusion_id").val()+"'><input type='hidden' name='service_code' id='service_codes' value='"+service_name+"'><input type='hidden' name='service_description' id='service_descriptions' value='"+service_description+"'><input type='hidden' name='amount_limit' id='amount_limits' value='"+amount_limit+"'><input type='hidden' name='session_limit' id='session_limits' value='"+session_limit+"'><input type='hidden' name='service_details' id='service_details' value=''><input type='hidden' name='drug_charges' id='drug_chargescreate' value='"+$("#drug_charges").val()+"'><input type='hidden' name='other_charges' id='other_charges_create' value='"+$("#other_charges").val()+"'>" + (($("#consultation_charges").val()) ? parseFloat($("#consultation_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + (($("#service_charges").val()) ? parseFloat($("#service_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + (($("#bed_charges").val()) ? parseFloat($("#bed_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + (($("#drug_charges").val()) ? parseFloat($("#drug_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" +(($("#other_charges").val()) ? parseFloat($("#other_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + amount_billed + "</td><td class=\"text-center\"><a id=\"delete-service-feee\"><i class=\"fa fa-trash user-delete\"" + "aria-hidden=\"true\"></i></a></td> <td style='display:none'>" + service + "</td></tr>";
    $("table#services_table").append(markup);
    $("#service-modal-add").modal('hide');
    var total_amount = parseInt($("#total_amount").val())+parseInt(amount_billed);
    $("#total_amount").val(total_amount);
    $("#day_total_amount").val(total_amount);
    $("#sl_no").val(sl);
    $("#services_count").val(services_count+parseInt(1));
    $("#service_code").empty().trigger('change');
    $("#service_description").val("");
    $("#amount_limit").val("");
    $("#session_limit").val("");
    $("#service_charges").val("");
    $("#bed_charges").val("");
    $("#drug_charges").val("");
    $("#other_charges").val("");
    $("#service_type2").val("");
    $("#service_details").val("");
    $("#consultation_charges").val("");
    //$("#amount_div").css("display", "block");
    $("#submit_div").css("display", "block");
    document.getElementById("services_div").style.display = "none";
    document.getElementById("services_action_div").style.display = "none";
    document.getElementById("limit_div").style.display = "none";
})

$("#policy_no").on("keyup", function(){
    if ($("#policy_no").val()) {
        $("#refresh_button").show();
    }else{
        $("#refresh_button").hide();
    }
})

$('#services_table').on("click", "a", function(event){ 
	claim_service_fee_items = [];
    var t = $(this).closest('tr').remove();
    var index2 = $(t).find('td:eq(1)').text();
   
	var claim_id=$('#claim_id').val(); 
	if(claim_id && claim_id!=''){
	 var totalAmount =$('#total_amount').val()
	  var currentTotals = $(t).find('td:eq(6)').text().replace('$','');
	  var currentTotal=currentTotals.replace(',','');
	}else{
    var totalAmount = $('#total_amount').val();
	 var currentTotal = $(t).find('td:eq(5)').text().replace('$','');
	}
	
    var overallTotal = parseFloat(totalAmount) - parseFloat(currentTotal);
    $('#total_amount').val(Number(overallTotal).toFixed(2));
    $('#day_total_amount').val(Number(overallTotal).toFixed(2));
    var services_count =parseInt($("#services_count").val())
    $("#services_count").val(services_count-parseInt(1));
	if(claim_id && claim_id!=''){
	    $('#services_table tbody tr').each(function(row, tr){
		var service_fee = { }; 
        service_fee["inclusion_id"]         = $(tr).find('#inclusion_id').val();
		service_fee["service_id"]         =  $(tr).find('#service_id').val();
        service_fee["service_code"]         = $(tr).find('#service_codes').val();
        service_fee["service_description"]  = $(tr).find('#service_descriptions').val();
        service_fee["amount_limit"]         = $(tr).find('#amount_limits').val();
        service_fee["session_limit"]        = $(tr).find('#session_limits').val();
        service_fee["service_details"]      = $(tr).find('#service_details').val();
        service_fee["amount_billed"]        = $(tr).find('#amount_billed').val();
		service_fee["service_details"]      = "";
		service_fee["consultation_charges"] = $(tr).find('#consultation_charges').val();
		service_fee["bed_charges"]          = $(tr).find('#bed_charges').val();
		service_fee["service_charges"]      = $(tr).find('#service_charges').val();
		service_fee["drug_charges"]         = $(tr).find('#drug_chargescreate').val();
		service_fee["other_charges"]        = $(tr).find('#other_charges_create').val();
		claim_service_fee_items.push(service_fee);
    });
	}
	else{
		
   	$('#services_table tbody tr').each(function(row, tr){
		var service_fee = { }; 
		var amount_billed=$(tr).find('td:eq(6)').text().replace('$','');
		var total_amount=0;
		service_fee["inclusion_id"]         = $(tr).find('#inclusion_id').val();
		service_fee["service_id"]         =  $(tr).find('#service_id').val();
        service_fee["service_code"]         = $(tr).find('#service_codes').val();
        service_fee["service_description"]  = $(tr).find('#service_descriptions').val();
        service_fee["amount_limit"]         = $(tr).find('td:eq(2)').text();
        service_fee["session_limit"]        = $(tr).find('td:eq(3)').text();
        service_fee["service_details"]      = $(tr).find('td:eq(4)').text();
        service_fee["amount_billed"]        = $(tr).find('td:eq(5)').text().replace('$','');;
		service_fee["consultation_charges"] = $(tr).find('td:eq(1)').text();
		service_fee["bed_charges"]          = $(tr).find('td:eq(3)').text();
		service_fee["service_charges"]      = $(tr).find('td:eq(2)').text();
		service_fee["drug_charges"]         = $(tr).find('#drug_charges').val();
		service_fee["other_charges"]        = $(tr).find('#other_charges').val();
		claim_service_fee_items.push(service_fee);
		var total_amount = parseInt(total_amount)+parseInt(amount_billed);
		$("#total_amount").val(total_amount);
		$("#day_total_amount").val(total_amount);
        /*service_fee["inclusion_id"]         = $(tr).find('td:eq(6)').text();
        service_fee["service_code"]         = $(tr).find('td:eq(0)').text();
        service_fee["service_description"]  = $(tr).find('td:eq(1)').text();
        service_fee["amount_limit"]         = $(tr).find('td:eq(2)').text();
        service_fee["session_limit"]        = $(tr).find('td:eq(3)').text();
        service_fee["service_details"]      = $(tr).find('td:eq(4)').text();
        service_fee["amount_billed"]        = $(tr).find('td:eq(5)').text().replace('$','');;
		claim_service_fee_items.push(service_fee);*/
    });
	
	}
    // console.log(claim_service_fee_items);
	$("#service_fee_items").val(JSON.stringify(claim_service_fee_items));
	
	
	
});

$("#edit_save_service").on("click",function(){
	var sl      	 = parseInt($("#sl_no").val())+1;
	var service 	 = $("#edit_service_code").val();
	var service_name = $("#edit_service_code").children("option:selected").text();
	var service_description  = $("#edit_service_description").val();
	var amount_limit         = $("#edit_amount_limit").val();
    var session_limit        = $("#edit_session_limit").val();
    //var service_details      = $("#service_details").val();
    var amount_billed        = $("#amount_billed_edit").val();
    var service_charges      = $("#edit_service_charges").val();
    var consultation_charges = $("#edit_consultation_charges").val();
	$("#edit_service_error").text("");
    $("#service_details_error").text("");
    $("#edit_total_amount_error").text("");
    $("#edit_service_charges_error").text("");
    $("#edit_consultation_charges_error").text("");

	var balance_sum_insured = $("#balance_sum_insured").val();
	
	if(service == "" || service == null){
		$("#edit_service_error").text('Select Service');
		return false;
	}
    /*if(service_details == "" || service_details == null){
        $("#service_details_error").text('Enter Service Details');
        return false;
    }*/
    if(consultation_charges == "" || consultation_charges == null){
        $("#edit_consultation_charges_error").text('Enter Consultation Amount');
        return false;
    }
    
    if(amount_billed == "" || amount_billed == null){
        $("#edit_total_amount_error").text('Enter Total Amount');
        return false;
    }
    	if(parseFloat(amount_billed) > parseFloat(balance_sum_insured) ){
		swal({
            title: "Available Claim Amount $"+balance_sum_insured+" Only",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        }); 

        return false;
	}

	 // console.log(claim_service_fee_items);
	var service_fee = { }; 
	service_fee["inclusion_id"] 	    = $("#edit_service_inclusion_id").val();
    service_fee["service_id"]           = service;
    service_fee["service_code"]         = service_name;
    service_fee["service_description"]  = service_description;
    service_fee["amount_limit"]         = amount_limit;
	service_fee["session_limit"]        = session_limit;
	service_fee["service_details"]      = "";
    service_fee["consultation_charges"] = $("#edit_consultation_charges").val();
    service_fee["bed_charges"]          = $("#edit_bed_charges").val();
    service_fee["service_charges"]      = $("#edit_service_charges").val();
    service_fee["drug_charges"]         = $("#edit_drug_charges").val();
    service_fee["other_charges"]        = $("#edit_other_charges").val();
    service_fee["amount_billed"]        = amount_billed;
	claim_service_fee_items.push(service_fee);
	var services_count =parseInt($("#services_count").val())
	$("#service_fee_items").val(JSON.stringify(claim_service_fee_items));

	var markup = "<tr class='serviceTable'><td>" + service_name + "</td><td align='right'>" + (($("#edit_consultation_charges").val()) ? parseFloat($("#edit_consultation_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + (($("#edit_service_charges").val()) ? parseFloat($("#edit_service_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + (($("#edit_bed_charges").val()) ? parseFloat($("#edit_bed_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + (($("#edit_drug_charges").val()) ? parseFloat($("#edit_drug_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" +(($("#edit_other_charges").val()) ? parseFloat($("#edit_other_charges").val()).toFixed(2) : "0.00") + "</td><td align='right'>" + amount_billed + "</td><td class=\"text-center\"><a id=\"delete-service-feee\"><i class=\"fa fa-trash user-delete\"" + "aria-hidden=\"true\"></i></a></td> <td style='display:none'>" + service + "</td></tr>";
    $("table#services_table").append(markup);
    $("#service-modal-add").modal('hide');
    var total_amount = parseInt($("#total_amount").val())+parseInt(amount_billed);
    $("#total_amount").val(total_amount);
    $("#day_total_amount").val(total_amount);
    $("#sl_no").val(sl);
    $("#services_count").val(services_count+parseInt(1));
    $("#edit_service_code").empty().trigger('change');
    $("#edit_service_description").val("");
    $("#edit_amount_limit").val("");
    $("#edit_session_limit").val("");
    $("#edit_service_charges").val("");
    $("#edit_bed_charges").val("");
    $("#edit_drug_charges").val("");
    $("#edit_other_charges").val("");
    $("#service_type2").val("");
    $("#service_details").val("");
    $("#edit_consultation_charges").val("");
    //$("#amount_div").css("display", "block");
    $("#submit_div").css("display", "block");
    document.getElementById("services_div").style.display = "none";
    document.getElementById("services_action_div").style.display = "none";
    document.getElementById("limit_div").style.display = "none";
});