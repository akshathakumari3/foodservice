$('#litigation_buyer_seller_update').click(function () {
	var winner =  $("input[name='winner']:checked").val();
	if(winner){
    	var transaction_id  = $("#transaction_id").val();
    	var url             = $('#baseurl').val()+"/admin/transactions/litigation";
        $.ajax({
            type: 'post', 
            url : url, 
            data : {"transaction_id":transaction_id, "owner":winner},
            success : function (data) {
                if(data == "pass"){
                	$("#litigation-customers").hide();
                    $("#transaction-checklist-modal-div").show();
                }else{
                	$("#warning-message-modal .modalMessage").html("Error");
                    $("#warning-message-modal").modal("show");
                }
            }
        });

	}else{
		
		var text ='Please Select Winner';
        $("#warning-message-modal .modalMessage").html(text);
        $("#warning-message-modal").modal("show");
	}
});