$(document).ready(function () {  
    
    var select =  "Select";
    var url    = $("#baseurl").val()+'/admin/api-get-customers';
    var modal  = $('#nominee-modal').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": url, 
    "columns": [
            { data: "id" },
            {data: "first_name",render: function ( data, type, full ) {
                var first_name  = (full["first_name"]) ? full["first_name"]: "";
                var second_name = (full["second_name"]) ? full["second_name"]: "";
                var third_name  = (full["third_name"]) ? full["third_name"]: "";
                var fourth_name = (full["fourth_name"]) ? full["fourth_name"]: "";
                var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

                return name }},       
            { data: "mobile_1" },
            { data: "gender" },  
            { data: "date_of_birth" },
            { data: "relationship"},          
            { data : undefined, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>", searchabe: false }                        
            ],            
        colReorder: true
    });

    $('#nominee-modal').on( 'click', 'button', function () {
     
        var data = modal.row( $(this).parents('tr') ).data();
        $("#nominee_id,#selected_nominee_id").val(data.id); 
        $('#nominee-modal-div').modal('hide');
    });    
});