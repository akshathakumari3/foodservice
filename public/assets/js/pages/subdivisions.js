division();
function division(){
    $("#sub_divisions").hide();
    $("#seller_sub_divisions").hide();
}
$("#district").on('change',function(){

    var districtName = $("#district").val();
    var field        = 'propertyDistrict';  
    var type         = 1;
    getSubDivisions(districtName,field,type);
});
$("#seller_district").on('change',function(){
    var districtName = $("#seller_district").val();
    var field        = 'sellerDistrict';
    var type         = 2;
    getSubDivisions(districtName,field,type);
});
function getSubDivisions(districtName,field,type){
    var baseurl = $("#baseurl").val(); 
    var url = baseurl+"/admin/subDivisions/filter/properties";
     $.ajax({
        type: "get",
        url: url,
        data : {"districtName" : districtName,"type":type},
        success : function (data) {
            var values=["<option value=''> Please Select </option>"];
            $.each(data, function (index, value) {
                if(field=='propertyDistrict'){
                    var value = "<option value='"+value.name+"'>"+value.name +"</option>";
                    values.push(value);
                }else{
                    var value = "<option value='"+value.id+"'>"+value.name +"</option>";
                    values.push(value);
                }
            });
            if(field=='propertyDistrict'){
                $("#sub_divisions").show();
                $('#sub_division').html(values);
            }else{
                $("#seller_sub_divisions").show();
                $('#seller_sub_division').html(values);
            } 
                
        }
    });

}
