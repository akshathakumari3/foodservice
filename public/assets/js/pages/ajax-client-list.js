$(document).ready(function () {     
    var select  =  $("#select").data('select');
    var baseurl = $("#baseurl").val();
    var url     =baseurl+'/admin/api-get-client-providers';
    var modal   = $('#client-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": url, 
    "columns": [
			 {data: "client_name",render: function ( data, type, full ) {
                var client_names        = full["client_name"] ;
                if(client_names){
                    return  "<a href="+baseurl+'/app/client/'+full["id"]+">"+client_names+"</a>"; 
                }else{
                   return '-'; 
                }
            }},
			{ data: "type", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "started_year", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "primary_conatct_person", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "primary_contact_no", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
			{ data: "email", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}
            ],            
        colReorder: true
    });
});