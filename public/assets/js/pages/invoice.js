$("li.bg-primary").click(function(){
    $("#invoice, #footer-bg").removeAttr('class');
   $("#invoice, #footer-bg").addClass("bg-primary");
});
$("li.bg-success").click(function(){
    $("#invoice, #footer-bg").removeAttr('class');
    $("#invoice, #footer-bg").addClass("bg-success");
});
$("li.bg-warning").click(function(){
    $("#invoice, #footer-bg").removeAttr('class');
    $("#invoice, #footer-bg").addClass("bg-warning");
});
$("li.bg-danger").click(function(){
    $("#invoice, #footer-bg").removeAttr('class');
    $("#invoice, #footer-bg").addClass("bg-danger");
});
$("li.bg-info").click(function(){
    $("#invoice, #footer-bg").removeAttr('class');
    $("#invoice, #footer-bg").addClass("bg-info");
});

$("li.bg-default").click(function(){
    $("#invoice, #footer-bg").removeAttr('class');
    $("#invoice, #footer-bg").addClass("bg-default");
});
var modal='';
$(document).ready(function () { 
    
    var url    = $("#baseurl").val()+'/app/invoice/list-datatable';
    modal  = $('#invoice-table').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": false,
        "order": [[ 0, "desc" ]],
        //"ajax": url,
        "ajax": {
           'url': url,
           'data': function(data){
              // Read values
              var invoiceStatus = $('#invoice_status').val();

              // // Append to data
              data.invoice_status = invoiceStatus;
           }
        }, 
        "columns": [
            // {data : "id",
            //     "visible": false,
            //     "searchable": false
            // },
            { data: "invoice_refno" ,render: function ( data, type, full ) {
                return  "<a href="+$("#baseurl").val()+'/app/invoice/'+full["id"]+">"+data+"</a>"; 
            }
            },
            { data: "invoice_date" },
            { data: "invoice.policy_id",render: function ( data, type, full ) {
				if(full.policy_id!=undefined && full.policy_id!=""){
					var str1 = full.policy_id;
					var str2 = "GP";
					
					if(str1.indexOf(str2) != -1){
						var text=full.policy_id;
						var policy_id=text.replace('GP', "");
						var url='/app/clientPolicies/getClientPolicyDetails/'+policy_id;
					}else{
						var policy_id=full.policy_id;
						var url='/app/policy/'+policy_id;
					}
                return  "<a href="+$("#baseurl").val()+ url +">"+data+"</a>"; 
				}else{
					return "-";
				}
            }
            },
            { data: "customer",render: function ( data, type, full ) {
				if(full.client_id){
					return  "<a href="+$("#baseurl").val()+'/app/client/'+full.client_id+" class='blueRow'>"+data+"</a>";
				}
				if(full.customer_id){
					return  "<a href="+$("#baseurl").val()+'/app/customers/'+full.customer_id+" class='blueRow'>"+data+"</a>";
					
				}
				 
            }
            },
            // { data: "total_amount"}, 
            {
                data: 'total_amount',
                render: function ( data, type, row ) {
                    if(data) {
                        return parseFloat(data).toFixed(2);
                    }
                    else {
                        return 0;
                    }
                }
            },
            {
                data: 'dueDate',
                render: function ( data, type, row ) {
                    if(data) {
                        return data;
                    }
                    else {
                        return '-';
                    }
                }
            },
            { data: "status", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
        ], 
        "columnDefs": [{ targets: [4], className: 'text-right' }],    
        // 'columnDefs'        : [         
        //         { 
        //             'searchable'    : false, 
        //             'targets'       : [0,1,3,4] 
        //         },
        //     ],      
        colReorder: true,
    });
 $('#invoice_status').change(function(){
	       var url    = $("#baseurl").val()+'/app/invoice/list-datatable';
	   if ( ! $.fn.DataTable.isDataTable( '#transaction-documents-table' ) ) {
			modal.destroy();
		}
		
       modal=$('#invoice-table').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": false,
        "order": [[ 0, "desc" ]],
        //"ajax": url,
        "ajax": {
           'url': url,
           'data': function(data){
              // Read values
              var invoiceStatus = $('#invoice_status').val();

              // // Append to data
              data.invoice_status = invoiceStatus;
           }
        }, 
        "columns": [
            // {data : "id",
            //     "visible": false,
            //     "searchable": false
            // },
            { data: "invoice_refno" ,render: function ( data, type, full ) {
                return  "<a href="+$("#baseurl").val()+'/app/invoice/'+full["id"]+">"+data+"</a>"; 
            }
            },
            { data: "invoice_date" },
            { data: "policy.reference_no",render: function ( data, type, full ) {
                return  "<a href="+$("#baseurl").val()+'/app/policy/'+full.policy_id+">"+data+"</a>"; 
            }
            },
            { data: "customer",render: function ( data, type, full ) {
                return  "<a href="+$("#baseurl").val()+'/app/customers/'+full.customer_id+">"+data+"</a>"; 
            }
            },
            // { data: "total_amount"}, 
            {
                data: 'total_amount',
                render: function ( data, type, row ) {
                    if(data) {
                        return parseFloat(data).toFixed(2);
                    }
                    else {
                        return 0;
                    }
                }
            },
            {
                data: 'dueDate',
                render: function ( data, type, row ) {
                    if(data) {
                        return data;
                    }
                    else {
                        return '';
                    }
                }
            },
            { data: "status" },
        ], 
        "columnDefs": [{ targets: [4], className: 'text-right' }],    
        // 'columnDefs'        : [         
        //         { 
        //             'searchable'    : false, 
        //             'targets'       : [0,1,3,4] 
        //         },
        //     ],      
        colReorder: true,
    });
    });
 
});
  