$(document).ready(function () { 
	var id   = $("#group_id").val();
	var check_status=$("#check_status").val();
	var url  = $("#baseurl").val()+'/app/clientPolicies/getPolicyListJson/'+id;

	var modal  = $('#client-policy-table').DataTable({
	    "responsive": true,
	    "processing": true,
	    "serverSide": true,
	    "order": [[ 0, "desc" ]],
	    "columnDefs": [
		    { "orderable": false,"targets": [ 1, 2,3, 4 ]},
		  ],
	    "ajax": url, 
	    "columns": [
	    	{data : "id",
                "visible": false,
                "searchable": false
            },
            {data : "customer.employer_id",name: "customer.employer_id" },
            {data : "reference_no",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/policy/'+full["id"]+" class='blueRow'>"+data+"</a>"; 
            }},
            { data: "lineOfBusiness",name: "lineOfBusiness.name" }, 
            { data: "product",name: "product.name" },
            // { data: "productVariant",name: "productVariant.variant" }, 
            {data : "customer.name",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/customers/'+full.customer.id+" class='blueRow'>"+data+"</a>"; 
            }},
            // { data: "sum_insured" },
            // { data: "sum_insured"},
            { data: "tenure_num" },
            // { data: "premium_amount" },

            { data: "premium_amount",render: function ( data, type, full ) {
                return parseFloat(data).toFixed(2); 
            }},
            {data : "action",render: function ( data, type, full ){
				if(check_status==1){
					 return  "<a class='btn btn-sm btn-danger' data-toggle='modal' data-href='#reject_modal_div' href='#reject_modal_div' id='inactiveClient' data-id='"+full["id"]+"' disabled><span class='glyphicon glyphicon-remove'></span> Inactive</a>";
				}else{
                return  "<a class='btn btn-sm btn-danger' data-toggle='modal' data-href='#reject_modal_div' href='#reject_modal_div' id='inactiveClient' data-id='"+full["id"]+"'><span class='glyphicon glyphicon-remove' ></span> Inactive</a>"; 
				}

                // return  "<a class='btn btn-sm btn-success' data-toggle='modal' data-href='#client_member_list_div' href='#client_member_list_div'><span class='glyphicon glyphicon-ok'></span> Active</a>"; 
            }},
        ],
        "columnDefs": [{ targets: [7], className: 'text-right' }, { targets: [8], className: 'text-center' }],                
        colReorder: true,
	});

    var modal  = $('#policy-group-table').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            { "orderable": false,"targets": [ 2, 3,4, 5 ]},
          ],
        "ajax": $("#baseurl").val()+'/app/clientPolicies/getPolicyGroupListJson', 
        "columns": [
            {data : "id",
                "visible": false,
                "searchable": false
            },
            {data : "reference_no", render: function ( data, type, full ) { 
                if(data != null) {
                    console.log(data);
                    return data;
                } else {
                    return '-';
                }
            }},
            {data : "client.client_name", render: function ( data, type, full ) { 
                if(data != null) {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data : "policy_group_name",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/clientPolicies/getClientPolicyDetails/'+full["id"]+" class='blueRow'>"+data+"</a>"; 
            }},
            // { data: "reference_no" },
            { data: "member_count", render: function ( data, type, full ) { 
                if(data != null) {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "policy.lineOfBusiness", render: function ( data, type, full ) { 
                if(data != null) {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "policy.product", render: function ( data, type, full ) { 
                if(data != null) {
                    return data;
                } else {
                    return '-';
                }
            }},
            // { data: "policy.productVariant" }, 
            // { data: "sum_insured" }, 
            { data: "tenure_num", render: function ( data, type, full ) { 
                if(data != null) {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "total_premium", render: function ( data, type, full ) { 
                if(data != ' ') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data : "action",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/clientPolicies/'+full["id"]+" class='blueRow'><i class='fa fa-eye'></i></a>"; 
            }},
			 {data : "action",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/reports/ClientGroupPolicyReport?id='+full["id"]+" class='blueRow'><i class='fa fa-eye'></i></a>"; 
            }},
			 {data : "status",render: function ( data, type, full ){
				 if(full.total_premium>0){
					 return 'Active';
				 }else{
					 return 'In Active';
				 }
			 }},
        ],
        "columnDefs": [{ targets: [8], className: 'text-right' },{ targets: [9], className: 'text-center', orderable:false }],                
        colReorder: true,
    });
	$('body').on('click', '#inactiveClient', function (e) {
	//$('#inactiveClient').click(function(e){
		e.preventDefault();
		var id=$(this).attr('data-id');
		$('#client_policy_id').val(id);		
	});
	$('#inactiveClientMember').click(function(){
		var client_policy_id=$('#client_policy_id').val();
		var comments=$('#comments').val();
		var group_id=$('#group_id').val();
		$("#comments_inactive_reason").text("");
		 if(comments == "" || comments==null){
            $("#comments_inactive_reason").text('Please fill COmments');
            return false;
        }
		//var $this = $(this);
		//$this.button('loading');
		var post_url  = $("#baseurl").val()+'/app/clientPolicies/inactiveClientMember/';
		 $.ajax({
            type: "GET",
            url: post_url,
            data: {group_id:group_id,"client_policy_id":client_policy_id,comments:comments},
            success: function(res){
			 window.location = $("#baseurl").val()+"/app/clientPolicies/getClientPolicyDetails/"+res;				

			}
			});

	});
});