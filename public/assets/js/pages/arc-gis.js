$("#sub_district").on('change',function(){

    var sub_district  = $("#sub_district").val();
    var please_select = $("#please_select").val();
    var base_url      = $("#baseurl").val();
    
    $.ajax({
        type: "post",
        url: base_url+'/admin/arcGis/streetName',
        data : {"sub_district" : sub_district,"grid":1},
        success : function (data) {
            var street_names=["<option value=''> "+please_select+" </option>"];
            var grids=["<option value=''> "+please_select+" </option>"];
            $.each(data.street_names, function (index, value) {

                var value = '<option value="'+value.attributes.street_name+'">'+value.attributes.street_name +'</option>';
                street_names.push(value);
            });

            $.each(data.grids, function (index, value) {

                var value = '<option value="'+value.attributes.grid+'">'+value.attributes.grid +'</option>';
                grids.push(value);
            });
                
            $('#street_name').html(street_names);
            $('#grid').html(grids);
            $("#street_names").show();
            $("#grids").show();

        }
    });
});

$("#district").on('change',function(){

    var district      = $("#district").val();
    var please_select = $("#please_select").val();
    var base_url      = $("#baseurl").val();
    
    $.ajax({
        type: "get",
        url: base_url+'/admin/arcGis/getSubDistricts',
        data : {"district" : district},
        success : function (data) {
            var sub_districts=["<option value=''> "+please_select+" </option>"];
            $.each(data.sub_districts, function (index, value) {

                var value = '<option value="'+value.attributes.sub_districts+'">'+value.attributes.sub_districts +'</option>';
                sub_districts.push(value);
            });
                
            $('#sub_district').html(sub_districts);
            $("#sub_districts").show();
        }
    });
});