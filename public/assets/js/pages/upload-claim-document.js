$("#upload-claim-document").click(function(){
    // console.log('dddd');
    var document_type_id = $("#document_type_id").val();
    var document_name    = $("#document_name").val();
    var file             = $("#document").val();

    if(!document_type_id){
        $("#document_type_error").html("Please Select Document Type");
        return false;
    } else {
        $("#document_type_error").html("");
    }

    /*if(!document_name){
        $("#document_name_error").html("Please Enter Document Name");
        return false;
    } else {
        $("#document_name_error").html("");
    }*/

    if(!file){
        $("#document_error").html("Please Upload Document");
        return false;
    } else {
        $("#document_error").html("");
    }

    $("#gis-loader").css("display", "block");
    var $this = $(this);
    $this.button('loading');

    var url = $('#baseurl').val()+"/app/claim/add-document";

    $.ajax({
        headers : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        url:url,
        data:new FormData($("#upload-document-form")[0]),
        dataType:'json',
        // async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(data){
            
            if(data.count){
                $("#claim_documents_count").val(data.count);
                // $("#claim_documents_data").html(data.view);
                $(".claim_documents_data").html(data.view);
            }else{
                $("#wizard-error").html(data).show();
            }

            $this.button('reset');
            /* empty field*/
            $("#document_type_id").val('');
            $("#document_name").val('');
            $("#document").val('');
            $("#claim_document_id").val(data.claim_application_id);
            $("#claim-document-div").modal('hide');
            $("#document-modal-add").modal('hide');
            $("#gis-loader").css("display", "none");
        },
        error: function(data1)
        {   $this.button('reset');
            $("#wizard-error").html(data1).show();
        }
    });
});

function deleteAddedDocument(id){
    $.ajax({
        type:'get',
        url: $('#baseurl').val()+"/app/claim/delete-added-document",
        data:{document_id:id,claim_id:$("#claim_document_id").val()},
        success:function(data){
            $("#claim_documents_count").val(data.count);
            $(".claim_documents_data").html(data);
        }
    });
}