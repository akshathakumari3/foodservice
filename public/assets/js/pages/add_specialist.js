$(document).ready(function() {

    $('#specialist-form').submit(function() {
        $('input[type=submit]').removeAttr("disabled");
        $("body").removeClass("loading");
        $("#specialist-form").valid();
    });

     $("#specialist-form").validate({
        rules: {
            // name: {
            //     required: true,
            // },
            // contact_number: {
            //     required: true,
            // },
            education: {
                required: true,
            },
            // license_number: {
            //     required: true,
            // },
            // services: {
            //     required: true,
            // },
            // specialisation: {
            //     required: true,
            // },
        },
        messages: {
            // name: {
            //     required: "Please Enter Name",
            // },
            // contact_number: {
            //     required: "Please Enter Contact Number",
            // },
            education: {
                required: "Please Select Education",
            },
            // license_number: {
            //     required: "Please Select License Number",
            // },
            // services: {
            //     required: "Please Select Services",
            // },
            // specialisation: {
            //     required: "Please Select specialisations",
            // },
            
        },
       /* errorPlacement: function(error, element) {
         if (element.is('select')) {
              error.appendTo( element.parents('.errorPlace') );
             }  else {
                 error.insertAfter(element);
             }
         },*/

        errorElement: 'label',
            highlight: function (element) { 
                return false;
            },
            unhighlight: function (element) {
                return false;
            }

    });

});