$(document).ready(function () {
    var base_url = $("#baseurl").val();
    $.fn.select2.defaults.set("theme","bootstrap");
    $('select[name="service_provider_2"]').select2({
        //dropdownParent: '',
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/claim/get-specialists',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
					providerId		  :$('#provider_id').val()
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {      
        var data = e.params.data;
        $("#service_provider_id").val(data.service_provider_id);
    });
	
	
	$('select[name="service_provider"]').select2({
        //dropdownParent: '',
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/claim/get-service-provider',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
                }

                return query;
            }
        }
    }).on('select2:select', function (e) { 
		
        var data = e.params.data;
		$('.primary_specialist').show();
		$('.primary_specialist_edit').hide();
        $("#provider_id").val(data.id);
    });
	
	
	$('select[name="service_hospital_provider"]').select2({
        //dropdownParent: '',
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/claim/get-service-provider',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {      
        var data = e.params.data;
        $("#provider_id").val(data.id);
		$('.primary_specialist').show();
		$('.primary_specialist_edit').hide();
    });
	

   $('select[name="service_hospital_sepcialist"]').select2({
        //dropdownParent: '',
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/claim/get-specialists',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
					providerId		  :$('#provider_id').val()
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {      
        var data = e.params.data;
        $("#service_provider_id").val(data.service_provider_id);
    });
});   