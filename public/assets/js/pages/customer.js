$(document).ready(function() {

	$("input:radio[name=gender]").click(function() {
		var gender  = $('input[name="gender"]:checked').val();

		if(gender){

	 		duplicateCustomerCheck();
	 	}
	});

})

function duplicateCustomerCheck() {

	var first_name    = $("#first_name").val();
	var second_name   = $("#second_name").val();
	var third_name    = $("#third_name").val();
	var fourth_name   = $("#fourth_name").val();
	var date_of_birth = $("#date_of_birth").val();
	var gender        = $('input[name="gender"]:checked').val();
	var mobile_1      = $("#mobile_1").val();
	var mobile_2      = $("#mobile_2").val();
	var baseurl       = $("#baseurl").val();


	if((first_name && second_name && third_name && fourth_name && date_of_birth && gender) || (mobile_2)){
		$('#save-customer').attr('disabled', 'true');
		$.ajax({
	            type: "POST", 
	            url : baseurl+"/app/customer/duplicate",
	            data : {"first_name":first_name,"second_name":second_name,"third_name":third_name,"fourth_name":fourth_name,
	                   "date_of_birth":date_of_birth,"gender":gender,"mobile_1":mobile_1,"mobile_2":mobile_2}, 
	            success : function (data) {
	                if(data == 'fail'){
						$('#save-customer').attr('disabled', 'true');
						$("#warning-message-modal .modalMessage").html("Customer Already Created");
						$("#warning-message-modal").modal("show");
		                
	                }else{
	                	$('#save-customer').removeAttr("disabled");

	                }
	            }
	    }); 
	}
}