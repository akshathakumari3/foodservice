"use strict";
$(document).ready(function () {
    
    //declare global variables required for wizard 
    var baseurl = $("#baseurl").val();
    // initializations
    
    $(".wrapper").addClass("hide_menu");
    
    // manage wizard 
    var form = $("#mortgage");    
    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            
            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            
            // dont change the order of the checks
            // No Property Selected
            if (newIndex === 1) {
                
                var property_id = $("#property_id").val();
                var customer_id = $("#customer_id").val();

                if(customer_id && property_id){

                    showPrimaryOwner();
                    $("#wizard-error").hide();
                }else{

                    $("#wizard-error").html("Property is Required").show();
                    return false;
                }
            }
            
            if (newIndex === 2 && $(!"#selected_customer").val()) {
                $("#wizard-error").html("Select Borrower").show();
                return false;
            }
            
            
            // Missing Bank Information
            if (newIndex === 3 && (!$("#bank").val() || !$("#loan_type").val() || !$("#settlement_amount") || !$("#mortgage_amount") || !$("#mortgage_start_date") || !$("#mortgage_end_date") )) {
                $("#wizard-error").html("Enter Lien Details").show();
                return false;
            }      
              
            
            if(newIndex === 2){
                $("#wizard-error").html("").hide();
            }
            if (newIndex === 3) {
                $("#wizard-error").hide();
                var wizardSection = "wizard-summary-view";
                var nextView      = "admin.lien.summary-partial";

                processNextStep(form, nextView, wizardSection);                             
            }
            
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            // form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex) {
            //form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            
            var formData = form.serialize();
            var url      = baseurl+"/lien/finish-wizard";
            $.post(url,  formData, 
                function (data) {
                    $("#gis-loader").css("display", "block");
                    $("#wizard-error").html("").hide();
                    window.location = baseurl+"/workflow/initiate/" + data;
                    return false;
                }).fail(function(){
                    console.log("error");
            });  
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        }
    });

    //datatable declaraion 
    var customerDataTable = $('#selected-customers-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Borrower are selected"
           }, 
           
    });

    $('#customer-modal-div').on('hidden.bs.modal', function () {

        var customer_id       = $("#customer_id").val(); 
        var selected_customer = $("#selected_customer").val();
        //console.log("Adding Customer " + customer_id);
       
        if (customer_id == selected_customer) {
            $('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p> Customer already selected </p>");  
        } else {
            showPrimaryOwner();
        }          
    });

    function showPrimaryOwner() {

        var customer_id       = $("#customer_id").val(); 

        if(customer_id){
                $.ajax({
                    type: 'GET', 
                    url : baseurl+"/admin/api-get-customer/"  + customer_id, 
                    success : function (data) {
                        // $("#litigation-wizard-error").html("").hide();
                        customerDataTable.row().remove().draw();

                        var first_name  = (data.first_name) ? data.first_name: "";
                        var second_name = (data.second_name) ? data.second_name: "";
                        var third_name  = (data.third_name) ? data.third_name: "";
                        var fourth_name = (data.fourth_name) ? data.fourth_name: "";
                        var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                        customerDataTable.row.add( [
                            data.id,
                            customer_name,
                            data.email,
                            data.mobile_1,
                            data.gender,
                            // "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                        ] ).draw( false );   
                        $('#customer_id').val(data.id); 
                        $('#selected_customer').val(data.id); 
                        //console.log("Selected Customer List " + $('#selected_plaintiff_id').val());
                    }
                });  
            }
    }
    
});


function processNextStep(form, nextView, wizardSection, dataModalDiv) {

    var baseurl  = $("#baseurl").val();
    var formData = form.serialize();
    formData    += "&next_view=" + nextView;
    var url      = baseurl+"/lien/process-wizard";
    $.post(url,  formData, 
        function (data) {
            $("#wizard-error").html("").hide();
            $("#"  + wizardSection).html(data);
            return false;
        }).fail(function(){
            console.log("error");
    });      
}