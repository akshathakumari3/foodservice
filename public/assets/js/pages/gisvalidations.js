$(document).ready(function() {

    $('#customer-form').submit(function() {

        $('input[type=submit]').removeAttr("disabled");
        $("body").removeClass("loading");
        $("#customer-form").valid();
    });

    $("#customer-form").validate({

        rules: {
            customer_type: {
                required: true,
            },
            first_name: {
                required: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
                alphanumeric: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            second_name: {
                 required: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
                alphanumeric: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            third_name:{
                alphanumeric: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            fourth_name:{
                alphanumeric: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            mobile_1:{
                required: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
                number: true,
            },
            mobile_2:{
                required: true,
                number: true,
            },
            // email:{
            //     required: true,
            //     email: true,
            // },
            gender:{
                required: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            date_of_birth:{
                required: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            
            place_of_birth:{
                required: function(element) {
                    var customer_type = $("#customer_type").val();
                    if (customer_type == 'Personal') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            district:{
                required: true,
            },
            // sub_division:{
            //     required: true,
            // },
            business_name:{
                required: true,
            },
            business_registration_number:{
                required: false,
            },
            business_address:{
                required: true,
            },
            location_reference:{
                required: true,
            },
             phone_carrier:{
                required: true,
            },
        },
        messages: {
            customer_type: {
                required: "Please Select Customer Type",
            },
            first_name: {
                required: "Please Enter  First Name",
            },
            second_name: {
                required: "Please Enter Second Name",
            },
            // third_name: {
            //     required: "Please Enter Third Name",
            // },
            // fourth_name: {
            //     required: "Please Enter Fourth Name",
            // },
            // email: {
            //     required: "Please Enter  Email ID",
            // },
            mobile_1: {
                required: "Please Enter Contact Number",
                number: "Enter Valid Numbers",
            },
            mobile_2: {
                required: "Please Enter Applicant Mobile Number",
                number: "Enter Valid Numbers",
            },
            gender: {
                required: "Please Select Gender",
            },

            date_of_birth: {
                required: "Please Select Date Of Birth",
            },
            place_of_birth: {
                required: "Please Enter Place Of Birth",
            },
            district: {
                required: "Please Select District",
            },
            // sub_division: {
            //     required: "Please Select Sub Division",
            // },
            business_name: {
                required: "Please Enter Business Name",
            },
            // business_registration_number: {
            //     required: "Please Enter Business Registration Number",
            // },
            business_address: {
                required: "Please Enter Business Address",
            },
            location_reference:{
                required: "Please Enter Address",
            },
            phone_carrier: {
                required: "Please Select Phone Carrier",
            },
        },

        errorPlacement: function(error, element)
            {
                if ( element.is(":radio") )
                {
                    error.appendTo( element.parents('.errorPlace') );
                }
                else
                { // This is the default behavior
                    error.insertAfter( element );
                }
            },

        errorElement: 'label',
            highlight: function (element) { 
                return false;
            },
            unhighlight: function (element) {
                return false;
            }

    });

    $('#property_register').submit(function() {

        $('input[type=submit]').removeAttr("disabled");
        $("body").removeClass("loading");
        $("#property_register").valid();
    });

    $("#property_register").validate({

        rules: {
            district: {
                required: true,
            },
            property_usage: {
                required: true,
            },
            building: {
                required: true,
            },
            building_area: {
                max: function() {
                    return parseInt($('#parcel_area_sqm').val());
                }
            },
            length: {
                required: true,
            },
            breadth: {
                required: true,
            },
            first_name: {
                required: true,
                alphanumeric: true
            },
            second_name: {
                required: true,
                alphanumeric: true
            },
            third_name: {
                alphanumeric: true
            },
            fourth_name: {
                alphanumeric: true
            },
            phone_carrier: {
                required: true,
            },
            seller_location_reference: {
                required: true,
            },
            seller_district: {
                required: true,
            },
            size: {
                required: true,
            },
            seller_sub_division: {
                required: function(element) {
                    var seller_district = $("#seller_district").val();
                    if (seller_district == '')  {
                       return false;
                    } else {
                        return true;
                    }
                  },
            },
            business_type:{
                required: function(element) {
                    var property_usage = $("#property_usage").val();
                    if (property_usage == 'Business') {
                       return true;
                    } else {
                        return false;
                    }
                },
            },
            seller_mobile_1: {
                required: true,
            },
            grade: {
                required: true,
            },
            parcel_area_sqm: {
                required: true,
            },
            down_town: {
                required: true,
            },
            number_of_stories: {
                required: true,
            },
        },
        messages: {
            district: {
                required: "Please Select District",
            },
            sub_division: {
                required: "Please Select Sub District",
            },
            property_usage: {
                required: "Please Select Property Usage",
            },
            Building: {
                required: "Please Select Building",
            },
            length: {
                required: "Please Enter Property Length",
            },
            breadth: {
                required: "Please Enter Property Breadth",
            },
            first_name: {
                required: "Please Enter First Name",
            },
            second_name: {
                required: "Please Enter Second Name",
            },
            phone_carrier: {
                required: "Please Select Phone Carrier",
            },
            seller_location_reference: {
                required:  "Please Enter Address",
            },
            seller_district: {
                required: "Please Select District",
            },
            seller_sub_division: {
                required: "Please Select Sub District",
            },
            seller_mobile_1: {
                required: "Please Enter Contact Number",
            },
            grade: {
                required: "Please Select Grade",
            },
            parcel_area_sqm: {
                required: "Please Enter Parcel Size",
            },
            down_town: {
                required: "Please Select Down Town",
            },
            number_of_stories: {
                required: "Please Select Number Of Stories",
            },
            business_type: {
                required: "Please Select Business Type",
            },
            size: {
                required: "Please Enter Size",
            },
            
        },

        errorElement: 'label',
            highlight: function (element) { 
                return false;
            },
            unhighlight: function (element) {
                return false;
            }

    });
    
    $('#create_property').submit(function() {
        $('input[type=submit]').removeAttr("disabled");
        $("body").removeClass("loading");
        $("#create_property").valid();
    });

    $("#create_property").validate({

        rules: {
            district: {
                required: true,
            },
            sub_division: {
                required: true,
            },
            address: {
                required: true,
            },
            building: {
                required: true,
            },
            property_usage: {
                required: true,
            },
            size: {
                required: true,
            },
            building_area: {
                required: true,
                max: function() {
                    return parseInt($('#size').val());
                }
            },
            property_wall: {
                required: true,
            },
            no_of_buildings: {
                required: true,
            },
        },
        messages: {
            district: {
                required: "Please Select District",
            },
            sub_division: {
                required: "Please Select Sub District",
            },
            address: {
                required: "Please Enter Address",
            },
            building: {
                required: "Please Select Building",
            },
            property_usage: {
                required: "Please Select Property Usage",
            },
            size: {
                required: "Please Enter Size",
            },
            building_area: {
                required: "Please Enter Building Area",
            },
            property_wall: {
                required: "Please Select Property Wall",
            },
            no_of_buildings: {
                required: "Please Enter No.of Buildings Usage",
            },
            
        },

        errorElement: 'label',
            highlight: function (element) { 
                return false;
            },
            unhighlight: function (element) {
                return false;
            }

    });
});


function isNumberKey(evt) {
   var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 95 && charCode != 45 && charCode > 31
    && (charCode < 48 || charCode > 57))
     return false;

 return true;
}


$('#checklist-form').submit(function() {
    $('input[type=submit]').removeAttr("disabled");
    $("body").removeClass("loading");
    $("#checklist-form").valid();
});

$("#checklist-form").validate({

        rules: {
            role_id: {
                required: true,
            },
            name: {
                required: true,
            },
            description: {
                required: true,
            },
            type: {
                required: true,
            },
        },
        messages: {
            role_id: {
                required: "Please Select Role ID",
            },
            name: {
                required: "Please Enter Name",
            },
            description: {
                required: "Please Enter Description",
            },
            type: {
                required: "Please Select Type",
            },
        },

        errorElement: 'label',
            highlight: function (element) { 
                return false;
            },
            unhighlight: function (element) {
                return false;
            }

    });