$(document).ready(function() {                   
    //$("#payment_date").datetimepicker().parent().css("position :relative"); 
    var today = new Date();
    $('#payment_date').datetimepicker({
        maxDate: today
    }).parent().css("position :relative");
                     
    $('#payment-modal-div').on('show.bs.modal', function(e) {
        var transactionId = $(e.relatedTarget).data('transaction-id');
        $("#payment-modal-div #transaction_id").val(transactionId);
        console.log("Calling Show.Bs.modal");
    });         

    $("#payment-form").submit(function(e) {
        var url = "transaction/submit-payment"; // the script where you handle the form input.
        var formData = $("#payment-form").serialize();
        $("#payment-modal-div").modal("hide");                
        $.post(url,  formData, 
            function (data) {
                $("#wizard-error").html("").hide();                
                $("#wizard-invoice-view").html(data);
                //$("#info-message-modal .modalMessage").html("Payment Successful");
                //$("#info-message-modal").modal("show");
                return false;
            }).fail(function(data){
                $("#warning-message-modal .modalMessage").html("Failed to make payment");
                $("#warning-message-modal").show();                
            });                     
        //e.preventDefault(); // avoid to execute the actual submit of the form.
        return false;
    });         
});  