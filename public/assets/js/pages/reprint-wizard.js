"use strict";
$(document).ready(function () {
    
    //declare global variables required for wizard 
    var baseurl           = $('#baseurl').val();
    // initializations
    
    $(".wrapper").addClass("hide_menu");
    
    // manage wizard 
    var form = $("#reprint");    
    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            
            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            
            // dont change the order of the checks
            // No Transaction Selected
            var transaction_id = $("#transaction_id").val();

            if (newIndex === 1) {

                var customer_id    = $("#customer_id").val(); 

                if(transaction_id && customer_id){

                    showPrimaryOwner();
                    $("#wizard-error").hide();
                }else{

                    $("#wizard-error").html("Select Transaction").show();
                    return false;
                }
            }
            
            // No Customer Selected
            if (newIndex === 2) {

                var selected_customer =  $("#selected_customer").val();

                if(selected_customer){

                   $("#wizard-error").html("").hide();
                   var wizardSection = "wizard-id-proof-details-view";
                   var nextView      = "admin.reprintCertificate.proof-details";

                   processNextStep(form, nextView, wizardSection); 
                   //getReprintDocuments(transaction_id);

                }else{

                    $("#wizard-error").html("Select Receiver").show();
                    return false;
                }
            }
            
            // Missing ID Information

            if (newIndex === 3 && !$("#reprint_documents_count").val()) {
                $("#wizard-error").html("Upload Proof Details").show();
                return false;
            }      
              
            
            // if(newIndex === 2){
            //     $("#wizard-error").html("").hide();
            // }

            if (newIndex === 3) {
                $("#wizard-error").hide();
                var wizardSection = "wizard-summary-view";
                var nextView      = "admin.reprintCertificate.summary-partial";
                processNextStep(form, nextView, wizardSection);                             
            }
                      
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            //form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex) {
            // form.validate().settings.ignore = ":disabled";
             return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var formData = form.serialize();
            var url      = baseurl+"/reprint/finish-wizard";
            $.post(url,  formData, 
                function (data) {
                    $("#gis-loader").css("display", "block");
                    $("#wizard-error").html("").hide();
                    window.location = baseurl+"/workflow/initiate/" + data;
                    return false;
                }).fail(function(){
                    console.log("error");
            });  
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        }
    });  

    //datatable declaraion 
    var customerDataTable = $('#selected-customers-table').DataTable({
        "bFilter"   : false,
        "bPaginate" : false,
        "language"  : {
             "emptyTable" : "No Customer are selected"
           }, 
           
    }); 

    $('#customer-modal-div').on('hidden.bs.modal', function () {

        var customer_id       = $("#customer_id").val(); 
        var selected_customer = $("#selected_customer").val();
        //console.log("Adding Customer " + customer_id);
       
        if (customer_id == selected_customer) {
            $('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p> Customer already selected </p>");  
        } else {

            showPrimaryOwner();

        }          
    });

    function showPrimaryOwner() {

        var customer_id = $("#customer_id").val(); 

        if(customer_id){
            $.ajax({
                type: 'GET', 
                url : baseurl+"/admin/api-get-customer/"  + customer_id, 
                success : function (data) {
                    $("#gis-loader").css("display", "none");
                    // $("#litigation-wizard-error").html("").hide();
                    customerDataTable.row().remove().draw();
                   // $('#selected_customers').val("");

                    var first_name  = (data.first_name) ? data.first_name: "";
                    var second_name = (data.second_name) ? data.second_name: "";
                    var third_name  = (data.third_name) ? data.third_name: "";
                    var fourth_name = (data.fourth_name) ? data.fourth_name: "";
                    var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                    customerDataTable.row.add( [
                        data.id,
                        customer_name,
                        data.email,
                        data.mobile_1,
                        data.gender,
                        // "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                    ] ).draw( false );   
                    $('#customer_id').val(data.id); 
                    $('#selected_customer').val(data.id); 
                    //console.log("Selected Customer List " + $('#selected_plaintiff_id').val());
                }
            });  
        }


    }
    
});


function processNextStep(form, nextView, wizardSection, dataModalDiv) {

    var baseurl  = $("#baseurl").val();
    var formData = form.serialize();
    formData    += "&next_view=" + nextView;
    var url      = baseurl+"/reprint/process-wizard";
    $.post(url,  formData, 
        function (data) {

            if(data.transaction_id){
                $("#wizard-error").html("").hide();
                $("#"  + wizardSection).html(data.view);
                $("#reprint_transaction_id").val(data.transaction_id);
                $("#new_transaction_id").val(data.transaction_id);  
                return false;
            }else{
                $("#wizard-error").html("Wizard Process error.").show();
                return false;
            }
        }).fail(function(){
            console.log("error");
    });      
}

function getReprintDocuments(transaction_id) {

    var url = $('#baseurl').val()+"/reprint/get-documents/"+transaction_id;

    $.ajax({
        type : 'get', 
        url  : url, 
        success : function (data) {
            if(data.count){
                $("#reprint_documents_count").val(data.count);
                $("#reprint-documents-data").html(data.view);
            }else{
                $("#wizard-error").html(data).show();
            }
            
            $("#reprint-loader").css("display", "none");
        }
    });
}