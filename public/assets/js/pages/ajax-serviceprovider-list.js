$(document).ready(function () {     
    var select  =  $("#select").data('select');
    var baseurl = $("#baseurl").val();
    var url     =baseurl+'/admin/api-get-service-providers';
    var modal   = $('#service_provider_table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": url, 
    "columns": [
          {data: "name",render: function ( data, type, full ) {
                var service_name        = full["name"] ;
                if(service_name){
                    return  "<a href="+baseurl+'/app/serviceProvider/'+full["id"]+">"+service_name+"</a>"; 
                }else{
                   return '-'; 
                }
            }},   
			{ data: "category", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "contact_person_name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "contact_number_1", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}
            ],            
        colReorder: true
    });
});