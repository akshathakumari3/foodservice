"use strict";
$(document).ready(function () {
    
    //declare global variables required for wizard 
    var selected_plaintiff_id = '';
    var selected_plaintiff_id = '';
    var baseurl               = $("#baseurl").val();

    // initializations
    
    $(".wrapper").addClass("hide_menu");
    
    // manage wizard 
    var form = $("#litigation-registration");    
    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            
            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            
            // dont change the order of the checks
            // No Plaintiff and Defendant Selected
            if (newIndex === 1 && (!$("#plaintiff_id").val() || !$("#defendant_id").val())) {
                $("#litigation-wizard-error").html("Select  Plaintiff and Defendant Details").show();
                return false;
            }
            
            if (newIndex === 1 && ($("#plaintiff_id").val() == $("#defendant_id").val())) {
                $("#litigation-wizard-error").html("Plantiff and Defendant Should not be Same").show();
                return false;
            }

            // No Property Selected
            if (newIndex === 2 && !$("#property_id").val()) {
                $("#litigation-wizard-error").html("Select Property").show();
                return false;
            }
            
            // Missing Litigation Information
            if (newIndex === 3 && (!$("#litigation_type").val() ||!$("#settlement_amount").val())) {
                $("#litigation-wizard-error").html("Enter Litigation Details").show();
                return false;
            }      
            
            if (newIndex === 3 ) {
                var wizardSection = "litigation-wizard-summary-view";
                var nextView      = "admin.litigation.summary-partial";
                processNextStep(form, nextView, wizardSection);  
            }    
            
          
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            //form.validate().settings.ignore = ":disabled,:hidden";
            // return form.valid();
            return true;
        },
        onFinishing: function (event, currentIndex) {
            //form.validate().settings.ignore = ":disabled";
            // return form.valid();
            return true;
        },
        onFinished: function (event, currentIndex) {
            
            var formData = form.serialize();
            var url      = baseurl+"/app/finish-litigation-wizard";

            // console.log(url);
            $.post(url,  formData, 
                function (data) {

                    // console.log(data+'ff');
                    $("#gis-loader").css("display", "block");
                    $("#litigation-wizard-error").html("").hide();
                    window.location = baseurl+"/workflow/initiate/" + data;
                    return false;
                }).fail(function(){
                    console.log("error");
            });  
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        }
    });   
    
    //datatable declaraion 
    var plaintiffDataTable = $('#selected-plaintiff-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Plaintiff are selected"
           }, 
           
    });
    var defendantDataTable = $('#selected-defendant-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Defendant are selected"
           }, 
           
    });  

    $('#plaintiff-modal-div').on('hidden.bs.modal', function () {
        var plaintiff_id = $("#plaintiff_id").val(); // plaintiff id is a hidden field in the plaintif .modal-serach 
      
        //console.log("Adding Customer " + plaintiff_id);
        var selected_plaintiff_id = $('#selected_plaintiff_id').val();
        if (selected_plaintiff_id && plaintiff_id && (selected_plaintiff_id == plaintiff_id)) {
            $('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p> Plaintiff already selected </p>");  
        } else {
            if(plaintiff_id){
                $.ajax({
                    type: 'GET', 
                    url : baseurl+"/admin/api-get-customer/"  + plaintiff_id, 
                    success : function (data) {
                        $("#litigation-wizard-error").html("").hide();
                        plaintiffDataTable.row().remove().draw();
                        $('#selected_plaintiff_id').val("");

                        var first_name  = (data.first_name) ? data.first_name: "";
                        var second_name = (data.second_name) ? data.second_name: "";
                        var third_name  = (data.third_name) ? data.third_name: "";
                        var fourth_name = (data.fourth_name) ? data.fourth_name: "";
                        var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                        plaintiffDataTable.row.add( [
                            data.id,
                            customer_name,
                            data.email,
                            data.mobile_1,
                            data.gender,
                            "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                        ] ).draw( false );   
                        $('#selected_plaintiff_id').val(data.id); //store array
                        //console.log("Selected Customer List " + $('#selected_plaintiff_id').val());
                    }
                });  
            }
        }          
    });  

    $('#selected-plaintiff-table').on("click", "a", function(event){
        event.preventDefault();
        var selectedRow = plaintiffDataTable.row($(this).parents('tr'));
        $('#selected_plaintiff_id').val("");
        selectedRow.remove().draw(false);
        $("#plaintiff_id").val("");
    });

    // defendant Details

    $('#defendant-modal-div').on('hidden.bs.modal', function () {
        var defendant_id = $("#defendant_id").val(); // defendant id is a hidden field in the defendant .modal-serach 
      
        //console.log("Adding Customer " + defendant_id);
        var selected_defendant_id = $('#selected_defendant_id').val();
        if (selected_defendant_id && defendant_id && (selected_defendant_id == defendant_id)) {
            $('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p> Defendant already selected </p>");  
        } else {
            if(defendant_id){
                $.ajax({
                    type: 'GET', 
                    url : baseurl+"/admin/api-get-customer/"  + defendant_id, 
                    success : function (data) {
                        $("#litigation-wizard-error").html("").hide();
                        defendantDataTable.row().remove().draw();
                        $('#selected_defendant_id').val("");

                        var first_name  = (data.first_name) ? data.first_name: "";
                        var second_name = (data.second_name) ? data.second_name: "";
                        var third_name  = (data.third_name) ? data.third_name: "";
                        var fourth_name = (data.fourth_name) ? data.fourth_name: "";
                        var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                        defendantDataTable.row.add( [
                            data.id,
                            customer_name,
                            data.email,
                            data.mobile_1,
                            data.gender,
                            "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                        ] ).draw( false );   
                        $('#selected_defendant_id').val(data.id); //store array
                        //console.log("Selected Customer List " + $('#selected_plaintiff_id').val());
                    }
                });  
            }
        }          
    });  

    $('#selected-defendant-table').on("click", "a", function(event){
        event.preventDefault();
        var selectedRow = defendantDataTable.row($(this).parents('tr'));
        $('#selected_defendant_id').val("");
        selectedRow.remove().draw(false);
        $("#defendant_id").val("");
    });
     
    // defendant Details end

    //property details

    $('#property-modal-div').on('hidden.bs.modal', function () {
        var id = $("#property_id").val();
        if(id){
            $("#litigation-wizard-error").html(""); 
            $.ajax({
                type: 'GET', 
                url : baseurl+"/admin/property-main-partial/" + id, 
                success : function (data) {
                    $("#property-details").html(data);
                    $("#wizard-error").html("").hide();
                }
            }); 
        }else{
           $("#litigation-wizard-error").html("Please select property").show(); 
        }           
    });    

    //property details end


});

function processNextStep(form, nextView, wizardSection, dataModalDiv) {

    var baseurl  = $("#baseurl").val();
    var formData = form.serialize();
    formData    += "&next_view=" + nextView;
    var url = baseurl+"/app/litigation-wizard";
    $.post(url,  formData, 
        function (data) {
            $("#litigation-wizard-error").html("").hide();
            $("#"  + wizardSection).html(data);
            //$("#" + dataModalDiv).show();
            return false;
        }).fail(function(){
            console.log("error");
    });      
}


  