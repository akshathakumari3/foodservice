/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }    
});
    
$('form').submit(function() {
  $body.addClass("loading");
  $(this).find("button[type='submit']").prop('disabled',true);
  $(this).find("input[type='submit']").prop('disabled',true);
});

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}

function flashMessage(message, type) {
    $("#notify-include span").html(message); 
    $("#notify-include alert").addClass(type);
    $("#notify-include").fadeTo(3000, 500).slideUp(1000, function(){
        $("#success-alert").slideUp(1000);
    });         
}

$("#resetSearch").on('click', function() {
        window.location=window.location;
});
