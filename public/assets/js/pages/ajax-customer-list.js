$(document).ready(function () {      
    var select  =  $("#select").data('select');
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/admin/api-get-customers';
    var modal   = $('#customer-table').DataTable({
    "order": [[ 0, "desc" ]],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    // "aoColumnDefs": [
    //     { "bSortable": false, "aTargets": [ -1 ] }
    // ], 
    "order": [[ 0, "desc" ]],
    "ajax": url, 
    "columns": [
            {data : "created_at"},   
            {data: "first_name",render: function ( data, type, full ) {
                var first_name  = (full["first_name"]) ? full["first_name"]: "";
                var second_name = (full["second_name"]) ? full["second_name"]: "";
                var third_name  = (full["third_name"]) ? full["third_name"]: "";
                var fourth_name = (full["fourth_name"]) ? full["fourth_name"]: "";
                var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                if(name != "   "){
                    return  "<a class='text-upper' href="+baseurl+'/app/customers/'+full["id"]+">"+name+"</a>"; 
                } else {
                    return '-';
                }
            }},     

            {data: "mobile_1",render: function ( data, type, full ) {

                var mobile_1 = (full["mobile_1"]) ? full["mobile_1"]: "";
                var mobile_2 = (full["mobile_2"]) ? ", "+full["mobile_2"]: "";
                if(mobile_1 || mobile_2) {
                    return  mobile_1  + mobile_2 ;
                }else {
                    return '-';
                }

            }},
            { data: "gender",render: function ( data, type, full ) {
                if(data) {
                    return data;
                }else {
                    return '-';
                }
            }}, 
            { data: "date_of_birth",render: function ( data, type, full ) {
                if(data) {
                    return data;
                }else {
                    return '-';
                }
            }}, 
          
            {data : "",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/customers/'+full["id"]+"/edit><i class='glyphicon glyphicon-edit user-edit' title='edit customer'></i></a>"; 
            }},
		],   
    "columnDefs": [{ targets: [5], className: 'text-center', "orderable": false }],    
        colReorder: true
    });
});