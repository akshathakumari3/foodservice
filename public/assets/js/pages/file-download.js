function downloadDocument (id) {

    var document_id   =  $(id).attr("data-document_id");
    var document_type =  $(id).attr("data-document_type");
    var document_name =  $(id).attr("data-document_name");
    var baseurl       = $('#baseurl').val();

    $.ajax({
        type: 'get', 
        url : baseurl+"/admin/file/download/"+document_id, 
        success : function (data) {

            const linkSource   = `data:application/pdf;base64,${data}`;
            const downloadLink = document.createElement("a");
            const fileName     = document_name+'.'+document_type;

            downloadLink.href     = linkSource;
            downloadLink.download = fileName;
            downloadLink.click();
         
            
        }
    });
};