$(document).ready(function () { 
	
	var url    = $("#baseurl").val()+'/app/policy/getPolicyMemberListJson';
	var modal  = $('#member-policy-list').DataTable({
	    "responsive": true,
	    "processing": true,
	    "serverSide": false,
	    "order": [[ 0, "desc" ]],
	    "ajax": url, 
	    "columns": [
	    	{data : "id",
                "visible": false,
                "searchable": false
            },
            {data : "reference_no",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/policy/'+full["id"]+" class='blueRow'>"+data+"</a>"; 
            }},
			
			{ data: "group_policy_id", render: function ( data, type, full ) { 
                if(data != '' && data != null) {
                    console.log(data+"if");
                    return data;
                } else {
                    console.log(data+"else");
                    return '-';
                }
            }}, 
			{ data: "client_name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "lineOfBusiness.name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "product.name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            // { data: "productVariant.variant",name: "productVariant.variant", orderable: true }, 
            { data: "customer.first_name", render: function ( data, type, full ) { 
                if(data != '   ') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            // { data: "sum_insured" },
            { data: "sum_insured", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "tenure", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "created_at", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
        ],
        "columnDefs": [{ targets: [7], className: 'text-right' }],                
        colReorder: true,
	});
});
