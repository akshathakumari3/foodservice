// $("#receiver_sign_save").click(function(e){
// 	html2canvas([document.getElementById('receiver_signature_pad')], {
// 		onrendered: function (canvas) {
// 			var canvas_img_data 	= canvas.toDataURL('image/png');
// 			receiver_signgnature    = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
// 			$("#receiver_signgnature").val(receiver_signgnature);
// 		}
// 	});
// });
$("#store-delivery-document").click(function(e){
	$("body").addClass("loading");
	var img_data        		= [];
	var receiver_name   		= ($("#transaction_details").data("resever-name")).replace(/ /g,'');
	var receiver_signgnature 	= $("#receiver_signgnature").val();
	var recevier_id_card 	    = $("#recevier_id_card").val();
	var recevier_photo          = $("#recevier_photo").val();
	var transaction_id         = $("#transaction_details").data("transaction-id");
	if(recevier_id_card ==null || recevier_id_card ==""){
		$("body").removeClass("loading");
		swal({
            title : "Receiver ID Card is Required",
            text  : "",
            type  : "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
	        image: recevier_id_card, 
	        name:  transaction_id+'idcard.png',
	        document_name: 'Customer IDCard'
	    });
    }
	
	if(recevier_photo ==null || recevier_photo ==""){
		$("body").removeClass("loading");
		swal({
            title : "Receiver Photo is Required",
            text  : "",
            type  : "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
	        image: recevier_photo, 
	        name:  transaction_id+'receiverimage.png',
	        document_name: 'Customer Image'
	    });
    }

	if(receiver_signgnature ==null || receiver_signgnature ==""){
		$("body").removeClass("loading");
		swal({
            title : "Receiver Signature is Required",
            text  : "",
            type  : "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
            image: receiver_signgnature, 
            name:  transaction_id+'singnature.png',
            document_name: 'Customer Signature'
        });
	}

	if(img_data.length!=0){
		uploadImage(img_data)
	}

});

function uploadImage(data){
	var transaction_id         = $("#transaction_details").data("transaction-id");
	var property_id            = $("#transaction_details").data("property-id");
	var serial_number          = $("#transaction_details").data("serial-number");
	var receiver_name          = $("#transaction_details").data("resever-name");
	var baseurl                = $("#baseurl").val();
	var seriel_ref_number_name = $("#seriel_ref_number_name").val();
	var receiver_phone_number  = $("#transaction_details").data("receiver-phone-number");
// alert(receiver_phone_number);

	$.ajax({
		url: baseurl+'/admin/delivery/document/store',
		data: {"_token": $('#token').val(),"transaction_id":transaction_id,"property_id":property_id,"image_data":data,
		"serial_number":serial_number,"receiver_name":receiver_name,"seriel_ref_number_name":seriel_ref_number_name,
	    "receiver_phone_number":receiver_phone_number},
		type: 'post',
		success: function (data) {
			// console.log(data);
			if(data=='pass'){
				// console.log('pass');
				window.location =  baseurl+'/workflow/application/'+transaction_id;
			}else{
				// console.log('fail');
				window.location =  baseurl+'/admin/delivery';
			}
		}
	});
}
