$(document).ready(function () {      
    
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/product-variant/addons/data-table';
    var modal   = $('#product-addons-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "columnDefs": [
        { "orderable": false,"targets": [ 0,3 ]},
        { "searchable": false, "targets": [ 0,3 ]},
    ],
    "order": [[ 0, "desc" ]],
    "ajax": {
         "url" : url, 
        "type" :"get",
        "data" : {
                "id" : $("#varient_id").val()
            },
        },
    "columns": [

            {data: 'DT_Row_Index', name: 'id'},
            {data: "service_name",name:"service_name"},
            {data: "description",name:"description"},
            {data : "",render: function ( data, type, full ){
                return  "<a class='product-addons-delete' data-id="+full["id"]+"><i class='glyphicon glyphicon-trash user-delete' title='Delete'></i></a>"; 
            }}
			],            
        colReorder: true
    });
});

$(document).on('click', '.product-addons-delete', function(){
    var id = $(this).data('id');
    Swal.fire(swal_fire).then((result) => {
        if (result.value) {
            $.ajax({
                type: "DELETE",
                url : $("#baseurl").val()+"/app/product-variant/addons/"+id,
                success : function (data) {
                    window.location.reload(true);
                }
            });
        }
    });
});