$(document).ready(function () {      
    var business_name = $("#business_name").val();
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/product-variant/inclustion/data-table';
    var modal   = $('#product-inclusion-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "columnDefs": [
        { "orderable": false,"targets": [ 0, 1, 7 ]},
        { "searchable": false, "targets": [ 0,7 ]},
    ],
    "order": [[ 0, "desc" ]],
    "ajax": {
         "url" : url, 
        "type" :"get",
        "data" : {
                "id" : $("#varient_id").val()
            },
        },
    "columns": [

            {data: 'DT_Row_Index', name: 'id'},
            {data: "service",name:"service.code"},
            {data: "description",name:"service.description"},
            // {data: "amount_limit",name:"amount_limit"},
            {
                data: 'amount_limit',
                render: function ( data, type, row ) {
                    if(data) {
                        return parseFloat(data).toFixed(2);
                    }
                    else {
                        return 0;
                    }
                }
            },

            {data : "sessions_limit",name:"sessions_limit"},
            // {data : "inpatient_amount_limit",name:"inpatient_amount_limit"},
            {
                data: 'inpatient_amount_limit',
                render: function ( data, type, row ) {
                    if(data) {
                        return parseFloat(data).toFixed(2);
                    }
                    else {
                        return 0;
                    }
                }
            },
            {data : "inpatient_sessions_limit",name:"inpatient_sessions_limit"},
            {data : "inpatient_sessions_limit",render: function ( data, type, full ){
                return  "<a class='product-inclusion-delete' data-id="+full["id"]+"><i class='glyphicon glyphicon-trash user-delete' title='Delete'></i></a>"; 
            }}

           
			],
    "columnDefs": [{ targets: [3, 4, 5, 6], className: 'text-right' },{ targets: [7], className: 'text-center' }],            
        colReorder: true
    });

    if(business_name != 'Health'){
        var column = modal.column(5);
        var column2 = modal.column(6);
        // Toggle the visibility
        column.visible( ! column.visible() );
        column2.visible( ! column2.visible() );
    }
   
});

$(document).on('click', '.product-inclusion-delete', function(){
    var id = $(this).data('id');
    Swal.fire(swal_fire).then((result) => {
        if (result.value) {
            $.ajax({
                type: "DELETE",
                url : $("#baseurl").val()+"/app/product-variant/inclusion/"+id,
                success : function (data) {
                    //console.log('success');
                    // console.log(data);
                    window.location.reload(true);
                    
                }
            });
        }
    });
});