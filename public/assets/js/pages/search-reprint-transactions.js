$("#search-reprint").click(function() {

    var transaction_ref_no = $("#transaction_ref_no").val();
    var mobile_number      = $("#mobile_number").val();
    var buyer_name         = $("#buyer_name").val();
    
    if(transaction_ref_no || mobile_number || buyer_name){

        // $(".loading-modal").css("display", "none");
        $("#reprint-loader").css("display", "block");

        var url = $('#baseurl').val()+"/reprint/search-transaction";

        $.ajax({
            type : 'get', 
            url  : url, 
            data : {"transaction_ref_no":transaction_ref_no,"mobile_number":mobile_number,"buyer_name":buyer_name},
            success : function (data) {

                $("#reprint-search-result").html(data);
                $('#transaction_table').DataTable();
                $("#reprint-loader").css("display", "none");
            }
        });
    }else{
        
        var text ='Please Enter Transaction Input';
        $("#warning-message-modal .modalMessage").html(text);
        $("#warning-message-modal").modal("show");
    }
});

function reprintTransactionDetails(transaction_id,buyer_id) {
    
    var url = $('#baseurl').val()+"/reprint/get-transaction/"+transaction_id;
    $("#reprint-loader").css("display", "block");
    $.ajax({
        type : 'get', 
        url  : url, 
        success : function (data) {

            $("#wizard-transaction-view").html(data);
            $("#transaction_id").val(transaction_id);
            $("#customer_id").val(buyer_id);
            $("#transaction-modal-div").modal('hide');
            $("#reprint-loader").css("display", "none");
        }
    });
}

$("#upload-reprint-document").click(function(){

    var document_type_id = $("#document_type_id").val();
    var document_name    = $("#document_name").val();
    var file             = $("#document").val();

    if(!document_type_id){
        $("#document_type_error").html("Please Select Document Type");
        return false;
    } else {
        $("#document_type_error").html("");
    }

    if(!document_name){
        $("#document_name_error").html("Please Enter Document Name");
        return false;
    } else {
        $("#document_name_error").html("");
    }

    if(!file){
        $("#document_error").html("Please Upload Document");
        return false;
    } else {
        $("#document_error").html("");
    }

    $("#gis-loader").css("display", "block");
    var $this = $(this);
    $this.button('loading');

    var url = $('#baseurl').val()+"/reprint/add-document";

    $.ajax({

        headers : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        url:url,
        data:new FormData($("#upload-document-form")[0]),
        dataType:'json',
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(data){

            if(data.count){
                $("#reprint_documents_count").val(data.count);
                $("#reprint-documents-data").html(data.view);
            }else{
                $("#wizard-error").html(data).show();
            }

            $this.button('reset');
            $("#transaction-document-div").modal('hide');
            $("#gis-loader").css("display", "none");
        },
        error: function(data1)
        {
            $("#wizard-error").html(data1).show();
        }
    });
 });


