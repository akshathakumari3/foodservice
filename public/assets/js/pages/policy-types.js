policyTypesHide();
function policyTypesHide(){
    
    $("#policy_types").hide();
}
$("#policy_group").on('change',function(){
    var policy_group_id = $("#policy_group").val();
    getPolicyTypes(policy_group_id);
});

function getPolicyTypes(policy_group_id){

    var baseurl = $("#baseurl").val(); 
    var url     = baseurl+"/app/policy/get-policy-types";

    $.ajax({
        type: "get",
        url: url,
        data : {"policy_group_id" : policy_group_id},
        success : function (data) {
            var values=["<option value=''> Please Select </option>"];
            $.each(data, function (index, value) {
                
                var value = "<option value='"+value.id+"'>"+value.name +"</option>";
                values.push(value);
            });
            
            $("#policy_types").show();
            $('#policy_type').html(values);
        }
    });
}