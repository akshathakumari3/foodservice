$(document).ready(function () { 
	
	var url    = $("#baseurl").val()+'/app/pricing/getPricingListJson';
	var modal  = $('#pricing-table').DataTable({
	    "responsive": true,
	    "processing": true,
	    "serverSide": true,
	    "order": [[ 0, "desc" ]],
		"columnDefs": [
	        { "orderable": false,"targets": [ 0, 1, 2, 3 ]},
	        { "searchable": false, "targets": [ 0,5 ]},
    	],
	    "ajax": url, 
	    "columns": [
	    	{data: 'DT_Row_Index',name: 'id'},
            {data: "lineOfBusiness", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: "product", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: "productOption", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: "productVariant", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: 'sum_insured',name: 'productVariant.sum_insured',render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/pricing/'+full["id"]+" class='blueRow'>$"+data+"</a>"; 
            }},
            {data: "actions" },
        ],   
        "columnDefs": [{ targets: [5], className: 'text-right' }, { targets: [6], className: 'text-center', orderable:false }],         
        colReorder: true,
	});

	$(document).on('click', '.priceing-delete', function(){
	    var id = $(this).data('id');
	    Swal.fire(swal_fire).then((result) => {
	        if (result.value) {
	            $.ajax({
	                type: "DELETE",
	                url : $("#baseurl").val()+"/app/pricing/"+id,
	                success : function (data) {
	                    
	                    window.location.reload(true);
	                }
	            });
	        }
	    });
	});
});