$("#mortgage").on('click', "#search_lien_property", function() {

	var parcel_id = $("#parcel_id").val();
	if(parcel_id){
	   var url = $('#baseurl').val()+"/lien/search-property";

        $.ajax({
            type: 'get', 
            url : url, 
            data : {"parcel_id":parcel_id},
            success : function (data) {

                if(data.result == true){

                   $("#property-details").html(data.data);
                   $("#property_id").val(data.property_id);
                   $("#customer_id").val(data.owner_id); 
     
                }else{
                   $("#property-details").html(data.data); 
                }
            }
        });
	}else{
		
		var text ='Please Enter Parcel ID';
        $("#warning-message-modal .modalMessage").html(text);
        $("#warning-message-modal").modal("show");
	}
});