$(document).ready(function () { 
	
	var url    = $("#baseurl").val()+'/app/specialist/list-datatable';
	var modal  = $('#specialist-table').DataTable({
	    "responsive": true,
	    "processing": true,
	    "serverSide": true,
	    "order": [[ 0, "desc" ]],
	    "ajax": url, 
	    "columns": [
            { data: "name" ,render: function ( data, type, full ) {
                return  "<a href="+$("#baseurl").val()+'/app/specialist/'+full["id"]+">"+data+"</a>"; 
            }},
			{ data: "speclailist_name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
                 
            }}, 
            { data: "contact_no", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
                 
            }}, 
            { data: "educations.value", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
                 
            }},
            { data: "licence", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
                 
            }}, 
            // { data: "status" },
            {data : "status",render: function ( data, type, full ){
                if(data == 'Active'){
                return  "<span class='success-msg'>"+full["status"]+"<span>"; 
            } else {
                return  "<span class='error-msg'>"+full["status"]+"<span>"; 
            }
            }},
            {data : "id",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/specialist/'+full["id"]+"/edit><i class='glyphicon glyphicon-edit user-edit' title='edit customer'></i></a>"; 
            }}
        ],
        "columnDefs": [{ targets: [6], className: 'text-center', orderable:false }],       
        colReorder: true,
	});
});