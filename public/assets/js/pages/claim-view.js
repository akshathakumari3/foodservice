$(document).ready(function () { 
    var baseurl  = $("#baseurl").val();

    $('.approve-claim-amount').click(function(){
        $("#claim_amount_error").text("");
		$("#claim_mode_error").text("");
        var claim_amount = $("#paid_amount").val();
		var mode_of_payment = $("#mode_of_payment").val();
		if(claim_amount == "" || claim_amount==null){
            $("#claim_amount_error").text('Please Enter Settled Amount');
            return false;
        }
		
        if(mode_of_payment == "" || mode_of_payment==null){
            $("#claim_mode_error").text('Please Select Mode of Payment');
            return false;
        }
        var claim_id = $("#claim_application_id").val();
		var $this = $(this);
		$this.button('loading');

        $.ajax({
            type    : 'POST', 
            url     :  baseurl+'/app/claim/settle-claim/'+claim_id,
            data    : {"paid_amount":claim_amount,"mode_of_payment":'USDollar',
            "amount_payment_type":'Full'},
            success : function (data) {
                $('#approve-claim-modal-add').modal('hide');
                $('#claim-amount-approve').val('');
                location.reload();
            }
        });
            
    });

    $('#reject').click(function(){		
		var comments=$('#comments').val();
		if(comments==""){
		swal({
                title: "Please write Comments",
                text: "",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#418BCA",
                confirmButtonText: "Ok",
            });
			return false;
		}
        var claim_status = "Rejected";
        var claim_amount = 0;
        var claim_id = $(".reject_application_id").val();

        var status = "Rejected";
        var post_url = baseurl+'/app/claim/post-claim-status/'+claim_status+'/'+claim_amount+'/'+claim_id+'/'+status;
		var $this = $(this);
		$this.button('loading');

        $.ajax({
            type    : 'GET', 
            url     : post_url,
			data : {"comments" : comments},
            success : function (data) {
                if(data == 'true'){
                   location.reload();
                }
            }
        }); 
    });
	
	$('#approve-claim').click(function(){
        var claim_status = "Approved";		
		$("#claim_amount_error").text("");
       
		var claim_total_amount=$("#claim_totol_amount").val();
		 var claim_amount =$("#claim_amount").val();
		  var claim_amounts =parseFloat($("#claim_amount").val());
        var claim_id = $("#claim_application_id").val();	
		 if(claim_amount == "" || claim_amount==null){
            $("#claim_amount_error").text('Please Enter Approve Amount');
            return false;
        }
		else  if(claim_total_amount < claim_amounts){
            $("#claim_amount_error").text('Please Enter valid Amount');
            return false;
        }
		
        var status = "Approved";
		var $this = $(this);
		$this.button('loading');
        var post_url = baseurl+'/app/claim/post-claim-status/'+claim_status+'/'+claim_amount+'/'+claim_id+'/'+status;

        $.ajax({
            type    : 'GET', 
            url     : post_url,
            success : function (data) {
				$('#approve-claim-amount').modal('hide');
				 $this.button('reset');
                if(data == 'true'){
                    location.reload();
                }
            }
        }); 
    });
	

    $.fn.select2.defaults.set("theme","bootstrap");
    $('select[name="document_c_type_id"]').select2({
        //dropdownParent: '',
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : baseurl+'/app/claim/get-servicetypes',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {      
        var data = e.params.data;
        $("#policy_select_value").val(data.text);
    });

   $('body').on('click','#upload-claim-view-document',function(){
    // console.log('dddd');
    var document_type_id = $("#policy_select_value").val();
    var document_name    = $("#document_c_name").val();
    var file             = $("#document_c").val();

    if(!document_type_id){
        $("#document_c_type_error").html("Please Select Document Type");
        return false;
    } else {
        $("#document_c_type_error").html("");
    }

    if(!file){
        $("#document_c_error").html("Please Upload Document");
        return false;
    } else {
        $("#document_c_error").html("");
    }

    $("#gis-loader").css("display", "block");
    var $this = $(this);
    $this.button('loading');
    var url = $('#baseurl').val()+"/app/claim/update-document";

    $.ajax({
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:url,
        data:new FormData($("#upload-document-form")[0]),
        dataType:'json',
        // async:false,
        type:'post',
        processData: false,
        contentType: false,
        success:function(data){
            if(data.count){
                $("#claim_documents_count").val(data.count);
                // $("#claim_documents_data").html(data.view);
                location.reload();
            }else{
                $("#wizard-error").html(data).show();
            }

            $this.button('reset');
            /* empty field*/
            $("#document_c_type_id").val('');
            $("#document_c_name").val('');
            $("#document_c").val('');
            $("#document-modal-claim-view-add").modal('hide');
            $("#gis-loader").css("display", "none");
        },
        error: function(data1)
        {   $this.button('reset');
            $("#wizard-error").html(data1).show();
        }
    });
});

function deleteAddedDocument(id){
    $.ajax({
        type:'get',
        url: $('#baseurl').val()+"/app/claim/delete-added-document",
        data:{document_id:id,claim_id:$("#claim_document_id").val()},
        success:function(data){
            $("#claim_documents_count").val(data.count);
            $(".claim_documents_data").html(data);
        }
    });
}
});