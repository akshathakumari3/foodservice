$(document).ready(function () { 
	
	var url    = $("#baseurl").val()+'/app/policy/getPolicyListJson';
	var modal  = $('#policy-table').DataTable({
	    "responsive": true,
	    "processing": true,
	    "serverSide": false,
	    "order": [[ 0, "desc" ]],
	    "ajax": url, 
	    "columns": [
	    	{data : "id",
                "visible": false,
                "searchable": false
            },
            {data : "reference_no",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/policy/'+full["id"]+" class='blueRow'>"+data+"</a>"; 
            }},
			
			// { data: "group_policy_id",name: "group_policy_id", orderable: true }, 
            { data: "group_policy_id",name: "group_policy_id" ,render: function ( data, type, full ) {
                if(full["group_policy_id"] != ""){
                    console.log('ree');
                    return  data; 
                }else{
                    console.log('else');
                  return "-";
                }
            }},

			// { data: "client_name",name: "client_name", orderable: true }, 
            { data: "client_name",name: "client_name" ,render: function ( data, type, full ) {
                if(full["client_name"] != ""){
                    console.log('ree');
                    return  data; 
                }else{
                    console.log('else');
                  return "-";
                }
            }},
            { data: "lineOfBusiness.name",name: "lineOfBusiness.name", orderable: true }, 
            { data: "product.name",name: "product.name", orderable: true },
            // { data: "productVariant.variant",name: "productVariant.variant", orderable: true }, 
            { data: "customer.first_name",name: "customer.first_name" ,render: function ( data, type, full ) {
                if(full["customer"] != null){
                    return  "<a class='text-upper' href="+$("#baseurl").val()+'/app/customers/'+full["customer"]['id']+">"+data+"</a>"; 
                }else{
                  return "-";
                }
            }},

            // { data: "sum_insured" },
            { data: "sum_insured"},
            { data: "tenure",name:"tenure" },
            { data: "created_at",name:"created_at" },
			 { data: "application_status",name: "application_status" ,render: function ( data, type, full ) {
				 if(full.application_status==1){
					 return 'Active';
				 }else{
					  return 'In Active';
				 }
			 }}
        ],
        "columnDefs": [{ targets: [7], className: 'text-right' }],                
        colReorder: true,
	});
});
