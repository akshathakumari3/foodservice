$(document).ready(function () { 
	
	var select = $("#select").data('select');
	var url    = $("#baseurl").val()+'/admin/api-get-customers';
	var modal  = $('#customer-modal').DataTable({
	    "responsive": true,
	    "processing": true,
	    "serverSide": true,
	    "order": [[ 0, "desc" ]],
	    "ajax": url, 
	    "columns": [
	            { data: "id" },
	            {data: "first_name",render: function ( data, type, full ) {
	                var first_name  = (full["first_name"]) ? full["first_name"]: "";
	                var second_name = (full["second_name"]) ? full["second_name"]: "";
	                var third_name  = (full["third_name"]) ? full["third_name"]: "";
	                var fourth_name = (full["fourth_name"]) ? full["fourth_name"]: "";
	                var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

	                return name }},       
	            { data: "mobile_1" },
	            { data: "gender" },           
	            { data : undefined, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>", searchabe: false }                        
	            ],            
	        colReorder: true
	});
                        
	$('#customer-modal').on( 'click', 'button', function () {

	    var data = modal.row( $(this).parents('tr') ).data();
	    $("#customer_id").val(data.id); 
	    $('#customer-modal-div').modal('hide');


	}); 
});