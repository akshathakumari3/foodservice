function duplicatePropertyCheck() {

	var legacy_file_number = $("#legacy_file_number").val();
	var reference_number   = $("#reference_number").val();
	var location_reference = $("#location_reference").val();
	var district           = $("#district").val();
	var sub_division       = $("#sub_division").val();
	var seller_mobile_no   = $("#seller_mobile_1").val();
    var baseurl            = $("#baseurl").val();

    if((legacy_file_number || reference_number) || (location_reference && district && sub_division) || (seller_mobile_no)){
     
    	$.ajax({
                type: "POST", 
                url : baseurl+"/admin/properties/duplicate",
                data : {"legacy_file_number":legacy_file_number,"reference_number":reference_number,
                       "location_reference":location_reference,"district":district,
                       "sub_division":sub_division,"seller_mobile_no":seller_mobile_no}, 
                success : function (data) {
                	
                    if(data.result == 'fail'){
                        $('#save-register-property').attr('disabled', 'true');
                    	$("#warning-message-modal .modalMessage").html(data.message);
    	                $("#warning-message-modal").modal("show");
                    }else{
                        $('#save-register-property').removeAttr("disabled");
                    }
                }
        });
    } 
}


function duplicateAssignParcelIdCheck () {
    var gis_reference_id   =  $("#gis_reference_id").val();
    var street_number      =  $("#street_number").val();
    var street_type        =  $("#street_type").val();
    var directions         =  $("#street_direction").val();
    var door_no            =  $("#door_no").val();
    var postal_code        =  $("#postal_code").val();

    var baseurl            = $("#baseurl").val(); 
    $.ajax({
        type: "get", 
        url : baseurl+"/admin/properties/duplicateReferenceId",
        data : {"gis_reference_id":gis_reference_id,"street_number":street_number,
               "street_type":street_type,"directions":directions,
               "door_no":door_no,"postal_code":postal_code}, 
        success : function (data) {

            if(data == 'reff_id'){
                $("#warning-message-modal .modalMessage").html("GIS Reference Id Already Created");
                $("#warning-message-modal").modal("show");
            }else if(data == 'property'){
                $("#warning-message-modal .modalMessage").html("Property Address Already Created");
                $("#warning-message-modal").modal("show");
            }
        }
    });

}


 $(document).on('click', '#property-address_sync', function () {
    var property_id      = $("#property-address_sync").data("property-id");
    var global_id        = $("#property-address_sync").data("global-id");
    var migration_status = $("#property-address_sync").data("migration_status");
    var transaction_id   = $("#transaction_id").val();
    
    if(migration_status=="Yes"){
        // $("#provisional-confirmation-message-modal .modalMessage").html("Are You Sure, Parcel ID Sync for This Property");
        $("#provisional-confirmation-message-modal").modal("show");
    }else{
        $("#info-message-modal .modalMessage").html("Migrate the Parcel First");
        $("#info-message-modal").modal("show");
    }
    var baseurl = $('#baseurl').val();
    $("#provisional-confirm").click(function() {
        var provisional = $('#provisional:checked').val();
        $.ajax({
            type: "POST",
            url : baseurl+'/admin/arcGis/updateParcel',
            data : {"property_id":property_id,"global_id":global_id,
                    "provisional":provisional,"transaction_id":transaction_id
                   },
            success : function (data) {
                location.reload();
                
            }
        });
    })
});

$("#building").on('change',function(){
    
    var building = $("#building").val();

    if(building == 1){
        $("#property_number_of_stories").show();
        $("#property_building_area").show();
        $("#no_of_buildings").val('1');
        $("#property_no_of_buildings").show();
        
    }else{
        $("#property_number_of_stories").hide();
        $("#number_of_stories").val('');
        $("#property_building_area").hide();
        $("#building_area").val('');
        $("#property_no_of_buildings").hide();
        $("#no_of_buildings").val('');
    }
   
});

$("#property_usage").on('change',function(){
    var building = $("#property_usage").val();

    if(building == "Business"){
        $("#business_types").show();
    }else{
        $("#business_types").hide();
        $("#business_type").val('');
    }
   
});
