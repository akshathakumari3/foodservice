"use strict";
$(document).ready(function () {     
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/clientMembers/get-members-datatable';
    var modal   = $('#client-member-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": url, 
    "columns": [
        // {data: "customers.employer_id",name:"customers.employer_id"},
        {data: "customers.employer_id",render: function ( data, type, full ) {
            if(full["customers"]["employer_id"] != undefined){
                var employer_id = full["customers"]["employer_id"];
                return employer_id;
            }else{
                return "-";
            }  
        }},
        {data: "customers.first_name",render: function ( data, type, full ) {
            var customer = full["customers"];
            if(data != "   ") {
                return  "<a class='text-upper' href="+$("#baseurl").val()+'/app/customers/'+customer.id+">"+data+"</a>"; 
            } else {
                return "-";
            }
        }},
		{data: "client_id", render: function ( data, type, full ) { 
            if(data != '') {
                return data;
            } else {
                return '-';
            }
        }},
		{data: "familyRelation.relation.relationship_name", render: function ( data, type, full ) { 
            if(data != '') {
                return data;
            } else {
                return '-';
            }
        }},
        {data: "customers.gender", render: function ( data, type, full ) { 
            if(data != '') {
                return data;
            } else {
                return '-';
            }
        }},
		{data: "customers.date_of_joining", render: function ( data, type, full ) { 
            if(data != '') {
                return data;
            } else {
                return '-';
            }
        }},
		// {data: "status"},
        {data : "status",render: function ( data, type, full ){
            if(data == 'Active'){
            return  "<span class='success-msg'>"+full["status"]+"<span>"; 
        } else {
            return  "<span class='error-msg'>"+full["status"]+"<span>"; 
        }
        }},
        {data: "id",render: function ( data, type, full ) {
            var customer = full["customers"];
            return  "<a href="+$("#baseurl").val()+'/app/customers/add-family-member/'+customer.id+" class='form-group btn btn-sm btn-success'><span class='glyphicon glyphicon-plus'></span> Add Members</a>"; 
        }}
    ], 
    "columnDefs": [{ targets: [7], className: 'text-center', orderable:false }],           
        colReorder: true
    });
});

$(document).ready(function () { 
    
    var url    = $("#baseurl").val()+'/app/policy/getPolicyListJson';
    var modal  = $('#policy-table0').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            { "orderable": false,"targets": [ 1, 2,3, 4 ]},
          ],
        "ajax": url, 
        "columns": [
            {data : "id",
                "visible": false,
                "searchable": false
            },
            {data : "reference_no",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/policy/'+full["id"]+" class='blueRow'>"+data+"</a>"; 
            }},
            { data: "lineOfBusiness",name: "lineOfBusiness.name" }, 
            { data: "product",name: "product.name" },
            { data: "productVariant",name: "productVariant.variant" }, 
            { data: "customer.name",name: "customer.name" }, 
            // { data: "sum_insured" },
            { data: "sum_insured"},
            { data: "tenure_num" },
        ],
        "columnDefs": [{ targets: [6], className: 'text-right' }],                
        colReorder: true,
    });
});
