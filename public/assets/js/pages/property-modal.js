$(document).ready(function () {   
    var baseurl = $("#baseurl").val();
    var select  =  $("#select").data('select');
    var modal   = $('#property-modal').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "order": [[ 0, "desc" ]],
    "ajax": baseurl+'/admin/property-datatable', 
    "columns": [
            { data: "id" },
            { data: "parcel_id", render: function ( data, type, full ) {
                                        return (full["parcel_id"]?full["parcel_id"]:"[[PENDING]]")    }},            
            { data: "address", render: function ( data, type, full ) {
                
                return (full["address"]?full["address"]:"[[PENDING]]") + "<br/>"  + (full["location_reference"]) ? full["location_reference"]: "" + "<br/>" + (full["district"]) ? full["district"]: "" + (full["sub_division"]?"-"+full["sub_division"]:"") + (full["area"]?"-"+full["area"]:"") + "-" + (full["block"]?"-"+full["block"]:"")   }},            

                                        // return (full["address"]?full["address"]:"[[PENDING]]") + "<br/>"  + full["location_reference"] + "<br/>" + full["district"] + (full["sub_division"]?"-"+full["sub_division"]:"") + (full["area"]?"-"+full["area"]:"") + "-" + (full["block"]?"-"+full["block"]:"")   }},            
            { data: "currentowners", render: function ( data, type, full ) {
                                        return $.map( data, function ( d, i ) {
                                            var first_name  = (d.customer.first_name) ? d.customer.first_name: "";
                                            var second_name = (d.customer.second_name) ? d.customer.second_name: "";
                                            var third_name  = (d.customer.third_name) ? d.customer.third_name: "";
                                            var fourth_name = (d.customer.fourth_name) ? d.customer.fourth_name: "";
                                            var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                                          if (d.primary_owner == "Yes") name += " [P] ";
                                            return name;
                                        } ).join( '<br/> ' ); },
                                "searchable" : false},
                                                                                

            { data : null, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>" ,"searchable" : false}                        
            ],            
        colReorder: true
    });

                        


    $('#property-modal').on( 'click', 'button', function () {
        var data = modal.row( $(this).parents('tr') ).data();
        $("#property_id").val(data.id); 
        $('#property-modal-div').modal('hide');
       
    } );    
} );