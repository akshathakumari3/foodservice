$("#customer-sign-save").click(function(e){
	html2canvas([document.getElementById('customer-signature-pad')], {
		onrendered: function (canvas) {
			var canvas_img_data = canvas.toDataURL('image/png');
			customer_img        = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
			$("#customer-sign").val(customer_img);
		}
	});
});
$("#witness1-sign-save").click(function(e){
	html2canvas([document.getElementById('witness1-signature-pad')], {
		onrendered: function (canvas) {
			var canvas_img_data = canvas.toDataURL('image/png');
			witness1_img        = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
			$("#witness1-sign").val(witness1_img);
		}
	});
});
$("#witness2-sign-save").click(function(e){
	html2canvas([document.getElementById('witness2-signature-pad')], {
		onrendered: function (canvas) {
			var canvas_img_data = canvas.toDataURL('image/png');
			witness2_img        = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
			$("#witness2-sign").val(witness2_img);
		}
	});
});

$("#seller-sign-save").click(function(e){
	html2canvas([document.getElementById('seller-signature-pad')], {
		onrendered: function (canvas) {
			var canvas_img_data = canvas.toDataURL('image/png');
			seller_img          = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
			$("#seller-sign").val(seller_img);
		}
	});
});

$("#notary-sign-save").click(function(e){
	html2canvas([document.getElementById('notary-signature-pad')], {
		onrendered: function (canvas) {
			var canvas_img_data = canvas.toDataURL('image/png');
			notary_img          = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
			$("#notary-sign").val(notary_img);
		}
	});
});

$("#store-notary-document").click(function(e){

	var buyer_sign 	    = $("#customer-sign").val();
	var witness1_sign 	= $("#witness1-sign").val();
	var witness2_sign 	= $("#witness2-sign").val();
	var notary_sign 	= $("#notary-sign").val();
	var seller_sign 	= $("#seller-sign").val();
	var transaction_id  = $("#transaction_details").data("transaction-id");
	var property_id     = $("#transaction_details").data("property-id");
	var baseurl         = $("#baseurl").val();

	var witness1_image 	= $("#witness1_image").val();
	var witness2_image 	= $("#witness2_image").val();
	var buyer_image 	= $("#buyer_image").val();
	var seller_image 	= $("#seller_image").val();

	var img_data        = [];
 	if(witness1_sign ==null || witness1_sign ==""){
		swal({
            title: "Witness1 signature is required",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
            img: witness1_sign, 
            name:  'witness1signature.png',
            document_name: 'Witness1 Signature'
        });
	} 

	if(witness2_sign ==null || witness2_sign ==""){
		swal({
            title: "Witness2 signature is required",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
            img: witness2_sign, 
            name:  'witness2signature.png',
            document_name: 'Witness2 Signature'
        });
	}

	if(buyer_sign ==null || buyer_sign ==""){
		swal({
            title: "Buyer signature is required",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
            img: buyer_sign, 
            name:  'buyersignature.png',
            document_name: 'Buyer Signature'
        });
	}

	if(seller_sign ==null || seller_sign ==""){
		swal({
            title: "Seller signature is required",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
            img: seller_sign, 
            name:  'sellersignature.png',
            document_name: 'Seller Signature'
        });
	}
	if(notary_sign ==null || notary_sign ==""){
		swal({
            title: "Notary signature is required",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        });
        return false;
	}else{
		img_data.push({
            img: notary_sign, 
            name:  'notarysignature.png',
            document_name: 'Notary Signature'
        });
	}

	//customer image add 
	if(witness1_image){
		img_data.push({
	            img           : witness1_image, 
	            name          : 'witness1_image.png',
	            document_name : 'Customer Image',
	            other         : 'image',
	            comments      : 'Witness1 Image'
	        });
	}

	if(witness2_image){
		img_data.push({
	            img           : witness2_image, 
	            name          : 'witness2_image.png',
	            document_name : 'Customer Image',
	            other         : 'image',
	            comments      : 'Witness2 Image'
	        });
	}

	if(buyer_image){
		img_data.push({
	            img           : buyer_image, 
	            name          : 'buyer_image.png',
	            document_name : 'Customer Image',
	            other         : 'image',
	            comments      : 'Buyer Image',
	        });
	}

	if(seller_image){
		img_data.push({
	            img           : seller_image, 
	            name          : 'seller_image.png',
	            document_name : 'Customer Image',
	            other         : 'image',
	            comments      : 'Seller Image',
	        });
	}

	//customer image end 

	var rowCount = $('#trustee_details tr').length;
    if (rowCount > 1) {
        $('#trustee_details tr').each(function(key) {
            var trustee_signature = $(this).find("td:eq(4) input[type='text']").val();
            var trustee_name      = $(this).find("td:eq(0)").text();
            if(trustee_signature){
	           	img_data.push({
		            img: trustee_signature, 
		            name:  'trustee'+key+'signature.png',
		            document_name: 'Trustee Signature',
		            document_file: 'Trustee Signature '+trustee_name
	        	});
	       	}
        });
    }

	if(img_data.length!=0){
		$("body").addClass("loading");
		$.ajax({
			url: baseurl+'/admin/notaries/transactionNotarySignature',
			data: {"_token": $('#token').val(),"transaction_id":transaction_id,"property_id":property_id,"img_data":img_data},
			type: 'post',
			success: function (data) {

				if(data=='pass'){
					window.location =  baseurl+'/workflow/application/'+transaction_id;
				}else{
					window.location =  baseurl+'/admin/notaries/acknowledge';
				}
			}
		});
	}
});



/// Trustee Signatures JS
var key = '';

$(".trustee_modal").on('click',function(e){
	var row        = $(this).closest('tr');
	key            = $('td', row).eq(3).text();
	customer_name  = $('td', row).eq(0).text();
	$("#customer_name").text(customer_name);
});



$(".trustee-sign-save").click(function(e){
	html2canvas([document.getElementById('trustee-signature-pad')], {
		onrendered: function (canvas) {
			var canvas_img_data = canvas.toDataURL('image/png');
			trustee_img         = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
			$("#trustee_signatures_"+key).val(trustee_img);
			$("#signature_check_"+key).css('display','block');
			document.getElementById("trustee_clear").click();
		}
	});
});