$(document).ready(function () { 
    var select =  $("#notary-modal").data('select');     
    var modal = $('#notary-modal').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax"      : $("#baseurl").val()+'/admin/api-get-notaries', 
    "columns": [
            { data: "id" },
            { data: "notary_name"}, 
            {data: "first_name",render: function ( data, type, full ) {
                var first_name  = (full["first_name"]) ? full["first_name"]: "";
                var second_name = (full["second_name"]) ? full["second_name"]: "";
                var third_name  = (full["third_name"]) ? full["third_name"]: "";
                var fourth_name = (full["fourth_name"]) ? full["fourth_name"]: "";
                var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

                return name }},       
            { data: "contact_number" },
            { data: "notary_number" },           
            { data: "notary_expiry_date" }, 
            { data : null, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>", "searchable" : false }                        
            ],            
        colReorder: true
    });

    $('#notary-modal').on( 'click', 'button', function () {
        
        var data = modal.row( $(this).parents('tr') ).data();
        var first_name  = (data.first_name) ? data.first_name: "";
        var second_name = (data.second_name) ? data.second_name: "";
        var third_name  = (data.third_name) ? data.third_name: "";
        var fourth_name = (data.fourth_name) ? data.fourth_name: "";
        $('#selected-notary').val(first_name+" "+second_name+" "+third_name+" "+fourth_name); 
        $("#notary_id").val(data.id); 
        $('#notary-modal-div').modal('hide');
    } );    
} );