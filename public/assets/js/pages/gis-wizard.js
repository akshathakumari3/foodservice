"use strict";
$(document).ready(function () {
    
    //declare global variables required for wizard 
    var selectedCustomers = [];
    var selectedAgent     = '';
    // initializations
    
    $(".wrapper").addClass("hide_menu");
    
    // manage wizard 
    var form = $("#coform");    
    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            
            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            
            // dont change the order of the checks
            // No Property Selected
            if (newIndex === 1 && !$("#property_id").val()) {
                $("#wizard-error").html("Select Property").show();
                return false;
            }
            
            // No Property Selected
            if (newIndex === 2 && selectedCustomers.length <= 0) {
                $("#wizard-error").html("Select Buyers").show();
                return false;
            }

            if (newIndex === 2 && selectedCustomers.length > 3) {
                $("#wizard-error").html("Please Add 3 buyers Only").show();
                return false;
            }
            
            // Missing Sale Information
            if (newIndex === 3 && (!$("#settlement_date").val() || !$("#settlement_amount").val())) {
                $("#wizard-error").html("Provide Transaction Details").show();
                return false;
            }      
            
            if (newIndex === 3 && !$("#notary_id").val() ) {
                $("#wizard-error").html("Select Notary").show();
                return false;
            }    
            
            if(newIndex === 2){
                $("#wizard-error").html("").hide();
            }
            if (newIndex === 3) {
                $("#wizard-error").hide();
                var wizardSection = "wizard-summary-view";
                var nextView = "admin.transactions.summary-partial";

                processNextStep(form, nextView, wizardSection);                             
            }
            
            if (newIndex == 4) {
                var wizardSection = "wizard-invoice-view";
                var nextView = "admin.invoice.invoice-items";

                processNextStep(form, nextView, wizardSection);              
            }
            
            // if (newIndex === 5 && !$("#d_transaction_id").val() ) {
            //     $("#wizard-error").html("Payment Missing").show();
            //     return false;
            // }      
            
            // if (newIndex === 5 && $("#d_transaction_id").val() ) {
            //     var wizardSection = "final-summary-view";
            //     var nextView = "admin.transactions.summary-partial";

            //     processNextStep(form, nextView, wizardSection) 
            // }              
            
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var paymentReference = $("#payment_reference_number").val();
            var transactionId = $("#d_transaction_id").val();
            // if (!paymentReference) {
            //     $("#wizard-error").html("Payment Missing").show();
            //     return false;
            // }
            var formData = form.serialize();
            var url = "../app/finish-application-wizard";
            $.post(url,  formData, 
                function (data) {
                    $("#gis-loader").css("display", "block");
                    $("#wizard-error").html("").hide();
                    var baseurl = $("#baseurl").val();
                    window.location = baseurl+"/workflow/initiate/" + data;
                    return false;
                }).fail(function(){
                    console.log("error");
            });  
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        }
    });   
    
    //datatable declaraion 
    var customerDataTable = $('#selected-customers-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Customers are selected"
           }, 
           
    });
    var agentDataTable = $('#selected-agent-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Agent are selected"
           }, 
           
    }); 
       
    
    var notaryDataTable = $('#selected-notary-table').DataTable({
        "bFilter" : false,
        "bPaginate" : false,
        "language": {
             "emptyTable": "No Notary is selected"
           }            
    });     
    
    //modals on close events 
    $('#confirmation-message-modal .confirm').click(function () {
        var id = $("#property_id").val();
        if(id){
            $.ajax({
                type: 'GET', 
                url : "property-main-partial/"  + id, 
                success : function (data) {
                    $("#property-details").html(data);
                    $("#wizard-error").html("").hide();
                }
            });   
        }         
     });    
     
    $('#property-modal-div').on('hidden.bs.modal', function () {
        var id = $("#property_id").val();
        if(id){
            $.ajax({
                type: 'GET', 
                url : "property-main-partial/"  + id, 
                success : function (data) {
                    $("#property-details").html(data);
                    $("#wizard-error").html("").hide();
                }
            }); 
        }           
     });    
     
    $('#customer-modal-div').on('hidden.bs.modal', function () {
        var id = $("#customer_id").val(); // customer id is a hidden field in the customers.modal-serach 
        var inArray = jQuery.inArray(eval(id), selectedCustomers);
        //console.log("Adding Customer " + id);
        if (inArray >= 0) {
            $('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p> Customer already selected </p>");  
        } else if(id) {
            $.ajax({
                type: 'GET', 
                url : "api-get-customer/"  + id, 
                success : function (data) {
                    $("#wizard-error").html("").hide();
                    var first_name  = (data.first_name) ? data.first_name: "";
                    var second_name = (data.second_name) ? data.second_name: "";
                    var third_name  = (data.third_name) ? data.third_name: "";
                    var fourth_name = (data.fourth_name) ? data.fourth_name: "";
                    var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

                    customerDataTable.row.add( [
                        data.id,
                        customer_name,
                        data.email,
                        data.mobile_1,
                        data.gender,
                        "<input type='radio' name='primary_owner' id='primary_owner' value=" + data.id + " checked>",
                        "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                    ] ).draw( false );   
                    selectedCustomers.push(data.id);
                    $('#selected_customers').val(selectedCustomers.join(", ")); //store array
                    //console.log("Selected Customer List " + $('#selected_customers').val());
                }
            });  
        }          
     });  

    $('#agent-modal-div').on('hidden.bs.modal', function () {
        var id = $("#agent_id").val(); // customer id is a hidden field in the customers.modal-serach 
        //var inArray = jQuery.inArray(eval(id), selectedAgent);
        //console.log("Adding Customer " + id);
        var selectedAgent = $('#selected_agent').val();
        if (selectedAgent && id && (selectedAgent == id)) {
            $('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p> Agent already selected </p>");  
        } else {
            if(id){
                $.ajax({
                    type: 'GET', 
                    url : "api-get-customer/"  + id, 
                    success : function (data) {
                        $("#wizard-error").html("").hide();
                        agentDataTable.row().remove().draw();
                        $('#selected_agent').val("");

                        var first_name  = (data.first_name) ? data.first_name: "";
                        var second_name = (data.second_name) ? data.second_name: "";
                        var third_name  = (data.third_name) ? data.third_name: "";
                        var fourth_name = (data.fourth_name) ? data.fourth_name: "";
                        var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;
                        agentDataTable.row.add( [
                            data.id,
                            customer_name,
                            data.email,
                            data.mobile_1,
                            data.gender,
                            "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                        ] ).draw( false );   
                        //selectedAgent.push(data.id);
                        $('#selected_agent').val(data.id); //store array
                        //console.log("Selected Customer List " + $('#selected_agent').val());
                    }
                });  
            }
        }          
    });    
     
    $('#notary-modal-div').on('hidden.bs.modal', function () {
        var id = $("#notary_id").val();
        $.ajax({
            type: 'GET', 
            url : "api-get-notary/"  + id, 
            success : function (data) {
                $("#wizard-error").html("").hide();
                notaryDataTable.clear();
                notaryDataTable.row.add( [
                    data.id,
                    data.notary_name,
                    data.first_name + ' ' + data.second_name + ' ' + data.third_name + ' ' + data.fourth_name,
                    data.contact_number,
                    data.email,
                    data.address,
                    data.notary_number,
                    data.notary_expiry_date,
                    "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                ] ).draw( false );   
            }
        });            
     });   
     
    $('#selected-customers-table').on("click", "a", function(event){
        event.preventDefault();
        var selectedRow = customerDataTable.row($(this).parents('tr'));
        selectedCustomers.splice( $.inArray(selectedRow.data()[0], selectedCustomers), 1 );
        $('#selected_customers').val(selectedCustomers.join(", ")); //store array        
        selectedRow.remove().draw(false);
        //console.log("Removing Customer " + selectedRow.data()[0]);        
        //console.log("Selected Customer List " + selectedCustomers);    
    });  

    $('#selected-agent-table').on("click", "a", function(event){
        event.preventDefault();
        var selectedRow = agentDataTable.row($(this).parents('tr'));
        $('#selected_agent').val("");
        selectedRow.remove().draw(false);
        $("#agent_id").val("");

        //console.log("Removing Customer " + selectedRow.data()[0]);        
        //console.log("Selected Customer List " + selectedCustomers);    
    });  


    $('#selected-notary-table').on("click", "a", function(event){
        event.preventDefault();
        var selectedRow = notaryDataTable.row($(this).parents('tr'));
        $('#notary_id').val("");
        selectedRow.remove().draw(false);
    });  
    
    // mapping on body because the invoice modal is dynamically included in the wizard
    $('body').on('click', 'a.invoice-print', function(event) {
         var url = "invoice/gis-invoice/"  + $("#d_transaction_id").val();
        $.get(url,
          function (data) {
              $("#wizard-error").html("").hide();
              $("#show-full-invoice-modal").html(data); // the section in the process blade to hold the invoice
              $("#full-invoice-modal-div").show(); // the actual model to show
              return false;
          }).fail(function(){
              console.log("error");
          });      
          event.preventDefault();
    });    
    
    $('body').on('click', 'a.payment-modal-button', function() {
         $("#transcation_id").val($("#d_transaction_id").val());
         $("#invoice_amount").val($("#d_invoice_amount").val());
         $("#payment_amount").val($("#d_invoice_amount").val());
         $(".payment-modal-div").show();
    });      
    
    $("body").on("click", "#btn-show-invoice-close", function() {
       $("#full-invoice-modal-div").hide();
    });
});

function processNextStep(form, nextView, wizardSection, dataModalDiv) {
    var formData = form.serialize();
    formData += "&next_view=" + nextView;
    var url = "../app/application-wizard";
    $.post(url,  formData, 
        function (data) {
            $("#wizard-error").html("").hide();
            $("#"  + wizardSection).html(data);
            //$("#" + dataModalDiv).show();
            return false;
        }).fail(function(){
            console.log("error");
    });      
}