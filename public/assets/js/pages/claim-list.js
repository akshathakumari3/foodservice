$(document).ready(function () { 

	
	var url    = $("#baseurl").val()+'/app/claim/list-datatable';
	var claim_table  = $('#claim-table').DataTable({
	    "responsive": true,
	    "processing": true,
	    "serverSide": true,
	    "order": [[ 0, "desc" ]],
	    //"ajax": url,
         'ajax': {
           'url': url,
           'data': function(data){
              // Read values
              var claimstatus = $('#searchByStatus').val();
              // // Append to data
              data.searchByStatus = claimstatus;
           }
        }, 
	    "columns": [
            {data : "id",
                "visible": false,
                "searchable": false
            },
            { data: "claim_reference_no" ,render: function ( data, type, full ) {
                return  "<a href="+$("#baseurl").val()+'/app/claim/'+full["id"]+">"+data+"</a>"; 
            }},
            { data: "application_date", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "customer.first_name",name: "customer.first_name" ,render: function ( data, type, full ) {
                if(full["customer"] != null){
                    return  "<a class='text-upper' href="+$("#baseurl").val()+'/app/customers/'+full["customer"]['id']+">"+data+"</a>"; 
                }else{
                  return "-";
                }
            }},
            { data: "total_amount", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            // { data: "claim_amount" },

            { data: "claim_amount",render: function ( data, type, full ) {
                if(data) {
                    return parseFloat(data).toFixed(2); 
                } else {
                    return "0.00"; 
                }
                
            }},
            { data: "status", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data : "",render: function ( data, type, full ){
			   if(full["status"]=="Rejected" || full["status"]=="Open"){
                return  "<a href="+$("#baseurl").val()+'/app/claim/'+full["id"]+"/edit><i class='glyphicon glyphicon-edit user-edit' title='edit customer'></i></a>";
				}else{
					return "<a id='editAlert' data-id='"+full["status"]+"'><i class='glyphicon glyphicon-edit user-edit' title='edit customer'></i></a>";;
				}
				
            }}
        ], 
        "columnDefs": [{ targets: [4,5], className: 'text-right' }, { targets: [7], className: 'text-center', "orderable": false }],    
        // 'columnDefs'        : [         
        //         { 
        //             'searchable'    : false, 
        //             'targets'       : [0,1,3,4] 
        //         },
        //     ],      
        colReorder: true,
	});

    $('#searchByStatus').change(function(){
        claim_table.draw();
    });

    var select  =  $("#select").data('select');
    var baseurl = $("#baseurl").val();
    var url     =baseurl+'/admin/api-get-addons';
    var modal   = $('#client-addon-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": url, 
    "columns": [
            {data : "id",
                "visible": false,
                "searchable": false
            },
             {data: "addon.service_name",render: function ( data, type, full ) {
                var types        = full["addon.service_name"] ;
                if(types){
                    return types;
                    return  "<a href="+baseurl+'/app/client/'+full["add_on_id"]+">"+types+"</a>"; 
                }else{
                   return '-'; 
                }
            }},
            { data: "line_of_business.name",orderable: false, render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "product.name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }}, 
            { data: "premium_type", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data : "",orderable: false,render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/clientMember/addon-view/'+full['id']+" class='two-btns' > <i class='fa fa-eye user-eye' title='View'></i></a><a id='addon-delete' data-id="+full["add_on_id"]+" data-product-id="+full["product_id"]+" ><i class='fa fa-trash user-delete' title='delete'></i></a>"; 
            }}
            ],    
            "columnDefs": [{ targets: [5], className: 'text-center' }],         

        colReorder: true
    });
});
$("body").on("click","#editAlert",function(e){
	var id      = $(this).data("id");
	if(id !='' && id !=null){
		var status_application=id;
	}else{
		var status_application="";
	}
		swal({
            title: "Your Application is  "+status_application+".Editing Application not allowed",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        }); 
});
$("body").on("click","#addon-delete",function(e){
    var id      = $(this).data("id");
     var product_id      = $(this).data("product-id");
   
    var token   = $("meta[name='csrf-token']").attr("content");
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/clientMember/delete/'+id;
    $.ajax({
        url : url, 
        type: 'DELETE',
        data: {
            _token: token,
            id: id,
            product_id:product_id
        },
        success: function (response){
            window.location=window.location;
        }
    });
});