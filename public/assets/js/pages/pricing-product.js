 $(document).ready(function () {

    function formatValue (value) {
        if (!value.id) { return value.text; }
        var $value = $(
          '<span><img src="'+assets_url+'/insurance/images/product/' + value.text.toLowerCase() + '.png" class="img-select-icon" width="20px"/> ' + value.text + '</span>'
        );
        return $value;
      };

    var base_url = $("#baseurl").val();
    $.fn.select2.defaults.set("theme","bootstrap");
    $('select[name="line_of_business"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/policy/get-line-of-business',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
                }
                return query;
            }
        },
        templateResult: formatValue,
        templateSelection: formatValue,
    }).on('select2:select', function (e) {  
        var product = $("<option selected='selected'></option>").val("").text("");
        $("#product_name_id").append(product);
    });

    $('select[name="product_name_id"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/pricing/productDetails',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                var query = {
                    search     : params.term,
                    type       : 'public',
                    business   : $("#line_of_business").val()
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {  
        var variant = $("<option selected='selected'></option>").val("").text("");
        $("#product_variant").append(variant);
        $("#sum_insured").val('');
    });
	
	$('select[name="product_option"]').select2({
      placeholder: 'Select a Value',
      allowClear: true,
      ajax: {
         url: base_url+'/app/get-product-option-details',
         dataType: 'json',
         method:'get',
         data:function (params) {
            var query = {
               search  : params.term,
               type    : 'public',
               product_id: $("#product_name_id").val()
            }
           return query;
         }
      }
   }).on('select2:select', function (e) {  
      //checkProductVarient();
    })

    $('select[name="product_variant"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/product-variant/get-product-variant',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                var query = {
                    search     : params.term,
                    type       : 'public',
                    business   : $("#line_of_business").val(),
                    product_id : $("#product_name_id").val(),
					product_option:$("#product_option").val()
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {      
        var data = e.params.data;
        $("#sum_insured").val(data.sum_insured);
        $("#insured_amount").text(data.sum_insured);
    });
});