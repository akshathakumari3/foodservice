$('#save_claim').click(function () {
    var status = 'Open';
    makeClaim(status);
    
});  

$('#edit_save_claim').click(function () {
    var status = 'Open';
    editMakeClaim(status);
    
}); 


$('#submit_claim').click(function () {
    var status = 'Settled';
    makeClaim(status);
}); 

function makeClaim(status) {

	var baseurl              = $("#baseurl").val();
	var policy_no            = $("#policy_no").val();
	var claim_select         = $("input[name='claim_select']:checked"). val();
	var day_hospital_select  = $("input[name='day_hospital_select']:checked"). val();
	var service_provider_2   = $("#service_hospital_sepcialist").val();
	var service_provider     = $("#service_provider").val();
	var total_fee            = $("#total_fee").val();
	var initial_diagnosis    = $("#initial_diagnosis").val();
	var documents_count      = parseInt($("#claim_documents_count").val());
	var services_count       = parseInt($("#services_count").val());
	var claimed_photo        = $("#claimed_photo").val();
	var bed_type             = $("#bed_type").val();
	var verify_member_id  = $("#verify_member_id").val();

	var total_claim_amount   = 0;
	if(!policy_no){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Policy Number is Requried. </p>");
        return false;
	}

	if(!claim_select){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Select Members. </p>");
        return false;
	}
	if(verify_member_id==""){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please verify Member </p>");
        return false;
	}

	// if(!claimed_photo){
	// 	$('#warning-message-modal').modal('show');
 //        $("#warning-message-modal .modalMessage").html("<p> Please Verify Member Photo. </p>");
 //        return false;
	// }

	if(!day_hospital_select){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Select Out Patient or In Patient. </p>");
        return false;
	}

	if(!services_count){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Add Services. </p>");
        return false;
	}
	

	/*if(!documents_count){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Upload Documents. </p>");
        return false;
	}*/

	if(day_hospital_select == "Day Visit"){

		if(service_provider == 'null' || !service_provider){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Select Specialist. </p>");
	        return false;
        }
        total_claim_amount = $("#day_total_amount").val();
	}

	if(day_hospital_select == "Hospitalization"){

		if(service_provider_2 == 'null' || !service_provider_2){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Select Specialist. </p>");
	        return false;
        }

        if(!initial_diagnosis){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Enter Diagnosis. </p>");
	        return false;
        }
        /*if(!bed_type){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Select Bed Type. </p>");
	        return false;
        }*/

        total_claim_amount = $("#day_total_amount").val();
	}
	var balance_sum_insured = $("#balance_sum_insured").val();

	if(parseFloat(total_claim_amount) > parseFloat(balance_sum_insured) ){
		swal({
            title: "Available Claim Amount $"+balance_sum_insured+" Only",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        }); 

        return false;
	}

	var form     = $("#create_claim"); 
    var formData = form.serialize();
    formData     += "&status=" + status;
    var url      = baseurl+"/app/claim/make-claim";
    $.post(url,  formData, 
        function (data) {
        	if(data.result == "pass"){
        		$("#gis-loader").css("display", "block");
        		window.location = baseurl+'/app/claim/'+data.claim_id;
        		var location    = baseurl+'/app/claim/print-claim-receipt/'+data.claim_id;
  				window.open(location, '_blank', 'location=yes,height=800,width=900,scrollbars=yes,status=yes');
        		console.log(location);
        	}else{
        		$('#warning-message-modal').modal('show');
            	$("#warning-message-modal .modalMessage").html("<p> Claim Apply Error. </p>");
        	}
        }).fail(function(){
            console.log("error");
    });      
}



function editMakeClaim(status) {

	var baseurl              = $("#baseurl").val();
	var policy_no            = $("#policy_no").val();
	var claim_select         = $("input[name='claim_select']:checked"). val();
	var day_hospital_select  = $("input[name='day_hospital_select']:checked"). val();
	var service_provider_2   = $("#service_hospital_sepcialist").val();
	var service_provider     = $("#service_provider").val();
	var total_fee            = $("#total_fee").val();
	var initial_diagnosis    = $("#initial_diagnosis").val();
	var documents_count      = parseInt($("#claim_documents_count").val());
	var services_count       = parseInt($("#services_count").val());
	var claimed_photo        = $("#claimed_photo").val();
	var bed_type             = $("#bed_type").val();
	var total_claim_amount   = 0;
		var verify_member_id  = $("#verify_member_id").val();
	if(!policy_no){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Policy Number is Requried. </p>");
        return false;
	}

	if(!claim_select){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Select Members. </p>");
        return false;
	}
	
	if(verify_member_id==""){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please verify Member. </p>");
        return false;
	}

	// if(!claimed_photo){
	// 	$('#warning-message-modal').modal('show');
 //        $("#warning-message-modal .modalMessage").html("<p> Please Verify Member Photo. </p>");
 //        return false;
	// }

	if(!day_hospital_select){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Select Out Patient or In Patient. </p>");
        return false;
	}

	if(!services_count){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Add Services. </p>");
        return false;
	}
	

	/*if(!documents_count){
		$('#warning-message-modal').modal('show');
        $("#warning-message-modal .modalMessage").html("<p> Please Upload Documents. </p>");
        return false;
	}*/

	if(day_hospital_select == "Day Visit"){

		if(service_provider == 'null' || !service_provider){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Select Specialist. </p>");
	        return false;
        }
        total_claim_amount = $("#day_total_amount").val();
	}

	if(day_hospital_select == "Hospitalization"){

		if(service_provider_2 == 'null' || !service_provider_2){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Select Specialist. </p>");
	        return false;
        }

        if(!initial_diagnosis){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Enter Diagnosis. </p>");
	        return false;
        }
        /*if(!bed_type){

			$('#warning-message-modal').modal('show');
	        $("#warning-message-modal .modalMessage").html("<p> Please Select Bed Type. </p>");
	        return false;
        }*/

        total_claim_amount = $("#day_total_amount").val();
	}
	var balance_sum_insured = $("#balance_sum_insured").val();

	if(parseFloat(total_claim_amount) > parseFloat(balance_sum_insured) ){
		swal({
            title: "Available Claim Amount $"+balance_sum_insured+" Only",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#418BCA",
            confirmButtonText: "Ok",
        }); 

        return false;
	}

	var form     = $("#create_claim"); 
    var formData = form.serialize();
    formData     += "&status=" + status;
    var url      = baseurl+"/app/claim/edit-make-claim";
    $.post(url,  formData, 
        function (data) {
        	if(data.result == "pass"){
        		$("#gis-loader").css("display", "block");
        		window.location = baseurl+'/app/claim/'+data.claim_id;
        		var location    = baseurl+'/app/claim/print-claim-receipt/'+data.claim_id;
  				window.open(location, '_blank', 'location=yes,height=800,width=900,scrollbars=yes,status=yes');
        		console.log(location);
        	}else{
        		$('#warning-message-modal').modal('show');
            	$("#warning-message-modal .modalMessage").html("<p> Claim Apply Error. </p>");
        	}
        }).fail(function(){
            console.log("error");
    });      
}