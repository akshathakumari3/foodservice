$(document).ready(function () {      
    
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/product-variant/data-table';
    var modal   = $('#product-variants-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": false,
    "columnDefs": [
        { "orderable": false,"targets": [ 1, 5,6, 7 ]},
        { "searchable": false, "targets": [ 1, 5,6, 7 ]},
        {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
    ],
    "order": [[ 0, "desc" ]],
    "ajax": url, 
    "columns": [
            {data: 'DT_Row_Index', name: 'id'},
            {data: "lineOfBusiness", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: "product", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: "productOption", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data : "variant",render: function ( data, type, full ){
                if(data != "") {
                    return  "<a href="+baseurl+'/app/product-variant/'+full["id"]+" class='blueRow'>"+data+"</a>";
                } else {
                    return "-";
                }
            }},
            {data : "",render: function ( data, type, full ){
                return  "<a href="+baseurl+'/app/product-variant/inclusions/'+full["id"]+"><i class='glyphicon glyphicon-eye-open user-eye' title='View'></i></a>"; 
            }},
            {data : "",render: function ( data, type, full ){
                return  "<a href="+baseurl+'/app/product-variant/exclusions/'+full["id"]+"><i class='glyphicon glyphicon-eye-open user-eye' title='View'></i></a>"; 
            }},
            {data : "",render: function ( data, type, full ){
                return  "<a href="+baseurl+'/app/product-variant/'+full["id"]+"/edit><i class='glyphicon glyphicon-edit user-edit two-btns' title='edit'></i></a><a id='product-variant-delete' class='product-variant-delete' data-id="+full["id"]+"><i class='glyphicon glyphicon-trash user-delete' title='delete'></i></a>"; 
            }}

			],  
            "columnDefs": [{ targets: [5,6,7], className: 'text-center', orderable:false }],          
        colReorder: true
    });
});

$(document).on('click', '.product-variant-delete', function(){
    var id = $(this).data('id');
    Swal.fire(swal_fire).then((result) => {
        if (result.value) {
            $.ajax({
                type: "DELETE",
                url : $("#baseurl").val()+"/app/product-variant/"+id,
                success : function (data) {
                    //console.log('success');
                    // console.log(data);
                    window.location.reload(true);
                    
                }
            });
        }
    });
});