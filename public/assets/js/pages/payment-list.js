$(document).ready(function () { 
    
    var url    = $("#baseurl").val()+'/app/payment/list-datatable';
    var modal  = $('#payment-table').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": false,
        "order": [[ 0, "desc" ]],
        //"ajax": url,
        "ajax": {
           'url': url,
           'data': function(data){
              // Read values
              var invoiceStatus = $('#invoice_status').val();

              // // Append to data
              data.invoice_status = invoiceStatus;
           }
        }, 
        "columns": [
            // {data : "id",
            //     "visible": false,
            //     "searchable": false
            // },
            { data: "payment_reference_id",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/payment/'+full["id"]+" class='blueRow'>"+data+"</a>"; 
            }},
            { data: "due_date", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "payment_date", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "reference_no", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "invoice.reference_no",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/invoice/'+full.invoice.id+" class='blueRow'>"+data+"</a>"; 
            }},
            { data: "invoice.policy_id", name:"invoice.policy_id",render: function ( data, type, full ){
				
				if(full.invoice.policy_id!=null){
					
					var str1 = full.invoice.policy_id;
					var str2 = "GP";
					
					if(str1.indexOf(str2) != -1){
						var text=full.invoice.policy_id;
						var policy_id=text.replace('GP', "");
						var url='/app/clientPolicies/getClientPolicyDetails/'+policy_id;
					}else{
                        if(str1.indexOf("PO-") != -1){
                            var text=full.invoice.policy_id;
                            var policy_id=text.replace('PO-', "");
                        }else {
                            var policy_id=full.invoice.policy_id;
                        }
						var url='/app/policy/'+policy_id;
					}
                return  "<a href="+$("#baseurl").val()+ url +" class='blueRow'>"+data+"</a>"; 
				}else{
				return "-";
				}
            }},
            { data: "customer",render: function ( data, type, full ){
                if(full.invoice.client_id){
					return  "<a href="+$("#baseurl").val()+'/app/client/'+full.invoice.client_id+" class='blueRow'>"+data+"</a>";
				}
				if(full.invoice.customer_id){
					return  "<a href="+$("#baseurl").val()+'/app/customers/'+full.invoice.customer_id+" class='blueRow'>"+data+"</a>";
					
				}
            }},
            { data: "paid_amount", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "status", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            { data: "action",render: function ( data, type, full ){
                return  "<a href="+$("#baseurl").val()+'/app/payment/'+full["id"]+" class='blueRow'><i class='fa fa-eye'></i></a>"; 
            }},
        ], 
        "columnDefs": [{ targets: [7], className: 'text-right' }, { targets: [9], className: 'text-center', orderable:false }],    
        // 'columnDefs'        : [         
        //         { 
        //             'searchable'    : false, 
        //             'targets'       : [0,1,3,4] 
        //         },
        //     ],      
        colReorder: true,
    });

    $('#invoice_status').change(function(){
        modal.draw();
    });
});