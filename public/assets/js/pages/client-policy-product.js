$(document).ready(function () {

    function formatValue (value) {
        if (!value.id) { return value.text; }
        var $value = $(
          '<span><img src="'+assets_url+'/insurance/images/product/' + value.text.toLowerCase() + '.png" class="img-select-icon" width="20px"/> ' + value.text + '</span>'
        );
        return $value;
      };

    var base_url = $("#baseurl").val();
    $.fn.select2.defaults.set("theme","bootstrap");
    $('select[name="line_of_business"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/policy/get-line-of-business',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                
                var query = {
                    search            : params.term,
                    type              : 'public',
                }
                return query;
            }
        },
        templateResult: formatValue,
        templateSelection: formatValue,
    }).on('select2:select', function (e) {  
        var referenceNameOption = $("<option selected='selected'></option>").val("").text("");
        $("#product_name").append(referenceNameOption);
        $("#product_variant").append(referenceNameOption);
        $("#sum_insured").val('');
        $("#product_details").val('');
    });

    $('select[name="product_name"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/get-product-details',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                var query = {
                    search     : params.term,
                    policy_type: $("#sme_policy").val(),
                    type       : 'public',
                    business   : $("#line_of_business").val()
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {  
        var referenceNameOption = $("<option selected='selected'></option>").val("").text("");
        $("#product_variant").append(referenceNameOption);
        $("#sum_insured").val('');
        $("#product_details").val('');
    });
	
	$('select[name="product_option"]').select2({
      placeholder: 'Select a Value',
      allowClear: true,
      ajax: {
         url: base_url+'/app/get-product-option-details',
         dataType: 'json',
         method:'get',
         data:function (params) {
            var query = {
               search  : params.term,
               type    : 'public',
               product_id: $("#product_name").val()
            }
           return query;
         }
      }
   }).on('select2:select', function (e) {  
      //checkProductVarient();
    })
	
	
    $('select[name="product_variant"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/product-variant/get-product-variant',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params) {
                var query = {
                    search     : params.term,
                    type       : 'public',
                    business   : $("#line_of_business").val(),
                    product_id : $("#product_name").val(),
					product_option:$("#product_option").val()
                }
                return query;
            }
        }
    }).on('select2:select', function (e) {      
        var data = e.params.data;
        $("#sum_insured").val(data.sum_insured);
        if(data.id){
            $("#product_details").val(data.id);
        }else{
            $("#product_details").val('');
        }
    });

    $('select[name="add_ons[]"]').select2({
        placeholder: 'Please Select',
        allowClear: true,
        ajax: {
            url         : base_url+'/app/product-variant/get-add-ons',
            dataType    : 'json',
            method      : 'get',
            quietMillis : 1000,
            data:function (params){
                var query = {
                    search     : params.term,
                    type       : 'public',
                    product_id : $("#product_name").val()
                }
                return query;
            }
        }
    });

    $("body").on("click",".addon-add",function(e){
        var id      = $('.addons-multiple').val();
        var baseurl = $("#baseurl").val();
        var rowCount = $('#selected-insurers-table tbody tr').length;
		var policy_product_detail=$('#policy_product_detail').val();
        $.ajax({
            type: "POST",
            url: base_url+'/app/product-variant/get-add-ons-variant',
            data: {id:id,"product_id":$("#product_name").val(),policy_product_detail:policy_product_detail},
            success: function(res){
                $('.selected_policy_plan_details').html("");
                
                var count = res.count[0];
  
                var names = res.name;
                var suminsure = res.sum_insured;
                var premiums = res.premium;

                for(var i = 0; i < count; i++) {
                      var name = res.count[1][i];  
                      var increse = i+1;
                      var txt1 = "name"+increse;
                      var txt2 = "sum"+increse;
                      var txt3 = "pre[]"; //increse
                      var txt4 = "pre"+increse;
					  var txt5 = "premium"+increse;
                      if(name){  
                          var data = '<div class="row" id="'+name+'_remove">'+
                                '<div class="col-md-3 form-group">'+
                                    '<label for="'+name+'">'+name+' Option</label>'+
                                    '<select onchange="changeAddonsLimit(this,\''+txt2+'\',\''+txt5+'\',\''+name+'\')" class="form-control '+name+'n'+'" name="'+name+'_option" data-cls="'+txt2+'" data-id="'+name+'" id="'+txt4+'" width="100%">'+
                                    '</select>'+
									
                                '</div>'+
                                '<div class="col-md-3 form-group">'+
                                    '<label for="'+name+' Limit">'+name+' Limit</label>'+
                                    '<div class="append-input input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-currency-usd"></i></span></div><input type="text" name="'+name+'_limit" class="form-control '+txt2+' '+name+'s'+'" id="'+txt2+'" value="" readonly=""></div>'+
                                '</div>'+
                                '<div class="col-md-3 form-group">'+
                                    '<label for="PremiumPerMember">'+name+' Premium </label>'+
                                    '<div class="append-input input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-currency-usd"></i></span></div><input type="text" name="pre[]" class="form-control '+txt4+' '+name+'p'+'" id="'+txt5+'" value="" readonly=""></div>'+
                                '</div>'+
                                '<div class="col-md-3 form-group">'+
                                '<label for="">&nbsp;</label>'+
                                '<div class="form-group change">'+
                                '<a class="btn btn-danger btn_remove" onclick="removeAddons(\''+name+'\')">- Remove</a></div>'+
                                '</div>'+
                            '</div>';
                            $('.selected_policy_plan_details').append(data);

                            $('.btn_remove').click(function(){
                                var id = $(this).attr('id');
                                $(''+'.'+id+'').remove();
                            }) 
                       
                            $('select[name="'+txt1+'"]').select2({
                                placeholder: 'Please Select',
                                allowClear: true,
                                ajax: {
                                    url         : base_url+'/app/product-variant/get-policy-premium',
                                    dataType    : 'json',
                                    method      : 'get',
                                    quietMillis : 1000,
                                    data:function (params) {
                                        var query = {
                                            search     : params.term,
                                            type       : 'public',
                                            id_name  : $(this).attr('id'),
                                            cls_name  : $(this).attr('data-cls'),
                                            data_id_name  : $(this).attr('data-id')
                                        }
                                        return query;
                                    }
                                }
                            }).on('select2:select', function(e){ 
                                var data = e.params.data;
                                $(''+'.'+data.sum_name+'').val(data.sum_value);
                                if(data.name == 'Maternity'){
                                    $(''+'.'+data.pre_name+'').val(data.pre_value);
                                }else{
                                    $(''+'.'+data.pre_name+'').val(rowCount*data.pre_value);
                                }
                            }); 
                        }  
                    }

                $.each(names, function(s, val){
                    for(var j=0; j<val.length; j++) {
                        $(''+'.'+s+'n'+'').append($("<option></option>").attr("value", val[j].id).text(val[j].text)); 
                    }
                });

                $.each(suminsure, function(s, val){ 
                    $(''+'.'+s+'s'+'').val(parseFloat(val[0].text).toFixed(2)); 
                });

                $.each(premiums, function(s, val){ 
                    if(s == 'Maternity'){
                        $(''+'.'+s+'p'+'').val(parseFloat(val[0].text).toFixed(2)); 
                    }else{
                        $(''+'.'+s+'p'+'').val(parseFloat(rowCount*val[0].text).toFixed(2));
                    }
                });
            }
        });
    });
});


  
		
     function changeAddonsLimit(add_ons_id,id,premium,name){
        var preminum_id = add_ons_id.value;
		var rowCount = $('#selected-insurers-table tbody tr').length;
        $.ajax({
            type: "get",
            url: $("#baseurl").val()+'/app/product-variant/change-addons-limit',
            data: {preminum_id:preminum_id},
            success: function(data){
                console.log(data);
				document.getElementById(id).value = (parseFloat(data.results).toFixed(2));
				if(name=='Maternity'){
					document.getElementById(premium).value = (parseFloat(data.premium).toFixed(2));
				}else{
					document.getElementById(premium).value = (parseFloat(rowCount*data.premium).toFixed(2));
					
				}
                
            }
        });
    }

    function test(){
        $("body").on("click",".addon-add",function(e){
        var id      = $('.addons-multiple').val();
        var baseurl = $("#baseurl").val();

        var rowCount = $('#selected-insurers-table tbody tr').length;

        $.ajax({
            type: "POST",
            url: base_url+'/app/product-variant/get-add-ons-variant',
            data: {id:id},
            success: function(res){
                $('.selected_policy_plan_details').html("");
                
                var count = res.count[0];
  
                var names = res.name;
                var suminsure = res.sum_insured;
                var premiums = res.premium;

                for(var i = 0; i < count; i++) {
                      var name = res.count[1][i];  
                      var increse = i+1;
                      var txt1 = "name"+increse;
                      var txt2 = "sum"+increse;
                      var txt3 = "pre[]"; //increse
                      var txt4 = "pre"+increse;

                      if(name){  
                          var data = '<div class="row">'+
                            '<div class="col-md-3 form-group">'+
                                '<label for="'+name+'">'+name+' Premium</label>'+
                                '<select class="form-control '+name+'n'+'" name="'+txt1+'" width="100%">'+
                                '</select>'+
                            '</div>'+
                            '<div class="col-md-3 form-group">'+
                                '<label for="'+name+' Limit">'+name+' Limit</label>'+
                                '<select class="form-control '+name+'s'+' '+txt2+'" name="'+txt2+'" id="'+txt4+'" width="100%">'+
                                '</select>'+
                            '</div>'+
                            '<div class="col-md-3 form-group">'+
                                '<label for="PremiumPerMember">Premium Per '+name+'</label>'+
                                '<input type="text" name="'+txt3+'" class="form-control '+txt4+' '+name+'p'+'" value=""readonly="">'+
                            '</div>'+
                            '</div>';
                            $('.selected_policy_plan_details').append(data);
                       
                        $('select[name="'+txt2+'"]').select2({
                            placeholder: 'Please Select',
                            allowClear: true,
                            ajax: {
                                url         : base_url+'/app/product-variant/get-policy-premium',
                                dataType    : 'json',
                                method      : 'get',
                                quietMillis : 1000,
                                data:function (params) {
                                    var query = {
                                        search     : params.term,
                                        type       : 'public',
                                        id_name  : $(this).attr('id')
                                    }
                                    return query;
                                }
                            }
                        }).on('select2:select', function(e){ 
                            var data = e.params.data;
                            $(''+'.'+data.pre_name+'').val(rowCount*data.value);
                        }); 
                       }  
                }

                $.each(names, function(s, val){
                    for(var j=0; j<val.length; j++) {
                        $(''+'.'+s+'n'+'').append($("<option></option>").attr("value", val[j].id).text(val[j].text)); 
                    }
                });

                $.each(suminsure, function(s, val){ 
                    for(var j=0; j<val.length; j++) {
                        $(''+'.'+s+'s'+'').append($("<option></option>").attr("value", val[j].id).text(val[j].text)); 
                    }
                });

                $.each(premiums, function(s, val){ 
                    for(var j=0; j<val.length; j++) {
                        $(''+'.'+s+'p'+'').val(rowCount*val[j].text); 
                    }
                });
            }
        });
    });
}


