$(document).ready(function () {      
    var select                  =  $("#select").data('select');
    var no_sellers_are_selected = $("#select").data('no_sellers_are_selected');
    var seller_already_selected = $("#select").data('seller_already_selected');
    var url                     = $("#baseurl").val()+'/admin/api-get-customers';
    var modal  = $('#customer-modal').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": url, 
        "columns": [
                { data: "id" },
                {data: "first_name",render: function ( data, type, full ) {
                    var first_name  = (full["first_name"]) ? full["first_name"]: "";
                    var second_name = (full["second_name"]) ? full["second_name"]: "";
                    var third_name  = (full["third_name"]) ? full["third_name"]: "";
                    var fourth_name = (full["fourth_name"]) ? full["fourth_name"]: "";
                    var name        = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

                    return name }},       
                { data: "mobile_1" },
                { data: "gender" },           
                { data : undefined, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>", searchabe: false }                        
                ],            
            colReorder: true
    });

    var selectedSellers = [];
    $('#customer-modal').on( 'click', 'button', function () {
        var data        = modal.row( $(this).parents('tr') ).data();
        var id          = data.id; // customer id is a hidden field in the customers.modal-serach 
        var inArray     = jQuery.inArray(eval(id), selectedSellers);
        //console.log("Adding Customer " + id);
        if (inArray >= 0) {
            $('#warning-message-modal').modal('show');
            $("#warning-message-modal .modalMessage").html("<p> "+seller_already_selected+" </p>");  
        } else if(id) {
            $.ajax({
                type: 'GET', 
                url : $("#baseurl").val()+'/admin/api-get-customer/' + id, 
                success : function (data) {
                    $("#wizard-error").html("").hide();
                    var first_name  = (data.first_name) ? data.first_name: "";
                    var second_name = (data.second_name) ? data.second_name: "";
                    var third_name  = (data.third_name) ? data.third_name: "";
                    var fourth_name = (data.fourth_name) ? data.fourth_name: "";
                    var customer_name = first_name + " " + second_name + " " + third_name +  " " + fourth_name ;

                    sellersDataTable.row.add( [
                        data.id,
                        customer_name,
                        data.email,
                        data.mobile_1,
                        data.gender,
                        "<input type='radio' name='primary_owner' id='primary_owner' value=" + data.id + " checked>",
                        "<a href='#'><i class='fa fa-fw fa-trash-o'></i></a>" 
                    ] ).draw( false );   
                    selectedSellers.push(data.id);
                    $('#selected_sellers').val(selectedSellers.join(", ")); //store array
                    $('#customer-modal-div').modal('hide');
                    
                    $('#selected-sellers-section').show();
                    $('#seller_details').hide();
                }
            });  
        }  
    }); 

    //sellersDataTable declaration

    var sellersDataTable = $('#selected-sellers-table').DataTable({
    "bFilter" : false,
    "bPaginate" : false,
    "language": {
        "emptyTable": no_sellers_are_selected
       }, 
           
    });


    //remove seller 

    $('#selected-sellers-table').on("click", "a", function(event){
        event.preventDefault();
        var selectedRow = sellersDataTable.row($(this).parents('tr'));
        selectedSellers.splice( $.inArray(selectedRow.data()[0], selectedSellers), 1 );
        $('#selected_sellers').val(selectedSellers.join(", ")); //store array        
        selectedRow.remove().draw(false);

        var selected_sellers_table = $('#selected-sellers-table').DataTable();
 
        if ( ! selected_sellers_table.data().count() ) {
            $('#seller_details').show();
            $('#selected-sellers-section').hide();
        }
    });
});