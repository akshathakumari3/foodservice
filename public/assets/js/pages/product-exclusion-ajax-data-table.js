$(document).ready(function () {      
    
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/product-variant/exclustion/data-table';
    var modal   = $('#product-exclusion-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "columnDefs": [
        { "orderable": false,"targets": [ 0, 1, 3 ]},
        { "searchable": false, "targets": [ 0,3 ]},
    ],
    "order": [[ 0, "desc" ]],
    "ajax": {
         "url" : url, 
        "type" :"get",
        "data" : {
                "id" : $("#varient_id").val()
            },
        },
    "columns": [

            {data: 'DT_Row_Index', name: 'id'},
            {data: "service",name:"service.code"},
            {data: "description",name:"description"},
            {data : "",render: function ( data, type, full ){
                return  "<a class='product-exclusion-delete' data-id="+full["id"]+"><i class='glyphicon glyphicon-trash user-delete' title='Delete'></i></a>"; 
            }}
			],   
            "columnDefs": [{ targets: [3], className: 'text-center' }],         
        colReorder: true
    });
});

$(document).on('click', '.product-exclusion-delete', function(){
    var id = $(this).data('id');
    Swal.fire(swal_fire).then((result) => {
        if (result.value) {
            $.ajax({
                type: "DELETE",
                url : $("#baseurl").val()+"/app/product-variant/exclusion/"+id,
                success : function (data) {
                    //console.log('success');
                    // console.log(data);
                    window.location.reload(true);
                    
                }
            });
        }
    });
});