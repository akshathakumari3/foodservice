$(document).ready(function () { 
    var select         =  $("#select").data('select');
    $("#search-tax-invoice-properties").click(function(){
        var status     = ["Invoice Created","Tax Collection Started","Tax Invoice  Delivered","Tax Property Created"];
        var url        = $("#baseurl").val()+'/admin/taxInvoice/datatable';
        var tax_table  = $('#tax-table').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        // "ajax": url, 
        ajax: {
            url: url,
            dataType: "json",
            type: "GET",
            data: function(d){
                d.sub_district = $("#sub_district").val()
                d.street_name  = $("#street_name").val()
                d.door_number  = $("#door_number").val()
                d.status       =  status;
            }},
        "columns": [
                { data: "id"},
                { data: "door_number"},
                { data: "street_name"},
                { data: "sub_district"},
                { data: "size"},           
                { data : undefined, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>"+select+"</button>", searchabe: false }                        
                ],  
        initComplete: function(settings, json) {
            $("#propertyDetails-modal-div").modal('hide');
            $("#tax_collections_property").css('display','block');
        },

        "destroy": true          
            // colReorder: true
        });
    });

    $('#tax-table').on( 'click', 'button', function () {
        
        var table          = $('#tax-table').DataTable();
        var data           = table.row( $(this).parents('tr') ).data();
        var tax_invoice_id = (data.id) ? data.id: "";

        if(tax_invoice_id){
            getTaxInvoiceDetails(tax_invoice_id);
        }
    });  

    $('#tax_collection_payment_summary').on('click',function(){

        var base_url        = $("#baseurl").val();
        var tax_invoice_id  = $("#tax_invoice_id").val();
        var customer_id     = $("#existingSellerId").val();
        if(tax_invoice_id&&customer_id){
            window.location     = base_url+'/admin/taxCollection/payment/'+tax_invoice_id+'/'+customer_id;
        }
    }); 

    $("#search-tax-invoices").click(function(){
        var select_sub_district   = $("#select_sub_district").val();
        var select_street_or_grid = $("#select_street_or_grid").val();
        var street_name           = $("#street_name").val();
        var grid                  = $("#grid").val();
        if(!$("#sub_district").val()){
                swal({
                    title: select_sub_district,
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#418BCA",
                    confirmButtonText: "Ok",
                });
            return false;
        }
        if(!(street_name) && !(grid)){
                swal({
                    title: select_street_or_grid,
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#418BCA",
                    confirmButtonText: "Ok",
                });
            return false;
        }

        var url        = $("#baseurl").val()+'/admin/taxInvoice/datatable';
        var tax_table  = $('#tax-invoices-table').DataTable({
            "responsive": true,
            "processing": true,
            "serverSide": true,
            ajax: {
                url: url,
                dataType: "json",
                type: "GET",
                data: function(d){
                    d.sub_district = $("#sub_district").val()
                    d.street_name  = $("#street_name").val()
                    d.grid         = $("#grid").val()
                }},
            "columns": [
                    { data: "id"},
                    { data: "door_number"},
                    { data: "street_name"},
                    { data: "sub_district"},
                    { data: "postal_code"},           
                    { data :"status" },
                    { data :"grid" },
                    { data :"officer_name"},
                    { data :"",render: function (data, type, full) {
                        if(full.status == 'Survey Completed') {
                           return '<button name="generate_invoice" class="btn btn-primary leftbtnsearch" id="generate_invoice" onclick ="generateTaxInvoice('+full.id+')">Invoice Generate</button>';
                        }
                    },sDefaultContent: "",searchable:false}                     
                    ],  
            initComplete: function(settings, json) {
                $("#tax-invoices-list-data").css('display','block');
                var tax_table_length =$('#tax-invoices-table').DataTable().rows().data().toArray().length;
                if(tax_table_length>0){
                    $("#assign-tax-surveyor").show();
                    $("#assignee-users").show();
                }else{
                    $("#assign-tax-surveyor").hide();
                    $("#assignee-users").hide();
                }
            },

            "destroy": true          
                // colReorder: true
        });
        
    });

    $('#assign-tax-surveyor').on('click',function(){
        
        var tax_table             = $('#tax-invoices-table').DataTable().rows().data().toArray();
        var sub_district          = $("#sub_district").val();
        var street_name           = $("#street_name").val();
        var grid                  = $("#grid").val();
        var assignee              = $("#assignee").val();
        var tax_surveyor_assigned = $("#tax_surveyor_assigned").val();
        var select_assignee       = $("#select_assignee").val();
        var base_url              = $("#baseurl").val();

        if(tax_table.length>0 && assignee && sub_district) {

            $.ajax({
                type : "POST",
                url  : base_url+'/admin/taxInvoice/assign-tax-surveyor',
                data : {"sub_district": sub_district,"street_name":street_name,"grid":grid,"assignee":assignee},
                success : function (data) {
                   if(data == 'pass'){
                      var  title =  tax_surveyor_assigned;
                      var  type  = "success";
                            
                    }else{
                        var  title =  'Tax Surveyor Assigned Error';
                        var  type  = "error";
                    }

                    swal({
                        title: title,
                        text: "",
                        type: type,
                        showCancelButton: false,
                        confirmButtonColor: "#418BCA",
                        confirmButtonText: "Ok",
                    });
                }
            });
        } else{

            swal({
                title: select_assignee,
                text: "",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#418BCA",
                confirmButtonText: "Ok",
            });
        }
    });
});
var base_url  = $("#baseurl").val();
function generateTaxInvoice(id){
        $.ajax({
            type : "POST",
            url  : base_url+'/admin/taxInvoice/tax-invoice-generate/'+id,
            success : function (data) {
                $body = $("body");
                $body.addClass("loading");
                location.reload();
            }
        });
    }
