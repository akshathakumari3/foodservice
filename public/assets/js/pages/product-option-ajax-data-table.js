$(document).ready(function () {      
    
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/productOption/data-table';
    var modal   = $('#productOption-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "columnDefs": [
        { "searchable": false, "targets": 5,"orderable": false},
        { "searchable": false, "targets": 0},
    ],
    "order": [[ 0, "desc" ]],
    "ajax": url, 
    "columns": [

            {data: 'id', name: 'id', "orderable": false},
            {data: "lineOfBusiness.name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: "products.name", render: function ( data, type, full ) { 
                if(data != '') {
                    return data;
                } else {
                    return '-';
                }
            }},
            {data: "name",render: function ( data, type, full ) {
                var name = full["name"] ;
                if(name){
                    return  "<a href="+baseurl+'/app/productOption/'+full["id"]+">"+name+"</a>"; 
                }else{
                   return '-'; 
                }
            }},
           
            {data : "",render: function ( data, type, full ){
                return  "<a href="+baseurl+'/app/productOption/'+full["id"]+"/edit><i class='glyphicon glyphicon-edit user-edit two-btns' title='edit'></i></a><a id='product-option-delete' data-id="+full["id"]+"><i class='glyphicon glyphicon-trash user-delete' title='delete'></i></a>"; 
            }} 
           
			], 
            "columnDefs": [{ targets: [4], className: 'text-center', orderable:false }],

        colReorder: true
    });
});

$("body").on("click","#product-option-delete",function(e){
    
    var id      = $(this).data("id");
    var token   = $("meta[name='csrf-token']").attr("content");
    var baseurl = $("#baseurl").val();
    var url     = baseurl+'/app/productOption/'+id;

    $.ajax({
        url : url, 
        type: 'DELETE',
        data: {
            _token: token,
            id: id
        },
        success: function (response){

            window.location=window.location;
        }
    });
});