$(document).ready(function () {   
    var baseurl = $("#baseurl").val();
    var select  =  $("#select").data('select');
    var modal   = $('#properties-table').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "order": [[ 0, "desc" ]],
    "ajax": baseurl+'/admin/property-datatable', 
    "columns": [
            {data: "id",render: function ( data, type, full ) {
                
                return  "<a href="+baseurl+'/admin/properties/'+full["id"]+">"+full["id"]+"</a>"; 

            }},
            { data: "parcel_id", render: function ( data, type, full ) {

                return  "<a href="+baseurl+'/admin/properties/'+full["id"]+">"+(full["parcel_id"]?full["parcel_id"]:"[[PENDING]]")+"</a>";  
            }}, 

            {data :"legacy_file_number"},
            {data :"district"},
            {data :"sub_division"},
            {data: "address"},            
                                                                                
            {data :"status"},
            {data :"migration_status"},
            {data : "action",render: function ( data, type, full ){

                if(full["edit"] == 1){
                    return  "<a href="+baseurl+'/admin/properties/'+full["id"]+"/edit><i class='glyphicon glyphicon-edit user-edit' title='edit customer'></i></a>"; 

                }else{
                    return  "";
                }
                },
                "searchable" : false
            }
            ],
        colReorder: true
    });

    
} );