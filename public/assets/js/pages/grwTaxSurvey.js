$(document).ready(function() {

    var baseurl = $("#baseurl").val();

    $("#tax-survey-search").on('click', function() {
        var sub_district   = $("#sub_district").val();
        var grid           = $("#grid").val();
        var contact_name   = $("#contact_name").val();
        var contact_number = $("#contact_number").val();
        
        if(sub_district) {

            $("#tax-survey-table").show();
            $('#tax-survey-table').DataTable({
                "dom": 'Bfrtip',
                "processing": true,
                "serverSide": true,
                // "searching": false,
               "ajax": {
                  url : baseurl+'/admin/taxSurvey/datatable',
                  dataType: "json",
                  headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  type: "get",
                  data : {"sub_district":sub_district,"grid":grid,"contact_name":contact_name,"contact_number":contact_number},
                },
                columns: [
                    {data: 'id'},
                    {data: 'door_number'},
                    {data: 'street_name'},
                    {data: 'sub_district'},
                    {data: 'size'},
                    {data: 'status'}, 
                    {data: 'grid'}

                ],
              
                "destroy": true
            });
        }else{
            swal({
                    title: "Please Select Sub District",
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#418BCA",
                    confirmButtonText: "Ok",
            });
        }
    });
});
