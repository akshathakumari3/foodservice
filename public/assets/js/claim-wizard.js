"use strict";
$(document).ready(function () {
    
    $(".wrapper").addClass("hide_menu");
    
    // manage wizard 
    var form  = $("#claimform");  

    form.steps({
        headerTag: "h6",
        bodyTag: "div",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex) {
            
            $("#info").hide();
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            
            // dont change the order of the checks
            // Customer  Information
            if (newIndex === 1) {
                return true;
            }

            // Mobile OTP
            if (newIndex === 2) {
            
                return true;
            }
        
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex) {

            form.validate().settings.ignore = ":disabled";
            $("#wizard-error").html("Form Not valid").show();
            return form.valid();

        },
        onFinished: function (event, currentIndex) {

            // console.log('finish');

        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) {
            console.log('error last'+error);
            element.before(error);
        }
    });  
    
});

